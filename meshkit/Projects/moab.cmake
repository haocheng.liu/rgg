set(extra_deps)
if (BUILD_WITH_CUBIT)
  list(APPEND extra_deps
    cubit)
endif ()

if (BUILD_WITH_MPI)
  list(APPEND extra_deps
    pnetcdf)
endif ()

add_external_project(moab
  DEPENDS szip hdf5 cgm netcdf zlib ${extra_deps}
  CMAKE_ARGS
    -DCMAKE_INSTALL_PREFIX:path=<INSTALL_DIR>
    -DMOAB_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DENABLE_IMESH:BOOL=ON
    -DENABLE_CGM:BOOL=ON
    -DENABLE_NETCDF:BOOL=ON
    -DENABLE_HDF5:BOOL=ON
    -DENABLE_IREL:BOOL=ON
    -DENABLE_PNETCDF:BOOL=${BUILD_WITH_MPI}
    -DENABLE_MPI:BOOL=${BUILD_WITH_MPI}
    -DENABLE_SZIP:BOOL=ON
    -DENABLE_TESTING:BOOL=OFF
    -DNETCDF_DIR:path=<INSTALL_DIR>
    -DNetCDF_DIR:path=<INSTALL_DIR>
    "-DCUBITROOT:PATH=${CUBITROOT}"
  ${suppress_build_out}
)
