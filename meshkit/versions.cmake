# This maintains the links for all sources used by this superbuild.
# Simply update this file to change the revision.
# One can use different revision on different platforms.
# e.g.
# if (UNIX)
#   ..
# else (APPLE)
#   ..
# endif()

add_revision(netcdf
  URL "ftp://ftp.unidata.ucar.edu/pub/netcdf/old/netcdf-4.3.0.tar.gz"
  URL_MD5 40c0e53433fc5dc59296ee257ff4a813)

add_revision(szip
  URL "http://paraview.org/files/dependencies/szip-2.1.tar.gz"
  URL_MD5 902f831bcefb69c6b635374424acbead)

add_revision(zlib
  URL "http://www.paraview.org/files/dependencies/zlib-1.2.7.tar.gz"
  URL_MD5 60df6a37c56e7c1366cca812414f7b85)

add_revision(pnetcdf
  URL "http://cucis.ece.northwestern.edu/projects/PnetCDF/Release/parallel-netcdf-1.6.0.tar.bz2"
  URL_MD5 43e1ce63da7aab72829502a1e2e27161)

add_revision(hdf5
  URL "http://paraview.org/files/dependencies/hdf5-1.8.9.tar.gz"
  URL_MD5 d1266bb7416ef089400a15cc7c963218)

add_revision(mpich2
  URL "http://www.mcs.anl.gov/research/projects/mpich2/downloads/tarballs/1.4.1p1/mpich2-1.4.1p1.tar.gz"
  URL_MD5 b470666749bcb4a0449a072a18e2c204)

add_revision(ftgl
  GIT_REPOSITORY "https://github.com/ulrichard/ftgl"
  GIT_TAG master
  )

add_revision(OCE
  GIT_REPOSITORY "https://github.com/mathstuf/oce.git"
  GIT_TAG "next"
  )

add_revision(cgm
  #GIT_REPOSITORY https://bitbucket.org/fathomteam/cgm.git
  #GIT_TAG master
  GIT_REPOSITORY "https://bitbucket.org/mathstuf/cgm.git"
  GIT_TAG update-cmakelists
  )

if(BUILD_MESHKIT_MASTER)
  add_revision(moab
    GIT_REPOSITORY https://bitbucket.org/mathstuf/moab.git
    GIT_TAG master
  )

  add_revision(meshkit
    GIT_REPOSITORY https://bitbucket.org/mathstuf/meshkit.git
    GIT_TAG cmake
  )
else()
  add_revision( moab
                GIT_REPOSITORY https://bitbucket.org/mathstuf/moab.git
                GIT_TAG windows-with-cmake-support )

  add_revision(meshkit
    #GIT_REPOSITORY https://bitbucket.org/fathomteam/meshkit.git
    GIT_REPOSITORY  https://bitbucket.org/mathstuf/meshkit.git
    #GIT_TAG MeshKitv1.3
    GIT_TAG cmake
    #GIT_TAG 57659678d1632bcac7a4f551cad7800a40c27aa4
  )
endif()
