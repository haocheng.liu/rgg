#include "cmbNucNeumannWidget.h"
#include "ui_qNeumannSet.h"

#include "cmbNucCore.h"

#include <algorithm>
#include <string>

#include <QComboBox>

class cmbNucNeumannWidgetInternal : public Ui::neumannSet
{
public:
  cmbNucCoreParams * params;
  std::vector<cmbNucCoreParams::NeumannSetStruct> neumannSet;
};

class NeumannSetTableItem : public QTableWidgetItem
{
public:
  NeumannSetTableItem( cmbNucNeumannWidget * w,
                       std::vector<cmbNucCoreParams::NeumannSetStruct> & ns, int r, int c)
  :neumannSet(ns), widget(w)
  {
    if(c == 1)
    {
      this->setText(QString::number(neumannSet[r].Id));
    }
    else if(c == 2)
    {
      this->setText(neumannSet[r].Equation.c_str());
    }
  }

  void setCol1(int role, const QVariant& value)
  {
    bool ok;
    int v = value.toInt(&ok);
    if(!ok) return;
    if(v < 0) return;
    for(size_t i = 0; i < neumannSet.size(); ++i)
    {
      if(this->row() != static_cast<int>(i))
      {
        if(neumannSet[i].Id == v) return;
      }
    }
    neumannSet[this->row()].Id = v;
    widget->checkChange();
    QTableWidgetItem::setData(role, value);
  }

  void setCol2(int role, const QVariant& value)
  {
    //check the mode
    QString v = value.toString();
    if(!v.isEmpty())
    {
      neumannSet[this->row()].Equation = v.toStdString();
      widget->checkChange();
      QTableWidgetItem::setData(role, value);
    }
  }

  virtual void setData(int role, const QVariant& value)
  {
    if(this->column() == 1 && role == Qt::EditRole)
    {
      this->setCol1(role, value);
    }
    else if(this->column() == 2 && role == Qt::EditRole)
    {
      this->setCol2(role, value);
    }
    else
    {
      QTableWidgetItem::setData(role, value);
    }
  }

  virtual ~NeumannSetTableItem()
  {}

  std::vector<cmbNucCoreParams::NeumannSetStruct> & neumannSet;
  cmbNucNeumannWidget * widget;
};

cmbNucNeumannWidget::cmbNucNeumannWidget(QWidget* p)
: cmbNucCheckableWidget(p), Internal(new cmbNucNeumannWidgetInternal())
{
  this->Internal->setupUi(this);
  this->Core = NULL;
  this->Internal->params = NULL;

  connect(this->Internal->NewmannSetAdd, SIGNAL(clicked()), this, SLOT(onAddToTable()));
  connect(this->Internal->NewmannSetDel, SIGNAL(clicked()), this, SLOT(onDeleteRow()));
  connect(this->Internal->table, SIGNAL(cellChanged(int, int)),  this, SLOT(checkChange()));
}

cmbNucNeumannWidget::~cmbNucNeumannWidget()
{
  delete this->Internal;
}

void cmbNucNeumannWidget::onApply()
{
  if( this->Core == NULL ) return;
  if(this->Internal->neumannSet.empty())
  {
    this->Internal->params->clearNeumannSet();
  }
  else
  {
    this->Internal->params->setNeumannSet(this->Internal->neumannSet);
  }
  if(isDifferent())
  {
    this->Core->setAndTestDiffFromFiles(isDifferent());
    this->Core->getConnection()->componentChanged(NULL);
  }
  onReset();
}

void cmbNucNeumannWidget::onReset()
{
  if( this->Core == NULL ) return;
  if( this->Internal->params->NeumannSetIsSet())
  {
    this->Internal->neumannSet = this->Internal->params->getNeumannSet();
  }
  else
  {
    this->Internal->neumannSet.clear();
  }
  this->Internal->table->setRowCount(static_cast<int>(this->Internal->neumannSet.size()));
  for(unsigned int i = 0; i < this->Internal->neumannSet.size(); ++i)
  {
    this->addToTable(i, this->Internal->neumannSet[i]);
  }
}

void cmbNucNeumannWidget::onClear()
{
  if( this->Core == NULL ) return;
  this->Internal->neumannSet.clear();
  this->Core = NULL;
}

void cmbNucNeumannWidget::set( cmbNucCore * c )
{
  this->Core = c;
  if(c == NULL)
  {
    this->Internal->params = NULL;
  }
  else
  {
    this->Internal->params = &(c->getParams());
  }
  onReset();
}

void cmbNucNeumannWidget::onAddToTable()
{
  std::set<int> ids;
  int id = 0;
  for (unsigned int i = 0; i < this->Internal->neumannSet.size(); ++i)
  {
    ids.insert(this->Internal->neumannSet[i].Id);
  }
  while(ids.find(id) != ids.end())
  {
    id++;
  }
  cmbNucCoreParams::NeumannSetStruct nss;
  nss.Id = id;
  nss.Side = "Top";

  int current_row = static_cast<int>(this->Internal->neumannSet.size());
  this->Internal->table->setRowCount(current_row + 1);
  this->Internal->neumannSet.push_back(nss);
  addToTable(current_row, nss);
  checkChange();
}

void cmbNucNeumannWidget::onDeleteRow()
{
  this->Internal->table->blockSignals(true);
  int row = this->Internal->table->currentRow();
  this->Internal->table->removeRow(row);
  this->Internal->neumannSet.erase(this->Internal->neumannSet.begin()+row);
  this->Internal->table->blockSignals(false);
  checkChange();
}

void cmbNucNeumannWidget::checkChange()
{
  bool hasChange = false;
  std::vector<cmbNucCoreParams::NeumannSetStruct> const& current =
                                                            this->Internal->params->getNeumannSet();
  if(current.size() == this->Internal->neumannSet.size())
  {
    for(unsigned int i = 0; i < current.size(); ++i)
    {
      if(current[i] != this->Internal->neumannSet[i]) hasChange = true;
    }
  }
  else
  {
    hasChange = true;
  }
  setValueChanged(hasChange);
}

void cmbNucNeumannWidget::addToTable(int row, cmbNucCoreParams::NeumannSetStruct const& n)
{
  QTableWidget * tmpTable = this->Internal->table;
  tmpTable->blockSignals(true);
  QWidget * tmpWidget = tmpTable->cellWidget(row, 0);
  QComboBox* comboBox = dynamic_cast<QComboBox*>(tmpWidget);
  if(comboBox == NULL)
  {
    comboBox = new QComboBox;
    comboBox->addItem("Top");
    comboBox->addItem("Bot");
    comboBox->addItem("Side");
    comboBox->addItem("SideAll");
    comboBox->setObjectName("NeumannPartBox_" + QString::number(row));
    tmpTable->setCellWidget(row, 0, comboBox);
    cmbNucEmmitingTableElement* item = new cmbNucEmmitingTableElement;
    connect(comboBox, SIGNAL(currentIndexChanged(QString)), item, SLOT(forward(QString)));
    connect(item, SIGNAL(cellchanged(int, int, QString)),
            this, SLOT(neumannTypeChanged(int, int, QString)));
    tmpTable->setItem(row, 0, item);
  }
  comboBox->setCurrentIndex(comboBox->findText(n.Side.c_str(), Qt::MatchFixedString));
  NeumannSetTableItem * nsti = new NeumannSetTableItem(this, this->Internal->neumannSet, row, 1);
  tmpTable->setItem(row, 1, nsti);
  nsti = new NeumannSetTableItem(this, this->Internal->neumannSet, row, 2);
  tmpTable->setItem(row, 2, nsti);
  setEquationEditable(row);
  tmpTable->blockSignals(false);
}

void cmbNucNeumannWidget::neumannTypeChanged(int row, int col, QString v)
{
  if(col == 0 && static_cast<size_t>(row) < this->Internal->neumannSet.size())
  {
    this->Internal->neumannSet[row].Side = v.toStdString();
    setEquationEditable(row);
  }
  checkChange();
}

void cmbNucNeumannWidget::setEquationEditable(int row)
{
  QTableWidgetItem * item = this->Internal->table->item(row, 2);
  if(item == NULL) return;
  std::string side = this->Internal->neumannSet[row].Side;
  std::transform(side.begin(), side.end(), side.begin(), ::tolower);
  if(side == "side")
  {
    item->setFlags(item->flags() |  Qt::ItemIsEditable);
    if(this->Internal->neumannSet[row].Equation.empty())
    {
      this->Internal->neumannSet[row].Equation = "x";
      item->setText("x");
    }
    item->setBackground( this->Internal->table->item(row,1)->background() );
  }
  else
  {
    item->setFlags(item->flags() & ~Qt::ItemIsEditable);
    this->Internal->neumannSet[row].Equation = "";
    item->setBackground(Qt::gray);
    item->setText("");
  }
}
