#include "DrawLatticeItem.h"

#include <QFont>
#include <QPainter>
#include <QDebug>

#include <cmath>

//#define DEBUG_CORDINATE

#ifdef DEBUG_CORDINATE
#include "cmbNucFillLattice.h"
#endif

DrawLatticeItem::DrawLatticeItem(const QPolygonF& poly, int l, int cellIdx,
                                 Lattice::CellReference const& ref, QGraphicsItem* parent)
: QGraphicsPolygonItem(poly, parent), refCell(ref), m_layer(l), m_cellIndex(cellIdx)

{
  this->setAcceptDrops(true);
  localCenter = QPointF(0,0);
  for(int i = 0; i < poly.count(); ++i)
  {
    localCenter += poly.value(i);
  }
  localCenter /= poly.count();
}

QString DrawLatticeItem::text() const
{
  return refCell.getCell()->getLabel();
}

int DrawLatticeItem::layer()
{
  return m_layer;
}

int DrawLatticeItem::cellIndex()
{
  return m_cellIndex;
}

void DrawLatticeItem::drawText(QPainter *painter)
{
  QRectF trect = boundingRect();
  Lattice::Cell const* cell = refCell.getModeCell();
  QColor c = cell->getColor();
  double gray = c.red()*0.299 + c.green()*0.587 + c.blue()*0.114;

  QColor textColor;

  switch(refCell.getDrawMode())
  {
    case Lattice::CellReference::SELECTED:
    case Lattice::CellReference::NORMAL:
    case Lattice::CellReference::FUNCTION_APPLY:
      textColor = ( gray < 186 ) ? Qt::white : Qt::black;
      break;
    case Lattice::CellReference::UNSELECTED:
      textColor = ( gray < 186 ) ? QColor(200, 200, 200, 100) : QColor(50, 50, 50, 100);
      break;
  }

  QFont font;
  font.setPixelSize(12);
  painter->setPen(textColor);
  QString txt = cell->getLabel();
  if(refCell.isInConflict())
  {
    txt = "!" + txt + "!";
    font.setBold(true);
    font.setStrikeOut(true);
  }
  painter->setFont(font);
#ifdef DEBUG_CORDINATE
  double mr = refCell.getMaxRadius();
  txt = QString::number(mr, 'f', 2);
#endif
  painter->drawText(trect, Qt::AlignCenter | Qt::AlignCenter | Qt::TextWordWrap, txt);
}

void DrawLatticeItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                            QWidget* widget)
{
  Lattice::Cell const* cell = refCell.getModeCell();
  QColor c = cell->getColor();
  double gray = c.red()*0.299 + c.green()*0.587 + c.blue()*0.114;
  switch(refCell.getDrawMode())
  {
    case Lattice::CellReference::SELECTED:
      this->setPen(QPen(Qt::darkCyan,3));
      this->setAcceptDrops(true);
      break;
    case Lattice::CellReference::NORMAL:
      this->setPen(QPen(Qt::black));
      this->setAcceptDrops(true);
      break;
    case Lattice::CellReference::UNSELECTED:
    {
      int g	= qGray(c.rgb());
      if( g >= 230 ) g-= 25;
      c = QColor(g,g,g, 175);
      this->setPen(QPen(Qt::white, 0));
      this->setAcceptDrops(false);
      break;
    }
    case Lattice::CellReference::FUNCTION_APPLY:
      this->setPen(QPen(Qt::darkRed, 2));
      this->setAcceptDrops(true);
      break;
  }
  this->setBrush(QBrush(c));

  this->Superclass::paint(painter, option, widget);

  if(refCell.isInConflict())
  {
    if(gray < 186)
    {
      c = c.lighter();
    }
    else
    {
      c = c.darker();
    }
    painter->setBrush(QBrush(c,Qt::DiagCrossPattern));
    painter->drawPolygon(this->polygon());
  }

  this->drawText(painter);
}

cmbNucPart * DrawLatticeItem::getPart()
{
  Lattice::Cell const* cell = refCell.getCell();
  return cell->getPart();
}

QPointF DrawLatticeItem::getCentroid() const
{
  return localCenter + pos();
}
