
#include "cmbNucAssembly.h"
#include "cmbNucCore.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <limits>
#include <cctype>
#include <functional>
#include <cassert>

#include "cmbNucMaterialColors.h"
#include "cmbNucDefaults.h"
#include "vtkCmbLayeredConeSource.h"
#include "cmbNucRender.h"
#include "cmbNucPartLibrary.h"
#include "cmbNucCordinateConverter.h"

#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>

#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkAppendPolyData.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkPolyDataNormals.h"
#include "vtkXMLMultiBlockDataWriter.h"
#include "vtkNew.h"
#include "vtkMath.h"

#include "vtkXMLMultiBlockDataWriter.h"
#include <QMap>
#include <QDebug>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>

void cmbAssyParameters::fill(cmbAssyParameters * other)
{
  UnknownParams = other->UnknownParams;
  this->AxialMeshSize = other->AxialMeshSize;
  this->MoveXYZ[0] = other->MoveXYZ[0];
  this->MoveXYZ[1] = other->MoveXYZ[1];
  this->MoveXYZ[2] = other->MoveXYZ[2];
  this->setNeumannSet_StartId(other->getNeumannSet_StartId());
  this->setMaterialSet_StartId(other->getMaterialSet_StartId());
  //this->SectionReverse = false;
#define FUN_SIMPLE(TYPE,X,Var,Key) this->Var = other->Var;
  ASSYGEN_EXTRA_VARABLE_MACRO()
#undef FUN_SIMPLE
}

std::string TO_AXIS_STRING[] = {"X", "Y", "Z"};

//transformation helper classes

void cmbNucAssembly::Transform::setAxis(std::string a)
{
  Valid = true;
  if(a == "X" || a == "x") axis = X;
  else if(a == "Y" || a == "y") axis = Y;
  else if(a == "Z" || a == "z") axis = Z;
  else Valid = false;
}

cmbNucAssembly::Rotate::Rotate( std::string a, double delta )
{
  this->setAxis(a);
  this->angle = delta;
}

cmbNucAssembly::Rotate::Rotate(AXIS a, double delta)
{
  Valid = true;
  axis = a;
  this->angle = delta;
}

std::ostream&
cmbNucAssembly::Rotate::write(std::ostream& os) const
{
  os << "Rotate " << TO_AXIS_STRING[this->axis] << " " << this->angle;
  return os;
}

cmbNucAssembly::Section::Section(AXIS a, double v, int d)
{
  Valid = true;
  axis = a;
  value = v;
  dir = d;
}

cmbNucAssembly::Section::Section( std::string a, double v, std::string d )
:dir(1), value(v)
{
  setAxis(a);
  std::transform(d.begin(), d.end(), d.begin(), ::tolower);
  d.erase(d.begin(), std::find_if(d.begin(), d.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
  d.erase(std::find_if(d.rbegin(), d.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), d.end());
  if(d == "reverse") dir = -1;
}

std::ostream&
cmbNucAssembly::Section::write(std::ostream& os) const
{
  os << "Section " << TO_AXIS_STRING[this->axis] << " " << this->value;
  if(dir == -1) os << " reverse";
  return os;
}

/*************************************************************************/

cmbNucAssembly::cmbNucAssembly(QString label, QString name, QColor color, bool cps,
                               double ppitchX, double ppitchY)
: LatticeContainer(label, name, color,ppitchX, ppitchY),  Defaults(new cmbNucDefaults()),
  Pins(NULL), AssyDuct(NULL), KeepPinsCentered(cps), Parameters(new cmbAssyParameters),
  DifferentFromCub(true), DifferentFromJournel(true)
{
  this->Parameters->setNeumannSet_StartId(0);
  this->Parameters->setMaterialSet_StartId(0);

  this->AssyDuct = NULL;

  QObject::connect(this->connection, SIGNAL(sendPitch(double, double)),
                   this->Defaults,   SIGNAL(recieveCalculatedPitch(double, double)));

  if(KeepPinsCentered)
  {
    calculatePitch(this->pitchX, this->pitchY);
  }
}

cmbNucAssembly::~cmbNucAssembly()
{
  this->deleteConnection(); //removes connections
  this->PinCells.clear(); //DO NOT DELETE, owned by someone else
  for(unsigned int i = 0; i < this->Transforms.size(); ++i)
  {
    delete this->Transforms[i];
    this->Transforms[i] = NULL;
  }
  this->Transforms.clear();

  delete this->Parameters;
  delete this->Defaults;
}

void cmbNucAssembly::geometryChanged()
{
  setAndTestDiffFromFiles(true);
}

vtkBoundingBox cmbNucAssembly::computeBounds()
{
  return this->AssyDuct->computeBounds(this->IsHexType());
}

void cmbNucAssembly::getZRange(double & z1, double & z2)
{
  this->AssyDuct->getZRange(z1, z2);
}

void cmbNucAssembly::AddPinCell(PinCell *pincell)
{
  if(pincell == NULL) return;
  this->connectObj(pincell);
  this->PinCells.push_back(pincell);
}

void cmbNucAssembly::SetPinCell(int i, PinCell *pc)
{
  if(static_cast<std::size_t>(i) > this->PinCells.size()) return;
  if(pc == NULL) return;
  PinCell * old = this->PinCells[i];
  if(old == pc) return;
  this->disconnectObj(old);
  this->connectObj(pc);
  this->PinCells[i] = pc;
}

void cmbNucAssembly::RemovePinCell(PinCell *pin)
{
  cmbNucPart::removeObj(pin, this->PinCells, false); //DO NOT DELETE, does not have ownership
  // update the Grid
  if(this->lattice.deletePart(pin))
  {
    setAndTestDiffFromFiles(true);
  }
}

QString
cmbNucAssembly::extractLabel(QString const& s)
{
  return this->Pins->extractLabel(s);
}

void cmbNucAssembly::setUpdateUsed()
{
  for(size_t i = 0; i < this->PinCells.size(); ++i)
  {
    this->disconnectObj(this->PinCells[i]);
  }
  this->PinCells.clear();

  std::vector<cmbNucPart *> usedParts = this->lattice.getUsedParts();
  
  for(std::vector<cmbNucPart *>::const_iterator it = usedParts.begin();
      it != usedParts.end(); ++it)
  {
    PinCell * p = dynamic_cast<PinCell *>(*it);
    if(p != NULL) this->AddPinCell(p);
  }
}

void cmbNucAssembly::fillList(std::vector< std::pair<QString, cmbNucPart *> > & l)
{
  this->Pins->fillListNamePlusLabel(l);
}

PinCell* cmbNucAssembly::GetPinCell(const QString &label_in)
{
  return dynamic_cast<PinCell*>(this->Pins->GetPartByLabel(label_in));
}

PinCell* cmbNucAssembly::GetPinCell(int pc) const
{
  if(static_cast<size_t>(pc) < this->PinCells.size())
  {
    return this->PinCells[pc];
  }
  return NULL;
}

std::size_t cmbNucAssembly::GetNumberOfPinCells() const
{
  return this->PinCells.size();
}

std::string cmbNucAssembly::getGeometryLabel() const
{
  return this->GeometryType;
}

void cmbNucAssembly::setGeometryLabel(std::string geomType)
{
  this->GeometryType = geomType;
  std::transform(geomType.begin(), geomType.end(),
                 geomType.begin(), ::tolower);
  if(geomType == "hexagonal")
  {
    this->lattice.SetGeometryType(HEXAGONAL);
  }
  else
  {
    this->lattice.SetGeometryType(RECTILINEAR);
  }
}

bool cmbNucAssembly::IsHexType()
{
  return this->lattice.GetGeometryType() == HEXAGONAL;
}

void cmbNucAssembly::calculateExtraTranslation(double & transX, double & transY)
{
  transX = 0; transY = 0;
}

void cmbNucAssembly::calculateTranslation(double & transX, double & transY)
{
  Duct *hexDuct = this->getAssyDuct().getDuct(0);
  transX = hexDuct->getX(); transY = hexDuct->getY();
  if(!this->IsHexType())
  {
    std::pair<int, int> wh = GetDimensions();
    transX = -(wh.second-1) * this->pitchY * 0.5;
    transY = -(wh.first-1) * this->pitchX * 0.5;
  }
}

bool cmbNucAssembly::setPitch(double x, double y, bool testDiff)
{
  bool changed = this->pitchX != x || this->pitchY != y;
  LatticeContainer::setPitch(x, y);
  if(testDiff && changed)
  {
    this->sendChanged();
    return true;
  }
  return false;
}

void cmbNucAssembly::GetDuctWidthHeight(double r[2])
{
  r[0] = 0;
  r[1] = 0;
  for(unsigned int i = 0; i < this->AssyDuct->numberOfDucts(); ++i)
  {
    Duct * tmpd = this->AssyDuct->getDuct(i);
    double t =tmpd->getThickness(0);
    if(t > r[0]) r[0] = t;
    t = tmpd->getThickness(1);
    if(t > r[1]) r[1] = t;
  }
}

void cmbNucAssembly::computeDefaults()
{
  double x, y, l = AssyDuct->getLength();
  if(l>0) Defaults->setHeight(l);
  this->calculatePitch(x, y);
}

void cmbNucAssembly::calculatePitch(int width, int height, double & x, double & y)
{
  double inDuctThick[] = {10,10};
  if(AssyDuct != NULL && !this->AssyDuct->GetInnerDuctSize(inDuctThick[0],inDuctThick[1]) &&
     !this->Defaults->getDuctThickness(inDuctThick[0],inDuctThick[1]))
  {
    //Size is 10
  }
  if(this->IsHexType())
  {
    const double d = inDuctThick[0]-inDuctThick[0]*0.035; // make it slightly smaller to make exporting happy
    x = y = (cmbNucMathConst::cos30 * d) / (width + 0.5 * (width - 1));
  }
  else
  {
    x = (inDuctThick[0])/(width+0.5);
    y = (inDuctThick[1])/(height+0.5);
  }
  if(x<0) x = -1;
  if(y<0) y = -1;
}

void cmbNucAssembly::calculatePitch(double & x, double & y)
{
  std::pair<int, int> dim = lattice.GetDimensions();
  this->calculatePitch(dim.first, dim.second, x, y);
}

void cmbNucAssembly::calculateRadius(double & r)
{
  double minWidth;
  double maxNumber;
  double inDuctRadius[2];
  this->getRadius(inDuctRadius[0], inDuctRadius[1]);
  std::pair<int, int> dim = lattice.GetDimensions();
  if(this->IsHexType())
  {
    minWidth = inDuctRadius[0];
    maxNumber = dim.first-0.5;
  }
  else
  {
    minWidth = std::min(inDuctRadius[0],inDuctRadius[1])*2.0;
    maxNumber = std::max(dim.second, dim.first);
  }
  r = (minWidth/maxNumber)*0.5;
  r = r - r*0.25;
  if(r<0) r = -1;
}

void cmbNucAssembly::setAndTestDiffFromFiles(bool diffFromFile)
{
  bool previous_file_state = this->changeSinceLastGenerate();
  if(diffFromFile)
  {
    this->DifferentFromCub = true;
    this->DifferentFromJournel = true;
  }
  else if(!QFileInfo(this->getFileName().c_str()).exists()) // make sure inp exists
  {
    this->DifferentFromCub = true;
    this->DifferentFromJournel = true;
  }
  else if(this->checkJournel(this->getFileName())) //check the assygen output
  {
    this->DifferentFromCub = true;
    this->DifferentFromJournel = true;
  }
  else if(this->checkCubitOutputFile(this->getFileName())) // check the cubit output
  {
    this->DifferentFromCub = true;
    this->DifferentFromJournel = false;
  }
  else
  {
    this->DifferentFromCub = false;
    this->DifferentFromJournel = false;
  }
  if(previous_file_state!=this->changeSinceLastGenerate()) this->checkChangeState();
}

std::string cmbNucAssembly::getOutputExtension()
{
  if(this->Parameters->getSave_Exodus())
    return ".exo";
  return ".cub";
}

bool cmbNucAssembly::changeSinceLastGenerate() const
{
  return this->DifferentFromCub || this->DifferentFromJournel;
}

void cmbNucAssembly::clear()
{
  this->PinCells.clear();
  delete this->Parameters;
  this->Parameters = new cmbAssyParameters;
  for(unsigned int i = 0; i < this->Transforms.size(); ++i)
  {
    delete this->Transforms[i];
    this->Transforms[i] = NULL;
  }
  this->Transforms.clear();
}

QSet< cmbNucMaterial* > cmbNucAssembly::getMaterials()
{
  QSet< cmbNucMaterial* > result = AssyDuct->getMaterials();
  for(unsigned int i = 0; i < PinCells.size(); ++i)
  {
    result.unite(PinCells[i]->getMaterials());
  }
  return result;
}

bool cmbNucAssembly
::has_boundary_layer_interface(QPointer<cmbNucMaterial> in_material) const
{
  //TODO consider inside the pin
  return this->AssyDuct->isInnerDuctMaterial(in_material);
}

void cmbNucAssembly::setFromDefaults(QPointer<cmbNucDefaults> d)
{
  if(d == NULL) return;
  bool change = false;
  double tmpD = d->getAxialMeshSize();
  int tmpI = d->getEdgeInterval();
  QString tmpS = d->getMeshType();
  if(d->hasAxialMeshSize())
  {
    change |= Parameters->getAxialMeshSize() != tmpD;
    Parameters->setAxialMeshSize(tmpD);
  }
  else if( Parameters->isSetAxialMeshSize() )
  {
    Parameters->clearAxialMeshSize();
    change = true;
  }
  if(d->hasEdgeInterval())
  {
    change |= !Parameters->isSetEdgeInterval() || Parameters->getEdgeInterval() != tmpI;
    Parameters->setEdgeInterval( tmpI );
  }
  else if(Parameters->isSetEdgeInterval())
  {
    Parameters->clearEdgeInterval();
    change = true;
  }
  if(d->hasMeshType())
  {
    std::string tmp = tmpS.toStdString();
    change |= !Parameters->isSetMeshType() || Parameters->getMeshType() != tmp;
    Parameters->setMeshType(tmp);
  }
  else if(Parameters->isSetMeshType())
  {
    Parameters->clearMeshType();
    change = false;
  }

  if(change) this->sendChanged();
}

void cmbNucAssembly::setCenterPins(bool t)
{
  this->KeepPinsCentered = t;
  if(this->KeepPinsCentered)
  {
    this->centerPins();
  }
}

void cmbNucAssembly::centerPins()
{
  double px, py;
  this->calculatePitch(px,py);
  this->setPitch(px,py);
}

bool cmbNucAssembly::addTransform(cmbNucAssembly::Transform * in)
{
  if(in != NULL && in->isValid())
  {
    this->Transforms.push_back(in);
    return true;
  }
  return false;
}

bool cmbNucAssembly::removeOldTransforms(int i)
{
  for(unsigned int r = i; r < this->Transforms.size(); ++r)
  {
    if(this->Transforms[r] != NULL)
    {
      delete this->Transforms[r];
      this->Transforms[r] = NULL;
    }
  }
  if(static_cast<size_t>(i) < this->Transforms.size())
  {
    this->Transforms.resize(i);
    return true;
  }
  return false;
}

cmbNucAssembly::Transform* cmbNucAssembly::getTransform(int i) const
{
  if(static_cast<size_t>(i) < this->Transforms.size())
  {
    return this->Transforms[i];
  }
  return NULL;
}

size_t cmbNucAssembly::getNumberOfTransforms() const
{
  return this->Transforms.size();
}

void cmbNucAssembly::setDuctCell(DuctCell * ad)
{
  if(ad == this->AssyDuct) return;

  if(this->AssyDuct != NULL)
  {
    this->disconnectObj(AssyDuct);
    this->AssyDuct->freed();
  }

  this->AssyDuct = ad;

  if(KeepPinsCentered || (this->pitchX == 0 && this->pitchY == 0))
  {
    this->calculatePitch(this->pitchX, this->pitchY);
  }

  this->lattice.updatePitchForMaxRadius(this->pitchX, this->pitchY);
  double r[2];
  this->GetDuctWidthHeight(r);
  this->lattice.updateMaxRadius( r[0], r[1] );

  if(ad != NULL)
  {
    this->connectObj(this->AssyDuct);
    this->AssyDuct->used();
  }
}

void cmbNucAssembly::setDuctLibrary(cmbNucPartLibrary * d)
{
  this->Ducts = d;
  if(this->AssyDuct == NULL)
  {
    this->setDuctCell(d->GetPart<DuctCell>(0));
  }
}

//Used when importing inp file
void cmbNucAssembly::adjustRotation()
{
  if(this->Transforms.empty()) return;
  double r = 0;
  if(this->lattice.getFullCellMode() ==  Lattice::HEX_FULL_30)
  {
    r = -30;
  }
  for(unsigned int i = 0; i < this->Transforms.size(); ++i)
  {
    if(this->Transforms[i]->getLabel() == "Rotate" && this->Transforms[i]->getAxis() == Transform::Z)
    {
      double v = this->Transforms[i]->getValue();
      r += v;
    }
  }
  while(r < -180) r += 360;
  if(r != 0)
  {
    this->setZAxisRotation(static_cast<int>(r));
  }
  removeOldTransforms(0);
}

double cmbNucAssembly::getZAxisRotation() const
{
  if(zAxisRotation.isValid()) return zAxisRotation.getValue();
  return 0;
}

void cmbNucAssembly::setZAxisRotation(double d)
{
  zAxisRotation = Rotate(Transform::Z, d);
}

cmbNucAssembly * cmbNucAssembly::clone(cmbNucPartLibrary * pl, cmbNucPartLibrary * dl)
{
  cmbNucAssembly * result = new cmbNucAssembly(this->Label, this->Name, this->LedgendColor,
                                               this->KeepPinsCentered, this->pitchX,
                                               this->pitchY);
  result->setPinLibrary(pl);
  result->setDuctLibrary(dl);
  for(std::vector<PinCell*>::const_iterator iter = this->PinCells.begin();
      iter != this->PinCells.end(); ++iter)
  {
    result->AddPinCell(pl->GetPart<PinCell>((*iter)->getLabel(),  cmbNucPartLibrary::BY_LABEL));
  }
  DuctCell * dc = dl->GetPart<DuctCell>(this->AssyDuct->getName(), cmbNucPartLibrary::BY_NAME);
  assert(dc != NULL);
  result->setDuctCell(dc);
  this->lattice.updatePitchForMaxRadius(this->pitchX, this->pitchY);
  double r[2];
  this->GetDuctWidthHeight(r);
  this->lattice.updateMaxRadius( r[0], r[1] );
  result->zAxisRotation = this->zAxisRotation;
  result->GeometryType = this->GeometryType;
  result->DifferentFromCub = this->DifferentFromCub;
  result->DifferentFromJournel = this->DifferentFromJournel;

  result->KeepPinsCentered = this->KeepPinsCentered;

  result->Parameters->fill(this->Parameters);

  result->lattice = Lattice(this->lattice);

  result->ExportFilename = this->ExportFilename;
  result->Path = this->Path;

  return result;
}

void cmbNucAssembly::setPath(std::string const& path)
{
  //bool changed = this->Path != path;
  this->Path = path;
  //if(changed) this->connection->emitCheckChangeState(); //Core data did not change
}

void cmbNucAssembly::setFileName(std::string const& fname)
{
  //NOTE: we are stripping directory information from filename
  QFileInfo fi(fname.c_str());
  //bool changed = this->ExportFilename != fi.fileName().toStdString();
  this->ExportFilename = fi.fileName().toStdString();
  //if(changed) this->connection->emitCheckChangeState(); //core data did not change
}

std::string cmbNucAssembly::getFileName()
{
  if(this->IsHexType())
  {
    return cmbNucAssembly::getFileName(Lattice::HEX_FULL);
  }
  else
  {
    return cmbNucAssembly::getFileName(Lattice::RECT);
  }
}

std::string cmbNucAssembly::getFileName(Lattice::CellDrawMode mode, size_t nom)
{
  if(mode == Lattice::RECT || mode == Lattice::HEX_FULL || mode == Lattice::HEX_FULL_30 || nom == 1)
  {
    if(this->ExportFilename.empty())
    {
      this->ExportFilename = (this->getName().toLower() + ".inp").toStdString();
    }
    return this->Path +  "/" + this->ExportFilename;
  }
  return (QString((this->Path).c_str()) + "/" +
          Lattice::generate_string(this->getName(), mode).toLower() + ".inp").toStdString();
}

bool cmbNucAssembly::needToRunMode(Lattice::CellDrawMode mode, std::string & fname, size_t nom )
{
  fname = this->getFileName(mode, nom);
  if(this->changeSinceLastGenerate()) return true;
  else if(!QFileInfo(fname.c_str()).exists()) return true;
  else if(this->checkJournel(fname)) return true;
  else if(this->checkCubitOutputFile(fname)) return true;
  //else
  return false;
}

bool cmbNucAssembly::checkJournel(std::string const& fname )
{
  QFileInfo inpInfo(fname.c_str());
  QDateTime inpLM = inpInfo.lastModified();
  QFileInfo jrlInfo(inpInfo.dir(), inpInfo.baseName().toLower() + ".jou");
  if(jrlInfo.exists())
  {
    QDateTime jrlLM = jrlInfo.lastModified();
    return jrlLM < inpLM;
  }
  return true;
}

bool cmbNucAssembly::checkCubitOutputFile(std::string const& fname )
{
  QFileInfo inpInfo(fname.c_str());
  QDateTime inpLM = inpInfo.lastModified();
  QFileInfo jrlInfo(inpInfo.dir(), inpInfo.baseName().toLower() + ".jou");
  QFileInfo outInfo(inpInfo.dir(), inpInfo.baseName().toLower() + getOutputExtension().c_str());
  if(!outInfo.exists())
  {
    return true;
  }
  QDateTime cubLM = outInfo.lastModified();
  return cubLM < jrlInfo.lastModified();
}

void cmbNucAssembly::componentPartDeleted(cmbNucPart * obj)
{
  if(obj != NULL)
  {
    qDebug() << obj->getName() << obj->getLabel() << obj->GetType();
    if(obj->GetType() == CMBNUC_ASSY_PINCELL)
    {
      this->RemovePinCell(dynamic_cast<PinCell*>(obj));
    }
  }
}

void cmbNucAssembly::componentPartChanged(cmbNucPart * obj)
{
  if(obj != NULL)
  {
    setAndTestDiffFromFiles(true);
    this->sendChanged();
  }
  if(obj == this->AssyDuct)
  {
    this->lattice.updatePitchForMaxRadius(this->pitchX, this->pitchY);
    double r[2];
    this->GetDuctWidthHeight(r);
    this->lattice.updateMaxRadius( r[0], r[1] );
  }
}

void cmbNucAssembly::getRadius(double & ri, double & rj) const
{
  double inDuctThick[2];
  if(!this->AssyDuct->GetInnerDuctSize(inDuctThick[0],inDuctThick[1]) &&
     !this->Defaults->getDuctThickness(inDuctThick[0],inDuctThick[1]))
  {
    inDuctThick[0] = inDuctThick[1] = 10;
  }
  ri = inDuctThick[0] * 0.5;
  rj = inDuctThick[1] * 0.5;
}
