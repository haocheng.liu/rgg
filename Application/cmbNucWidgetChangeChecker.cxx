#include "cmbNucWidgetChangeChecker.h"

#include "cmbNucColorButton.h"

#include <set>
#include <string>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QCheckBox>

//Value conversions are common, do them in one place

template<typename QType>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QType * /*qt*/, cmbNucWidgetChangeChecker * /*t*/ )
{
}

template<typename QType>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QType * /*qt*/, cmbNucWidgetChangeChecker * /*t*/ )
{
}


//cmbNucColorButton
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( cmbNucColorButton * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(changed()), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( cmbNucColorButton * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(changed()), t, SLOT(changeDetected()));
}

template<>
boost::function<QColor (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(cmbNucColorButton * widget)
{
  return boost::bind(&cmbNucColorButton::getColor, widget);
}

template<>
boost::function<void (QColor)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(cmbNucColorButton * widget)
{
  return boost::bind(&cmbNucColorButton::setColor, widget, _1);
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(cmbNucColorButton * /*widget*/)
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::nothingToSet;
}

//QLineEdit
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QLineEdit * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(textEdited(const QString &)), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QLineEdit * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(textEdited(const QString &)), t, SLOT(changeDetected()));
}

template<>
boost::function<QString (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(QLineEdit * widget)
{
  return boost::bind(&QLineEdit::text, widget);
}

template<>
boost::function<void (QString)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(QLineEdit * widget)
{
  return boost::bind(&QLineEdit::setText, widget, _1);
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(QLineEdit * widget)
{
  return boost::bind(&QLineEdit::clear,  widget);
}

//QComboBox
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QComboBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(currentIndexChanged(const QString &)), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QComboBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(currentIndexChanged(const QString &)), t, SLOT(changeDetected()));
}

template<>
boost::function<QString (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(QComboBox * widget)
{
  return boost::bind(&QComboBox::currentText, widget);
}

template<>
boost::function<void (QString)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(QComboBox * widget)
{
  return boost::bind(&QComboBox::setCurrentIndex, widget, boost::bind(&QComboBox::findText, widget,
                                                                      _1, Qt::MatchFixedString));
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(QComboBox * widget)
{
  return boost::bind(&QComboBox::setCurrentIndex, widget,
                     boost::bind(&QComboBox::findText, widget, QString(""), Qt::MatchFixedString));
}

//QDoubleSpinBox
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QDoubleSpinBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(valueChanged(const QString &)), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QDoubleSpinBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(valueChanged(const QString &)), t, SLOT(changeDetected()));
}

template<>
boost::function<double (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(QDoubleSpinBox * widget)
{
  return boost::bind(&QDoubleSpinBox::value, widget);
}

template<>
boost::function<void (double)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(QDoubleSpinBox * widget)
{
  return boost::bind(&QDoubleSpinBox::setValue, widget, _1);
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(QDoubleSpinBox * /*widget*/)
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::nothingToSet;
}


//QPlainTextEdit
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QPlainTextEdit * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(textChanged()), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QPlainTextEdit * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(textChanged()), t, SLOT(changeDetected()));
}

template<>
boost::function<QString (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(QPlainTextEdit * widget)
{
  return boost::bind(&QPlainTextEdit::toPlainText, widget);
}

template<>
boost::function<void (QString)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(QPlainTextEdit * widget)
{
  return boost::bind(&QPlainTextEdit::setPlainText, widget, _1);
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(QPlainTextEdit * widget)
{
  return boost::bind(&QPlainTextEdit::clear, widget);
}

//QCheckBox
template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::connect( QCheckBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::connect( qt, SIGNAL(stateChanged(int)), t, SLOT(changeDetected()));
}

template<>
void  cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::disconnect( QCheckBox * qt, cmbNucWidgetChangeChecker * t )
{
  QObject::disconnect( qt, SIGNAL(stateChanged(int)), t, SLOT(changeDetected()));
}

template<>
boost::function<bool (void)>
cmbNucFunctionWrapperHelper::wrapQtGetFunction(QCheckBox * widget)
{
  return boost::bind(&QCheckBox::isChecked, widget);
}

template<>
boost::function<void (bool)>
cmbNucFunctionWrapperHelper::wrapQtSetFunction(QCheckBox * widget)
{
  return boost::bind(&QCheckBox::setChecked, widget, _1);
}

template<>
boost::function<void (void)>
cmbNucFunctionWrapperHelper::wrapQtClearFunction(QCheckBox * /*widget*/)
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::nothingToSet;
}


//DATA Conversion functions

template<typename TO_DATA_TYPE, typename FROM_DATA_TYPE>
boost::function<TO_DATA_TYPE (FROM_DATA_TYPE)>
cmbNucFunctionWrapperHelper::wrapDataType()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::justReturn<TO_DATA_TYPE>;
}

#define INST_CONVERTER_SAME(TYPE) \
template boost::function<TYPE (TYPE)> cmbNucFunctionWrapperHelper::wrapDataType<TYPE, TYPE>()

INST_CONVERTER_SAME(QString);
INST_CONVERTER_SAME(QColor);
INST_CONVERTER_SAME(double);
INST_CONVERTER_SAME(int);
INST_CONVERTER_SAME(bool);

template<>
boost::function<QString (int)>
cmbNucFunctionWrapperHelper::wrapDataType<QString, int>()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::toNumber<int>;
}

template<>
boost::function<QString (double)>
cmbNucFunctionWrapperHelper::wrapDataType<QString, double>()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::toNumber<double>;
}

template<>
boost::function<double (QString)>
cmbNucFunctionWrapperHelper::wrapDataType<double, QString>()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::toDouble;
}

template<>
boost::function<int (QString)>
cmbNucFunctionWrapperHelper::wrapDataType<int, QString>()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::toInt;
}

template<>
boost::function<QString (std::string)>
cmbNucFunctionWrapperHelper::wrapDataType<QString, std::string>()
{
  return QString::fromStdString;
}

template<>
boost::function<std::string (QString)>
cmbNucFunctionWrapperHelper::wrapDataType<std::string, QString>()
{
  return cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper::toStdString;
}

QString cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
::replace_space_converter(QString in)
{
  return in.replace(' ', "_");;
}

//Main code
cmbNucWidgetChangeChecker::cmbNucWidgetChangeChecker()
{
  this->clear();
}

cmbNucWidgetChangeChecker::~cmbNucWidgetChangeChecker()
{
  this->clear();
}

void cmbNucWidgetChangeChecker::reset()
{
  emit sendReset();
  for(checker_map_type::iterator i = this->objectToChecker.begin();
      i != this->objectToChecker.end(); ++i)
  {
    i->second->reset();
  }
  this->numberOfChanges = 0;
}

void cmbNucWidgetChangeChecker::clear()
{
  emit sendClear();
  std::set<cmbNucWidgetChangeCheckerWrapper *> toDelete;
  for(checker_map_type::iterator i = this->objectToChecker.begin();
      i != this->objectToChecker.end(); ++i)
  {
    i->second->disconnect(this);
    toDelete.insert(i->second);
  }
  this->objectToChecker.clear();
  for(std::set<cmbNucWidgetChangeCheckerWrapper *>::iterator i = toDelete.begin();
      i != toDelete.end(); ++i)
  {
    delete *i;
  }
  this->numberOfChanges = 0;
}

void cmbNucWidgetChangeChecker::apply()
{
  emit sendApply();
  for(checker_map_type::iterator i = this->objectToChecker.begin();
      i != this->objectToChecker.end(); ++i)
  {
    i->second->apply();
  }
  this->numberOfChanges = 0;
}

bool cmbNucWidgetChangeChecker::needToApply()
{
  return this->numberOfChanges != 0;
}

void cmbNucWidgetChangeChecker::registerToTrack( cmbNucWidgetChangeCheckerWrapper * val )
{
  if(val == NULL) return; //DO NOTHING
  std::set<cmbNucWidgetChangeCheckerWrapper *> toDelete;
  for(size_t n = 0; n < val->getNumberOfObjects(); ++n)
  {
    checker_map_type::iterator i = this->objectToChecker.find(val->getObject(n));
    if(i != this->objectToChecker.end())
    {
      toDelete.insert(i->second);
      i->second = val;
    }
    else
    {
      this->objectToChecker[val->getObject(n)] = val;
    }
  }
  for(std::set<cmbNucWidgetChangeCheckerWrapper *>::iterator i = toDelete.begin();
      i != toDelete.end(); ++i)
  {
    (*i)->disconnect(this);
    delete *i;
  }
  val->connect(this);
}

void cmbNucWidgetChangeChecker::changeDetected()
{
  QObject *senderObj = sender();
  checker_map_type::iterator i = this->objectToChecker.find(senderObj);
  if(i != this->objectToChecker.end())
  {
    updateMode(i->second->check());
  }
}

void cmbNucWidgetChangeChecker::updateMode(cmbNucWidgetChangeChecker::mode m)
{
  switch(m)
  {
    case do_nothing:
      break;
    case reverted_to_orginal:
      if(this->numberOfChanges !=0 ) this->numberOfChanges--;
      break;
    case newly_changed:
      this->numberOfChanges++;
      break;
  }
}
