#ifndef cmbNucColorButton_h
#define cmbNucColorButton_h

#include <QWidget>
#include <QColor>

class QPushButton;

class cmbNucColorButton : public QWidget
{
  Q_OBJECT
public:
  cmbNucColorButton(QPushButton * pb);
  void setColor(QColor);
  QColor getColor() const;
signals:
  void changed();
protected slots:
  void chooseColor();

protected:
  QPushButton * button;

};

#endif