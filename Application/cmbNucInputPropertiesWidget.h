#ifndef __cmbNucInputPropertiesWidget_h
#define __cmbNucInputPropertiesWidget_h

#include <QWidget>
#include "cmbNucPartDefinition.h"
#include "cmbNucLattice.h"
#include "cmbNucDraw2DLattice.h"
#include <QStringList>
#include <QPointer>

class cmbNucInputPropertiesWidgetInternal;
class cmbNucMainWindow;
class cmbNucAssembly;
class cmbNucAssemblyLink;
class cmbNucCore;
class PinCell;
class Frustum;
class Cylinder;
class Duct;
class DuctCell;
class cmbNucHexLattice;
class cmbCoreParametersWidget;
class cmbAssyParametersWidget;
class cmbNucDefaultWidget;

class cmbNucInputPropertiesWidget : public QWidget
{
  Q_OBJECT

public:
  cmbNucInputPropertiesWidget(cmbNucMainWindow *mainWindow);
  virtual ~cmbNucInputPropertiesWidget();

  void connectLatticeWidget(cmbNucDraw2DLattice * w);

  // Description:
  // Set the assembly object the widget will be interacting
  void setObject(cmbNucPart* selObj, const char* name);
  cmbNucPart* getObject() {return this->CurrentObject;}

  // Description:
  // set/get the assembly that this widget with be interact with
  void setAssembly(cmbNucAssembly*);
  void setAssemblyLink(cmbNucAssemblyLink*);
  cmbNucAssembly* getAssembly(){return this->Assembly;}

  bool ductCellIsCrossSectioned();
  bool pinCellIsCrossSectioned();

  bool checkForApply();
  void clearChangeMode();

  void clear();

  void setCore(cmbNucCore * core)
  {
    this->Core = core;
  }

signals:
  // Description:
  // Fired when the current object is modified
  void objGeometryChanged(cmbNucPart* selObj);
  void currentObjectNameChanged(const QString& name);
  void sendNameChange(cmbNucPart*, const QString);
  void valuesChanged();
  void resetView();
  void sendLattice(LatticeContainer *);
  void reset();
  void sendXSize(int i);
  void sendYSize(int i);
  void select3DModelView();
  void checkSaveAndGenerate();
  void drawCylinder(double r, int i);
  void clearCylinder();
  void pitchChanged(double, double);

public slots:
  void colorChanged();
  // Invoked when Apply button clicked
  void onApply();
  void emitNameChange(cmbNucPart* obj, const QString & name, const QString & oname)
  {
    emit sendNameChange(obj, name);
    emit currentObjectNameChanged( oname );
  }
  void deleteObj(cmbNucPart *);

protected slots:
  // Invoked when Reset button clicked
  void onReset();
  // reset property panel with given object

  void xSizeChanged(int i);
  void ySizeChanged(int i);

  void computePitch();

  void sendPitchChange();

private:
  cmbNucInputPropertiesWidgetInternal* Internal;

  void autoPitch();

  void initUI();
  cmbNucPart* CurrentObject;
  cmbNucAssembly *Assembly;
  cmbNucCore *Core;

  cmbNucMainWindow *MainWindow;
  cmbCoreParametersWidget* CoreProperties;
  cmbAssyParametersWidget* assyConf;
  QPointer<cmbNucDefaultWidget> CoreDefaults;

  double currentRadius, previousRadius;
  int currentInterval, previousInterval;
};
#endif
