#ifndef cmbNucCore_H
#define cmbNucCore_H

#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <climits>
#include <sstream>
#include <QColor>
#include <QString>
#include <QObject>
#include <QPointer>
#include <QStringList>

#include "vtkMultiBlockDataSet.h"
#include "cmbNucPart.h"
#include "cmbNucLattice.h"
#include "vtkSmartPointer.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkTransform.h"
#include "vtkBoundingBox.h"

#include "cmbNucMaterialColors.h"
#include "cmbNucMaterial.h"

class cmbNucAssembly;
class cmbNucAssemblyLink;
class cmbNucPartLibrary;
class inpFileReader;
class inpFileHelper;
class inpFileWriter;
class cmbNucDefaults;
class vtkPolyData;

#define EXTRA_VARABLE_MACRO() \
   FUN_SIMPLE(std::string, QString, ProblemType, problemtype, "", "") \
   FUN_SIMPLE(std::string, QString, Geometry, geometry, "", "") \
   FUN_SIMPLE(double, QString, MergeTolerance, mergetolerance, -1e23, "") \
   FUN_SIMPLE(std::string, QString, SaveParallel, saveparallel, "", "") \
   FUN_SIMPLE(bool, bool, Info, info, false, "on") \
   FUN_SIMPLE(bool, bool, MeshInfo, meshinfo, false, "on") \
   FUN_STRUCT(std::vector<cmbNucCoreParams::NeumannSetStruct>, cmbNucCoreParams::NeumannSetStruct, NeumannSet, neumannset, std::vector<cmbNucCoreParams::NeumannSetStruct>(), "") \

template<class TYPE>
bool operator!=(std::vector<TYPE> const& vec, bool v)
{
  return vec.empty() == v;
}

class cmbNucCoreParams : public QObject
{
  Q_OBJECT
public:
  class ExtrudeStruct
  {
  public:
    ExtrudeStruct()
    :validSize(false), validDivisions(false)
    {}

    bool isValid() const { return validSize && validDivisions; }

    bool hasValidSize() const { return validSize; }
    bool hasValidDivisions() const { return validDivisions; }

    double getSize() const { return Size; }
    void setSize(double s){ validSize = s > 0; Size = s; }

    int getDivisions() const { return Divisions; }
    void setDivisions(int d) { validDivisions = d > 0; Divisions = d; };

    void clear() { validSize = validDivisions = false; }

  private:
    bool validSize, validDivisions;
    double Size;
    int Divisions;
  };

  struct NeumannSetStruct
  {
    NeumannSetStruct()
    :Side(""), Id(-100), Equation("")
    {}

    std::string Side;
    int Id;
    std::string Equation;
    bool operator!=(bool b)
    { return (Side == "" && Id == -100) == b; }
    bool operator!=(NeumannSetStruct const& other) const
    {
      return this->Side != other.Side || this->Id != other.Id || this->Equation != other.Equation;
    }
  };

  class ReactorVessel
  {
  public:
    enum Mode {None=0,External=1,Generate=2};
    std::string Background;
    std::string BackgroundFullPath;

    ReactorVessel() : Background(""), BackgroundFullPath(""), cylinderRadius(0), cylinderOuterSpacing(0), BackgroundMode(None)
    {}

    double getCylinderRadius()
    { return cylinderRadius; }

    int getCylinderOuterSpacing()
    { return cylinderOuterSpacing; }

    void setCylinderRadius(double r)
    {
      this->cylinderRadius = r;
    }

    void setCylinderOuterSpacing(int v)
    {
      this->cylinderOuterSpacing = v;
    }
    bool generate()
    {
      return BackgroundMode == Generate;
    }
    bool external()
    {
      return BackgroundMode == External;
    }
    bool hasCylinder()
    {
      return BackgroundMode != None;
    }
    void setMode(ReactorVessel::Mode m)
    {
      if(m != BackgroundMode && m == Generate)
      {
        cmbNucMaterialColors::instance()->getUnknownMaterial()->inc();
      }
      else if(m != BackgroundMode && BackgroundMode == Generate)
      {
        cmbNucMaterialColors::instance()->getUnknownMaterial()->dec();
      }
      BackgroundMode = m;
    }
    ReactorVessel::Mode getMode() const { return BackgroundMode; }
  protected:
    double cylinderRadius;
    int cylinderOuterSpacing;
    Mode BackgroundMode;
  } vessel;



private:
  template<typename T>
  class singleton
  {
  public:
    T value;
    bool valid;
    singleton():value(T()),valid(false)
    {}
    void clear()
    {
      valid = false;
      value = T();
    }
  };

#define FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, DK) \
private: \
  singleton<TYPE> Var; \
public: \
  void set##Var(TYPE & in) {Var.valid = true; Var.value = in;} \
  TYPE const& get##Var() { return Var.value; } \
  void clear##Var(){Var.clear();}\
  bool Var##IsSet() \
  { return Var.valid; }

#define FUN_STRUCT(TYPE,X,Var,Key,DEFAULT, DK) FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, DK)

  EXTRA_VARABLE_MACRO()

#undef FUN_SIMPLE
#undef FUN_STRUCT



  bool BackgroundIsSet() {return !vessel.Background.empty();}

  cmbNucCoreParams()
  {
#define FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, DK) Var.clear();
#define FUN_STRUCT(TYPE,X,Var,Key,DEFAULT, DK) Var.clear();
    EXTRA_VARABLE_MACRO()
#undef FUN_SIMPLE
#undef FUN_STRUCT
  }

  void clear()
  {
#define FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, DK) Var.clear();
#define FUN_STRUCT(TYPE,X,Var,Key,DEFAULT, DK) Var.clear();
    EXTRA_VARABLE_MACRO()
#undef FUN_SIMPLE
#undef FUN_STRUCT
    clearUnknownKeyWords();
    Extrude.clear();
    meshFilePrefix = "";
    meshFileExtention = "";
  }

private:
  cmbNucCoreParams::ExtrudeStruct Extrude;
public:
  cmbNucCoreParams::ExtrudeStruct & getExtrude()
  { return Extrude; }

private:
  std::string meshFilePrefix;
  std::string meshFileExtention;
public:
  std::string getMeshOutputFilename() const;
  void setMeshOutputFilename(std::string const& str);
  bool MeshOutputFilenameIsSet() const;
  void clearMeshOutputFilename();

private:
  QString UnknownKeyWords;
public:
  void setUnknownKeyWords(QString & in) {UnknownKeyWords = in;}
  QString const& getUnknownKeyWords() { return UnknownKeyWords; }
  void clearUnknownKeyWords(){UnknownKeyWords = QString();}
  bool UnknownKeyWordsIsSet(){ return true; }
};

// Represents the core which is composed of multiple assemblies
// (cmbNucAssembly). Assemblies are layed out on a lattice.
class cmbNucCore : public LatticeContainer
{
public:
  class  boundaryLayer
  {
  public:
     boundaryLayer()//TODO update this when there is better understanding
    {
      interface_material = cmbNucMaterialColors::instance()->getMaterialByName("water");
      NeumannSet = 14;
      Fixmat = 4;
      Thickness = 0.05;
      Intervals = 6;
      Bias = 0.7;
    }
    QPointer<cmbNucMaterial> interface_material;
    QPointer<cmbNucMaterial> fixed_material;
    int NeumannSet;
    int Fixmat;
    double Thickness;
    int Intervals;
    double Bias;
    //used for export purposes
    std::string jou_file_name;
  };

  // Creates an empty Core.
  cmbNucCore(bool needSaved = true);

  cmbNucPart * clone(){ return NULL; }

  // Destroys the Core.
  virtual ~cmbNucCore();

  vtkBoundingBox computeBounds();

  virtual enumNucPartsType GetType() const {return CMBNUC_CORE;}

  void clearExceptAssembliesAndGeom();

  //Add part
  bool addPart(cmbNucPart * part);
  cmbNucPart * getPart(size_t i);
  size_t getNumberOfParts();
  virtual cmbNucPart * getFromLabel(const QString & s);

  bool name_unique(QString const& n);
  bool label_unique(QString const& n);

  std::vector< cmbNucAssembly* > GetUsedAssemblies();
  std::vector< cmbNucAssemblyLink* > GetUsedLinks();
  std::vector< cmbNucPart* > GetUsedBlanks();

  // Sets the dimensions of the Assembly Core.
  void SetDimensions(int i, int j);

  void SetLegendColors(int numDefaultColors, int defaultColors[][3]);

  // Check if GeometryType is Hexagonal
  bool IsHexType();

  void computePitch();

  virtual void calculateRectPt(unsigned int i, unsigned int j, double pt[2]);

  virtual void calculateExtraTranslation(double & transX, double & transY);
  virtual void calculateTranslation(double & transX, double & transY);
  

  //Set the different from file and tests the h5m file;
  void setAndTestDiffFromFiles(bool diffFromFile);
  void fileChanged();
  void boundaryLayerChanged();
  bool changeSinceLastSave() const;
  bool changeSinceLastGenerate() const;

  std::string getFileName(){return CurrentFileName;}
  virtual QString getTitle(){ return "Core"; }
  virtual QString getListTitle() const { return "Core"; }

  void setGeometryLabel(std::string geomType);

  void setFileName( std::string const& fname );

  std::string getMeshOutputFilename() const;
  void setMeshOutputFilename(std::string const& fname);

  std::string const& getExportFileName() const;
  void setExportFileName(std::string const& fname);

  void setHexSymmetry(int sym);

  cmbNucCoreParams & getParams()
  {
    return Params;
  }

  QPointer<cmbNucDefaults> GetDefaults();
  bool HasDefaults() const;
  void calculateDefaults();
  void sendDefaults();
  void initDefaults();

  void drawCylinder(double r, int i);
  void clearCylinder();

  void checkUsedAssembliesForGen();

  void fillList(std::vector< std::pair<QString, cmbNucPart *> > & l);

  cmbNucCoreParams::ReactorVessel & getVessel()
  {
    return this->Params.vessel;
  }

  cmbNucPartLibrary * getPinLibrary()
  {
    return this->PinLibrary;
  }

  cmbNucPartLibrary * getDuctLibrary()
  {
    return this->DuctLibrary;
  }

  virtual QString extractLabel(QString const& s)
  {
    return s;
  }

  virtual void setUpdateUsed()
  {}

  std::map< QString, std::set< Lattice::CellDrawMode > > getDrawModesForAssemblies();

  void addBoundaryLayer( boundaryLayer * bl); //Takes ownership
  std::vector< cmbNucCore::boundaryLayer*> const& getBoundaryLayers() const
  {
    return this->BoundaryLayers;
  }
  int getNumberOfBoundaryLayers() const;
  void removeBoundaryLayer(size_t bl);
  void clearBoundaryLayer();
  boundaryLayer * getBoundaryLayer(int bl) const;

  int getFreeId();
  int getFreeLinkId(int parentId);
  bool isFreeId(cmbNucPart* exclude, int id);


private:

  cmbNucCoreParams Params;

  std::vector<cmbNucPart*> parts;

  std::vector< boundaryLayer *> BoundaryLayers;
  std::vector< boundaryLayer> ExportBoundaryLayers;

  cmbNucPartLibrary * PinLibrary;
  cmbNucPartLibrary * DuctLibrary;

  QPointer<cmbNucDefaults> Defaults;

  //TODO: we need to redo this.  We should move to a mode form, to keep state
  //of saved file, output mesh, and  boundary layers.
  //Maybe move the diff from file outside of this.
  bool DifferentFromFile;
  bool DifferentFromH5M;
  bool DifferentFromGenBoundaryLayer;

  int HexSymmetry;

  std::string CurrentFileName;
  std::string GenerateDirectory;
  std::string ExportFileName;

  std::set<int> getOccupiedIds(cmbNucPart* exclude);

  virtual void componentPartDeleted(cmbNucPart * obj);
  virtual void componentPartChanged(cmbNucPart * obj);

  void checkUsedAssembliesForGen_imple();
};

#endif // cmbNucCore_H
