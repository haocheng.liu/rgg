
#include "cmbAssyParametersWidget.h"
#include "ui_qAssyParameters.h"

#include "cmbNucAssembly.h"

#include <QLabel>
#include <QPointer>
#include <QDebug>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QDoubleValidator>

class cmbAssyParametersWidget::cmbAssyParametersWidgetInternal :
  public Ui::qAssyParametersWidget
{
public:
};

//-----------------------------------------------------------------------------
cmbAssyParametersWidget::cmbAssyParametersWidget(QWidget *p)
  : QWidget(p)
{
  this->Internal = new cmbAssyParametersWidgetInternal;
  this->Internal->setupUi(this);
  this->initUI();
  this->Assembly = NULL;
}

//-----------------------------------------------------------------------------
cmbAssyParametersWidget::~cmbAssyParametersWidget()
{
  delete this->Internal;
}

//-----------------------------------------------------------------------------
void cmbAssyParametersWidget::initUI()
{
}

//-----------------------------------------------------------------------------
namespace
{
class idUniqueChecker: public cmbNucGetSetDataUnqueChecker<int>
{
public:
  idUniqueChecker(cmbNucAssembly *a, cmbNucCore *c)
  : assembly(a), core(c)
  {}
  virtual bool check(int const& in)
  {
    return core->isFreeId(assembly, in);
  }
  virtual void displayMessage(int const&)
  {
    //no message for now
  }
private:
  cmbNucAssembly * assembly;
  cmbNucCore * core;
};
  void assemblyPostApply(cmbNucAssembly *assyObj, bool change)
  {
    if(change)
    {
      assyObj->setAndTestDiffFromFiles(change);
      assyObj->sendChanged();
    }
  }
}

void cmbAssyParametersWidget
::setAssembly(cmbNucAssembly *assyObj, cmbNucCore* core, cmbNucWidgetChangeChecker & checker)
{
  if(assyObj == NULL)
  {
    return;
  }
  this->Assembly = assyObj;
  cmbAssyParameters* prms = this->Assembly->GetParameters();

  typedef cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper WRAPPER;

#define CHECKERFUN(NAME, DATA_TYPE, WIDGET, W_TYPE, IS_VALID_DATA)                                 \
cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE> * tmp##NAME##Data;                                     \
{                                                                                                  \
  boost::function<DATA_TYPE (void)> getData = boost::bind(&cmbAssyParameters::get##NAME, prms);    \
  boost::function<void (DATA_TYPE)> setData = boost::bind(&cmbAssyParameters::set##NAME, prms, _1);\
  boost::function<void (void)> setDataInvalid = boost::bind(&cmbAssyParameters::clear##NAME, prms);\
  boost::function<bool (void)> dataIsSet = boost::bind(&cmbAssyParameters::isSet##NAME, prms);     \
                                                                                                   \
  boost::function<bool (W_TYPE)> isValidData = IS_VALID_DATA;                                      \
                                                                                                   \
  WIDGET * widget = this->Internal->NAME;                                                          \
  boost::function<void (bool)> postApply = boost::bind(&assemblyPostApply, assyObj, _1);           \
                                                                                                   \
  tmp##NAME##Data = new cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE>(widget, getData, setData,      \
                                                                    setDataInvalid,  dataIsSet,    \
                                                                    isValidData, postApply);       \
  checker.registerToTrack(tmp##NAME##Data);                                                        \
}

  CHECKERFUN(MergeTolerance, double, QLineEdit, QString, WRAPPER::isNumber)
  CHECKERFUN(MeshScheme, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(StartPinId, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(CreateMatFiles, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(Save_Exodus, bool, QCheckBox, bool, WRAPPER::alwaysValid<bool>)
  CHECKERFUN(List_NeumannSet_StartId, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(List_MaterialSet_StartId, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(NumSuperBlocks, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(SuperBlocks, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(CreateSideset, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(TetMeshSize, double, QLineEdit, QString, WRAPPER::isNumber)
  CHECKERFUN(RadialMeshSize, double, QLineEdit, QString, WRAPPER::isNumber)
  CHECKERFUN(CreateFiles, int, QLineEdit, QString, WRAPPER::isNumber)
  CHECKERFUN(Center, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(Info, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(MoveX, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>)
  CHECKERFUN(MoveY, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>)
  CHECKERFUN(MoveZ, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>)
  CHECKERFUN(UnknownParams, QString, QPlainTextEdit, QString, WRAPPER::alwaysValid<QString>)

  CHECKERFUN(NeumannSet_StartId, int, QLineEdit, QString, WRAPPER::isNumber)
  tmpNeumannSet_StartIdData->addUniqueChecker(new idUniqueChecker(this->Assembly, core));
  CHECKERFUN(MaterialSet_StartId, int, QLineEdit, QString, WRAPPER::isNumber)
  tmpMaterialSet_StartIdData->addUniqueChecker(new idUniqueChecker(this->Assembly, core));

  //21
  //Nothing for Geometry
#undef CHECKERFUN
}
