#include "cmbNucRender.h"
#include <vtkCompositePolyDataMapper2.h>
#include <vtkActor.h>
#include <vtkGlyph3DMapper.h>
#include <vtkProperty.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkRenderer.h>
#include <vtkPoints.h>
#include <vtkIntArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkBoundingBox.h>
#include <vtkDoubleArray.h>
#include <vtkPlane.h>
#include <vtkNew.h>
#include <vtkMath.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>
#include <vtkPolyDataNormals.h>
#include <QDebug>
#include <cmath>

#include "cmbNucDefaults.h"
#include "vtkCmbLayeredConeSource.h"
#include "cmbNucMaterialColors.h"
#include "cmbNucPartDefinition.h"
#include "cmbNucAssemblyLink.h"
#include "cmbNucPartLibrary.h"
#include "cmbNucCordinateConverter.h"

typedef cmbNucRender::point point;
typedef cmbNucRender::GeoToPoints GeoToPoints;
typedef cmbNucRender::key key;

const int PinCellResolution = 18;

cmbNucRender::key::key()
{
  type = Jacket;
  sides = -1;
  boundaryLayer = false;
  for(int r = 0; r < 8; ++r) radius[r] = -1;
}

cmbNucRender::key::key( int s, double rTop, double rBottom )
{
  type = Frustum;
  sides = s;
  radius[0] = rTop;
  radius[1] = rBottom;
  boundaryLayer = false;
  for(int r = 2; r < 8; ++r) radius[r] = -1;
}

cmbNucRender::key::key( int s )
{
  type = Cylinder;
  sides = s;
  boundaryLayer = false;
  for(int r = 0; r < 8; ++r) radius[r] = -1;
}

cmbNucRender::key::key( int s,
    double rTop1, double rTop2, double rTop3, double rTop4,
    double rBottom1, double rBottom2, double rBottom3, double rBottom4, bool bl)
{
  type = Annulus;
  sides = s;
  radius[0] = rTop1;
  radius[1] = rBottom1;
  radius[2] = rTop2;
  radius[3] = rBottom2;
  radius[4] = rTop3;
  radius[5] = rBottom3;
  radius[6] = rTop4;
  radius[7] = rBottom4;
  boundaryLayer = bl;
}

bool cmbNucRender::key::operator<(key const& other) const
{
  if(other.boundaryLayer != boundaryLayer)
  {
    return boundaryLayer;
  }
  if(type != other.type) return type < other.type;
  switch(type)
  {
    case Annulus:
    case Frustum:
      if(sides == other.sides)
      {
        for(unsigned int i = 0; i < 8; ++i)
        {
          if(radius[i] != other.radius[i])
          {
            return radius[i] < other.radius[i];
          }
        }
      }
    case Cylinder:
    case Sectioned:
      return sides < other.sides;
    case Jacket:
      return false;
  }
  return false;
}

struct CellKey
{
  cmbNucPart * part;
  Lattice::CellDrawMode mode;
  CellKey( cmbNucPart * s): part(s), mode(Lattice::HEX_FULL)
  {}
  CellKey( cmbNucPart * s, Lattice::CellDrawMode m): part(s), mode(m)
  {}
  bool operator<(CellKey const& other) const
  {
    if(part < other.part) return true;
    if(part == other.part) return mode < other.mode;
    return false;
  }
  bool operator==(CellKey const& other) const
  {
    return part == other.part && mode == other.mode;
  }
};

class cmbNucRenderHelper
{
public:

  static void pointCalc(Lattice & lat, const double radius1, const double radius2, bool markblank,
                        const double tx, const double ty, bool force, int ysign,
                        std::map< CellKey, std::vector<point> > & idToPoint)
  {
    cmbNucCordinateConverter converter(lat, force);
    double lx, ly;
    for(size_t i = 0; i < lat.getSize(); i++)
    {
      const int ti = static_cast<int>(i);
      for(size_t j = 0; j < lat.getSize(i); j++)
      {
        const int tj = static_cast<int>(j);
        if(!lat.GetCell(ti,tj).isBlank() || markblank)
        {
          CellKey k( lat.GetCell(ti,tj).getPart(), lat.getDrawMode(tj, ti));
          converter.convertToPixelXY(ti, tj, lx, ly, radius1, radius2);
          idToPoint[k].push_back(point( tx + lx, ( ty + ysign * ly)));
        }
      }
    }
  }

  template<class T>
  static void calculatePoints( T * input,  std::map< CellKey, std::vector<point> > & idToPoint,
                              bool markblank)
  {
    bool isHex = input->IsHexType();
    double mult = (isHex)?1:0.5;
    double tx, ty;
    input->calculateTranslation(tx, ty);
    pointCalc(input->getLattice(), input->getPitchX()*mult, input->getPitchY()*mult, markblank,
              tx, ty, (!isHex || input->GetType() == CMBNUC_ASSEMBLY), (isHex)?-1:1, idToPoint);
  }

  static void createGeo(DuctCell* ductcell, bool isHex, std::map<key,GeoToPoints> & geometry,
                        cmbNucCore::boundaryLayer * bl = NULL )
  {
    if(ductcell == NULL) return;
    size_t numParts = ductcell->numberOfDucts();
    for(unsigned int j = 0; j < numParts; ++j)
    {
      Duct *duct = ductcell->getDuct(j);
      vtkSmartPointer<vtkCmbLayeredConeSource> manager =
                                              cmbNucRender::CreateLayerManager(ductcell, isHex, j);
      //inner cylinder
      {
        double thickness[] = {manager->GetTopRadius(0,0), manager->GetTopRadius(0,1)};
        key k (manager->GetResolution(0));
        point loc;
        manager->GetBaseCenter(loc.xyz);
        double h = manager->GetHeight();
        cmbNucRender::scale scale(thickness[0],thickness[1],h);
        if(geometry.find(k) == geometry.end())
        {
          geometry[k].geo = manager->CreateUnitLayer(0);
        }
        GeoToPoints & geo = geometry[k];
        geo.points.push_back(GeoToPoints::data(loc, point(), duct->getMaterial(0), scale));
      }
      for( int i = manager->GetNumberOfLayers()-1; i > 0; --i)
      {
        cmbNucMaterial * mat = duct->getMaterial(i);
        key k(manager->GetResolution(i),
              manager->GetTopRadius(i, 0), manager->GetTopRadius(i, 1),
              manager->GetTopRadius(i-1, 0), manager->GetTopRadius(i-1, 1),
              manager->GetBaseRadius(i,0), manager->GetBaseRadius(i,1),
              manager->GetBaseRadius(i-1,0), manager->GetBaseRadius(i-1,1));
        if(geometry.find(k) == geometry.end())
        {
          geometry[k].geo = manager->CreateUnitLayer(i);
        }
        GeoToPoints & geo = geometry[k];
        point loc;
        manager->GetBaseCenter(loc.xyz);
        double h = manager->GetHeight();
        cmbNucRender::scale scale(1,1,h);
        geo.points.push_back(GeoToPoints::data(loc, point(), mat, scale));
      }
      if( bl != NULL && bl->Intervals != 0)
      {
        double tmp_height = duct->getZ2() - duct->getZ1();
        double thickness = 0;
        double bias = bl->Bias;
        //HACK TO HANDLE LESS than one
        if(bias < 2) bias = 1+0.5*(bias);

        int at = 0;
        double max = pow(bl->Intervals, bias);
        for(int inter = 0; inter < bl->Intervals; ++inter)
        {
          thickness = bl->Thickness*pow(inter+1,bias)/max;
          key k(manager->GetResolution(at),
                manager->GetTopRadius(at, 0) - thickness,
                manager->GetTopRadius(at, 1) - thickness,
                manager->GetTopRadius(at, 0) - thickness,
                manager->GetTopRadius(at, 1) - thickness,
                manager->GetBaseRadius(at, 0) - thickness,
                manager->GetBaseRadius(at,1) - thickness,
                manager->GetBaseRadius(at, 0) - thickness,
                manager->GetBaseRadius(at,1) - thickness, true);
          if(geometry.find(k) == geometry.end())
          {
            geometry[k].geo = manager->CreateBoundaryLayer( -thickness, at );
          }
          GeoToPoints & geo = geometry[k];
          point loc;
          manager->GetBaseCenter(loc.xyz);
          loc.xyz[2] -= tmp_height * 0.0005 * 0.5;
          cmbNucRender::scale scale(1,1,tmp_height);
          geo.points.push_back(GeoToPoints::data(loc, point(), bl->interface_material, scale, true));
        }
      }
    }
  }

  static void createGeo(PinCell* pincell, bool isHex,
                        std::map<key,GeoToPoints> & geometry,
                        cmbNucCore::boundaryLayer * bl = NULL,
                        double pitchX = -1, double pitchY = -1 )
  {
    if(pincell == NULL) return;
    size_t numParts = pincell->GetNumberOfParts();
    point rotate(0,0,(isHex)?30:0);

    for(size_t j = 0; j < numParts; ++j)
    {
      PinSubPart* part = pincell->GetPart(static_cast<int>(j));
      vtkSmartPointer<vtkCmbLayeredConeSource> manager =
          cmbNucRender::CreateLayerManager(pincell, isHex, j, pitchX, pitchY);
      if(manager == NULL) continue;
      //inner cylinder/frustum
      {
        double thickness[] = {manager->GetTopRadius(0), manager->GetBaseRadius(0)};
        key k (manager->GetResolution(0));
        point loc;
        manager->GetBaseCenter(loc.xyz);
        double h = manager->GetHeight();
        cmbNucRender::scale scale(thickness[0],thickness[0],h);
        if(thickness[0] != thickness[1])
        {
          k = key(manager->GetResolution(0), thickness[0], thickness[1]);
          scale = cmbNucRender::scale(1,1,h);
        }
        if(geometry.find(k) == geometry.end())
        {
          geometry[k].geo = manager->CreateUnitLayer(0);
        }
        GeoToPoints & geo = geometry[k];
        geo.points.push_back(GeoToPoints::data(loc, rotate, part->GetMaterial(0), scale));
      }

      bool addCellMat = pincell->cellMaterialSet();
      for( int i = manager->GetNumberOfLayers()-1; i > 0; --i)
      {
        cmbNucMaterial * mat = part->GetMaterial(i);

        if(addCellMat)
        {
          addCellMat = false;
          mat = pincell->getCellMaterial();
        }
        key k(manager->GetResolution(i),
              manager->GetTopRadius(i, 0), manager->GetTopRadius(i, 1),
              manager->GetTopRadius(i-1, 0), manager->GetTopRadius(i-1, 1),
              manager->GetBaseRadius(i,0), manager->GetBaseRadius(i,1),
              manager->GetBaseRadius(i-1,0), manager->GetBaseRadius(i-1,1));
        if(geometry.find(k) == geometry.end())
        {
          geometry[k].geo = manager->CreateUnitLayer(i);
        }
        GeoToPoints & geo = geometry[k];
        point loc;
        manager->GetBaseCenter(loc.xyz);
        double h = manager->GetHeight();
        cmbNucRender::scale scale(1,1,h);
        geo.points.push_back(GeoToPoints::data(loc, rotate, mat, scale));
      }
      //TODO: handle cell material
      if(bl != NULL && !pincell->cellMaterialSet())
      {
        double thickness = 0;
        double bias = bl->Bias;
        //HACK TO HANDLE LESS than one
        if(bias < 2) bias = 1+0.5*(bias);
        int at = manager->GetNumberOfLayers()-1;
        double max = pow(bl->Intervals, bias);

        for(int inter = 0; inter < bl->Intervals; ++inter)
        {
          thickness = bl->Thickness*pow(inter+1,bias)/max;
          key k(manager->GetResolution(at),
                manager->GetTopRadius(at, 0) + thickness,
                manager->GetTopRadius(at, 1) + thickness,
                manager->GetTopRadius(at, 0) + thickness,
                manager->GetTopRadius(at, 1) + thickness,
                manager->GetBaseRadius(at, 0) + thickness,
                manager->GetBaseRadius(at,1) + thickness,
                manager->GetBaseRadius(at, 0) + thickness,
                manager->GetBaseRadius(at,1) + thickness, true);
          if(geometry.find(k) == geometry.end())
          {
            geometry[k].geo = manager->CreateBoundaryLayer( thickness, at );
          }
          GeoToPoints & geo = geometry[k];
          point loc;
          manager->GetBaseCenter(loc.xyz);
          double h = manager->GetHeight();
          cmbNucRender::scale scale(1,1,h);
          geo.points.push_back(GeoToPoints::data(loc, rotate, bl->interface_material, scale, true));
        }
      }
    }
  }

  static void createGeo(cmbNucAssembly * input,
                        Lattice::CellDrawMode mode,
                        std::vector<cmbNucCore::boundaryLayer*> const& bl,
                        std::map<key,GeoToPoints> & geometry)
  {
    std::map< CellKey, std::vector<point> > idToPoint;
    double pitchX = input->getPitchX();
    double pitchY = input->getPitchY();
    calculatePoints(input, idToPoint, false);
    point xformR;
    point xformS;
    bool hasSectioning = false;
    std::vector<point> sectioningPlanes;
    static int sectionedCount = 0;
    //TODO support more than one
    cmbNucCore::boundaryLayer* usedBL = NULL;
    for(size_t i = 0; i < bl.size(); ++i)
    {
      if(input->has_boundary_layer_interface(bl[i]->interface_material))
      {
        usedBL = bl[i];
        break;
      }
    }
    if(input->getLattice().getFullCellMode() == Lattice::HEX_FULL_30)
    {
      xformR.xyz[2] += 30;
      xformS.xyz[2] -= 30;
    }
    {
      double v = input->getZAxisRotation();
      xformR.xyz[2] += v;
      xformS.xyz[2] -= v;
    }

    hasSectioning = true;
    double s = 1;
    int inc = 0;
    switch(mode)
    {
      case Lattice::HEX_TWELFTH_CENTER:
        inc = 1;
        s = -1;
      case Lattice::HEX_SIXTH_FLAT_CENTER:
      case Lattice::HEX_SIXTH_VERT_CENTER:
      {
        point plane;
        plane.xyz[1] = 1;
        transformNormal(plane.xyz, xformS.xyz);
        sectioningPlanes.push_back(plane);
        plane.xyz[(inc+1)%2] = s*-0.5;
        plane.xyz[inc%2] = s*cmbNucMathConst::cos30;
        transformNormal(plane.xyz, xformS.xyz);
        sectioningPlanes.push_back(plane);
        break;
      }
      case Lattice::HEX_SIXTH_FLAT_BOTTOM:
      case Lattice::HEX_SIXTH_VERT_BOTTOM:
      case Lattice::HEX_TWELFTH_BOTTOM:
      {
        point plane;
        plane.xyz[1] = 1;
        transformNormal(plane.xyz, xformS.xyz);
        sectioningPlanes.push_back(plane);
        break;
      }
      case Lattice::HEX_TWELFTH_TOP:
        inc = 1;
        s = -1;
      case Lattice::HEX_SIXTH_FLAT_TOP:
      case Lattice::HEX_SIXTH_VERT_TOP:
      {
        point plane;
        plane.xyz[(1+inc)%2] = s*-0.5;
        plane.xyz[inc%2] = s*cmbNucMathConst::cos30;
        transformNormal(plane.xyz, xformS.xyz);
        sectioningPlanes.push_back(plane);
        break;
      }
      default:
        hasSectioning = false;
    }

    //Duct
    {
      std::map<key, GeoToPoints> tmpGeo;
      //prepopulate the keys
      for(std::map<key, GeoToPoints>::const_iterator i = geometry.begin(); i != geometry.end(); ++i)
      {
        tmpGeo[i->first].geo = i->second.geo;
      }
      createGeo(&(input->getAssyDuct()), input->IsHexType(), tmpGeo, usedBL);
      std::vector<point> points(1);

      if(hasSectioning)
      {
        for(std::map<key, GeoToPoints>::iterator i = tmpGeo.begin(); i != tmpGeo.end(); ++i)
        {
          //NOTE: assuming _|_ to z axis.
          if(i->second.points.empty()) continue;
          key k = i->first;
          k.type = cmbNucRender::key::Sectioned;
          k.sides = sectionedCount++;
          GeoToPoints data = i->second;
          for(unsigned int j = 0; j < sectioningPlanes.size(); ++j)
          {
            point plane = sectioningPlanes[j];
            double tmXf[3] = {-data.points[0].rotation.xyz[0],
                              -data.points[0].rotation.xyz[1],
                              -data.points[0].rotation.xyz[2]};
            cmbNucRenderHelper::transformNormal(plane.xyz, tmXf);
            clip(data.geo, data.geo, plane.xyz, 0);
          }
          addGeometry( k, data, points, geometry, xformR );
        }
      }
      else
      {
        mergeGeometry(tmpGeo, points, geometry, xformR);
      }
    }
    //pins
    for(std::map< CellKey, std::vector<point> >::const_iterator iter = idToPoint.begin();
        iter != idToPoint.end(); ++iter)
    {
      std::map<key, GeoToPoints> tmpGeo;
      //prepopulate the keys
      for(std::map<key, GeoToPoints>::const_iterator i = geometry.begin(); i != geometry.end(); ++i)
      {
        tmpGeo[i->first].geo = i->second.geo;
      }

      createGeo(dynamic_cast<PinCell*>(iter->first.part), input->IsHexType(),
                tmpGeo, usedBL, pitchX, pitchY);

      std::vector<point> const& points = iter->second;
      if(hasSectioning)
      {
        for(std::map<key, GeoToPoints>::iterator i = tmpGeo.begin(); i != tmpGeo.end(); ++i)
        {
          double radius = -1;
          for(unsigned int k = 0; k < 8; ++k)
          {
            radius = std::max(radius, i->first.radius[k]);
          }
          if(radius == -1) radius = 1;
          GeoToPoints const& data = i->second;
          GeoToPoints keep;
          keep.geo = data.geo;
          //NOTE: assuming _|_ to z axis.
          for(unsigned int pt = 0; pt < data.points.size(); ++pt)
          {
            GeoToPoints::data dp = data.points[pt];
            double currentR = radius * std::max(dp.ptScale.xyz[0],dp.ptScale.xyz[1]);
            for(unsigned int z = 0; z < points.size(); ++z)
            {
              GeoToPoints::data cp = computeData( dp, points[z], 0);
              enum {KEEP_PT, CROPPED_PT, IGNORE_PT} modePt = KEEP_PT;
              key k = i->first;
              k.type = cmbNucRender::key::Sectioned;
              k.sides = sectionedCount++;
              GeoToPoints ndp;
              ndp.geo = data.geo;
              for(unsigned int j = 0; j < sectioningPlanes.size(); ++j)
              {
                vtkNew<vtkPlane> testPlane;
                point plane = sectioningPlanes[j];
                testPlane->SetOrigin(0, 0, 0);
                testPlane->SetNormal(plane.xyz[0], plane.xyz[1], plane.xyz[2]);
                double tpdist = testPlane->EvaluateFunction(cp.pt.xyz);
                if( tpdist < -currentR )
                {
                  modePt = IGNORE_PT;
                  continue;
                }
                else if( std::abs(tpdist) <= currentR && modePt != IGNORE_PT )
                {
                  modePt = CROPPED_PT;
                  double tmXf[3] = {-dp.rotation.xyz[0],
                                    -dp.rotation.xyz[1],
                                    -dp.rotation.xyz[2]};
                  transformNormal(plane.xyz, tmXf);
                  clip(ndp.geo, ndp.geo, plane.xyz, 1);
                }
              }
              switch(modePt)
              {
                case KEEP_PT:
                  keep.points.push_back(cp);
                  break;
                case CROPPED_PT:
                  ndp.points.push_back(cp);
                  addGeometry( k, ndp, std::vector<point>(1), geometry, xformR);
                  break;
                default: break;//do nothing
              }
            }
          }
          addGeometry( i->first, keep, std::vector<point>(1), geometry, xformR);
        }
      }
      else
      {
        mergeGeometry(tmpGeo, points, geometry, xformR);
      }
      //start of delete
    }
  }

  static void createGeo(cmbNucCore * input, bool drawBoundryLayer,
                        std::map<key, GeoToPoints> & geometry)
  {
    std::map< CellKey, std::vector<point> > idToPoint;
    calculatePoints(input, idToPoint, input->getVessel().generate());

    for( std::map< CellKey, std::vector<point> >::const_iterator iter = idToPoint.begin();
        iter != idToPoint.end(); ++iter)
    {
      cmbNucPart * part = iter->first.part;
      cmbNucAssembly* assembly = NULL;
      if(part != NULL)
      {
        if(part->GetType() == CMBNUC_ASSEMBLY)
          assembly = dynamic_cast<cmbNucAssembly*>(part);
        else if(part->GetType() == CMBNUC_ASSEMBLY_LINK)
          assembly = dynamic_cast<cmbNucAssemblyLink*>(part)->getLink();
      }
      std::map<key, GeoToPoints> tmpGeo;
      if(assembly)
      {
        //prepopulate the keys
        for(std::map<key, GeoToPoints>::const_iterator i = geometry.begin(); i != geometry.end(); ++i)
        {
          tmpGeo[i->first].geo = i->second.geo;
        }
        if(drawBoundryLayer)
        {
          createGeo(assembly, iter->first.mode, input->getBoundaryLayers(), tmpGeo);
        }
        else
        {
          createGeo(assembly, iter->first.mode, std::vector< cmbNucCore::boundaryLayer*>(), tmpGeo);
        }
      }
      else if((part == NULL || part->getLabel() == "XX") && input->getVessel().generate())
      {
        point xformR = point();
        std::map<key, GeoToPoints> dGeo;
        for(std::map<key, GeoToPoints>::const_iterator i = geometry.begin(); i != geometry.end(); ++i)
        {
          dGeo[i->first].geo = i->second.geo;
        }
        DuctCell dc;
        double dt1, dt2, l = input->GetDefaults()->getHeight(), z0 = input->GetDefaults()->getZ0();
        input->GetDefaults()->getDuctThickness(dt1,dt2);
        dc.AddDuct(new Duct(l, dt1, dt2));
        dc.setZ0(z0);
        createGeo(&dc, input->IsHexType(), dGeo);
        if(input->IsHexType() &&
           !( input->getLattice().GetGeometrySubType() & ANGLE_60 &&
              input->getLattice().GetGeometrySubType() & VERTEX) )
        {
          xformR.xyz[2] = 30;
        }
        std::vector<point> pts(1);
        mergeGeometry(dGeo, pts, tmpGeo, xformR);
      }
      else
      {
        continue;
      }

      std::vector<point> const& points = iter->second;
      mergeGeometry(tmpGeo, points, geometry);
    }
  }

  static void transform(double * xyz, point xformR)
  {
    if(xformR.xyz[0] != 0 || xformR.xyz[1] != 0 || xformR.xyz[2] != 0)
    {
      vtkSmartPointer<vtkTransform> xform = vtkSmartPointer<vtkTransform>::New();
      xform->RotateX(xformR.xyz[0]);
      xform->RotateY(xformR.xyz[1]);
      xform->RotateZ(xformR.xyz[2]);
      double tp[3];
      xform->TransformVector(xyz,tp);
      xyz[0] = tp[0];
      xyz[1] = tp[1];
      xyz[2] = tp[2];
    }
  }

  static void transformNormal(double * xyz, double * xformR)
  {
    if(xformR[0] != 0 || xformR[1] != 0 || xformR[2] != 0)
    {
      vtkSmartPointer<vtkTransform> xform = vtkSmartPointer<vtkTransform>::New();
      xform->RotateX(xformR[0]);
      xform->RotateY(xformR[1]);
      xform->RotateZ(xformR[2]);
      double tp[3];
      xform->TransformNormal(xyz,tp);
      xyz[0] = tp[0];
      xyz[1] = tp[1];
      xyz[2] = tp[2];
      normalize(xyz);
    }
  }

  static void normalize(double *xyz)
  {
    double sum = std::sqrt(xyz[0]*xyz[0]+ xyz[1]*xyz[1] + xyz[2]*xyz[2]);
    if(sum == 0) return;
    xyz[0] /= sum;
    xyz[1] /= sum;
    xyz[2] /= sum;
  }

  static GeoToPoints::data computeData( GeoToPoints::data const& pt, point const& apt,
                                        point xformR = point())
  {
    point tranpt = pt.pt;
    tranpt.xyz[0] += apt.xyz[0];
    tranpt.xyz[1] += apt.xyz[1];
    transform(tranpt.xyz, xformR);
    point rotation = pt.rotation;
    rotation.xyz[0] += xformR.xyz[0];
    rotation.xyz[1] += xformR.xyz[1];
    rotation.xyz[2] += xformR.xyz[2];
    return GeoToPoints::data(tranpt,rotation, pt.material, pt.ptScale, pt.boundaryLayer);
  }

  static void addGeometry( key keyIn, GeoToPoints const& tmpGeoI,
                           std::vector<point> const& points,
                           std::map<key, GeoToPoints> & geometry,
                           point xformR = point())
  {
    GeoToPoints &newGeo = geometry[keyIn];
    if(newGeo.geo == NULL)
    {
      newGeo.geo = tmpGeoI.geo;
    }

    size_t at = newGeo.points.size();
    newGeo.points.resize(newGeo.points.size() + points.size()*tmpGeoI.points.size());
    for(size_t j = 0; j<points.size(); ++j)
    {
      point const& apt = points[j];
      for(size_t k = 0; k < tmpGeoI.points.size(); ++k)
      {
        newGeo.points[at] = computeData( tmpGeoI.points[k], apt, xformR);
        ++at;
      }
    }
  }

  static void mergeGeometry(std::map<key, GeoToPoints> const& in,
                            std::vector<point> const& points,
                            std::map<key, GeoToPoints> & geometry,
                            point xformR = point())
  {
    for(std::map<key, GeoToPoints>::const_iterator i = in.begin(); i != in.end(); ++i)
    {
      addGeometry( i->first, i->second, points, geometry, xformR);
    }
  }

  static void clip(vtkSmartPointer<vtkPolyData> in,
                   vtkSmartPointer<vtkPolyData> &out,
                   double *normal, int offset = 0)
  {
    vtkSmartPointer<vtkPolyData> tmpIn = in;
    out = vtkSmartPointer<vtkPolyData>::New();
    vtkNew<vtkPlane> plane;
    plane->SetOrigin(-normal[0]*0.005*offset, -normal[1]*0.005*offset, -normal[2]*0.005*offset);
    plane->SetNormal(normal[0], normal[1], normal[2]);

    vtkNew<vtkClipClosedSurface> clipper;
    vtkNew<vtkPlaneCollection> clipPlanes;
    vtkNew<vtkPolyDataNormals> normals;
    clipPlanes->AddItem(plane.GetPointer());
    clipper->SetClippingPlanes(clipPlanes.GetPointer());
    clipper->SetActivePlaneId(0);
    clipper->SetClipColor(1.0,1.0,1.0);
    clipper->SetActivePlaneColor(1.0,1.0,0.8);
    clipper->GenerateOutlineOff();
    clipper->SetInputData(tmpIn);
    clipper->GenerateFacesOn();
    normals->SetInputConnection(clipper->GetOutputPort());
    normals->Update();
    out->DeepCopy(normals->GetOutput());
  }

  static void addOuterJacket(cmbNucCore * core, std::map<key, GeoToPoints> & geometry)
  {
    if(!core->getVessel().generate())
    {
      return;
    }

    double r = core->getVessel().getCylinderRadius();

    int s = core->getVessel().getCylinderOuterSpacing();

    vtkSmartPointer<vtkCmbLayeredConeSource> cylinder = vtkSmartPointer<vtkCmbLayeredConeSource>::New();

    double outerDuctWidth;
    double outerDuctHeight;
    core->GetDefaults()->getDuctThickness(outerDuctWidth, outerDuctHeight);

    double extraXaTrans = 0, extraYaTrans = 0;

    if(core->IsHexType())
    {
      double odc = outerDuctHeight*core->getLattice().getSize();
      double tmp = odc - outerDuctHeight;
      double t2 = tmp*0.5;
      double ty = std::sqrt(tmp*tmp-t2*t2);
      extraYaTrans = -ty;
      extraXaTrans = odc;
    }
    else
    {
      double w = core->getLattice().getSize(0);
      double h = core->getLattice().getSize();
      double tx = outerDuctWidth*(w-1)*0.5;
      double ty = outerDuctHeight*(h-1)*0.5;
      extraXaTrans = tx;//-outerDuctWidth*(core->getLattice().getSize()-1);
      extraYaTrans = ty - outerDuctHeight*(h-1);
    }

    cylinder->SetHeight(1);
    cylinder->SetNumberOfLayers(1);
    cylinder->SetTopRadius(0, r);
    cylinder->SetBaseRadius(0, r);
    cylinder->SetResolution(0, s);
    double odc = outerDuctHeight*(core->getLattice().getSize()-1);
    double tmp = (outerDuctHeight*0.5) / (cos(30.0 * vtkMath::Pi() / 180.0));
    //todo replace this
    static const double a[6][2] =
    { { cos( 2*(vtkMath::Pi() / 6.0) * (5 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (5 + 3) ) },
      { cos( 2*(vtkMath::Pi() / 6.0) * (4 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (4 + 3) ) },
      { cos( 2*(vtkMath::Pi() / 6.0) * (3 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (3 + 3) ) },
      { cos( 2*(vtkMath::Pi() / 6.0) * (2 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (2 + 3) ) },
      { cos( 2*(vtkMath::Pi() / 6.0) * (1 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (1 + 3) ) },
      { cos( 2*(vtkMath::Pi() / 6.0) * (0 + 3) ), sin( 2*(vtkMath::Pi() / 6.0) * (0 + 3) ) } };
    double pt1[] = {odc * a[5][0], odc * a[5][1]};
    double pt2[2];

    static const double AssyCosSinAngles[6][2] =
    { { cos( (vtkMath::Pi() / 3.0) * (0 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (0 + 3.5) ) },
      { cos( (vtkMath::Pi() / 3.0) * (1 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (1 + 3.5) ) },
      { cos( (vtkMath::Pi() / 3.0) * (2 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (2 + 3.5) ) },
      { cos( (vtkMath::Pi() / 3.0) * (3 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (3 + 3.5) ) },
      { cos( (vtkMath::Pi() / 3.0) * (4 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (4 + 3.5) ) },
      { cos( (vtkMath::Pi() / 3.0) * (5 + 3.5) ), sin( (vtkMath::Pi() / 3.0) * (5 + 3.5) ) } };
    if(core->IsHexType())
    {
      for(int i = 0; i < 6; ++i)
      {
        int at = 5-i-1;
        if(at<0) at = 5;
        pt2[0] = odc * a[at][0];
        pt2[1] = odc * a[at][1];
        int sp1 = (i + 0)%6;
        int sp2 = (i + 1)%6;
        if(core->getLattice().getSize() == 1)
        {
          cylinder->addInnerPoint(pt1[0]+tmp * AssyCosSinAngles[sp1][0],
                                  pt1[1]+tmp * AssyCosSinAngles[sp1][1]);
        }
        else for(size_t j = 0; j < core->getLattice().getSize(); ++j)
        {
          double tmps = j/(core->getLattice().getSize()-1.0);
          double pt[] = {pt1[0]*(1.0-tmps)+pt2[0]*(tmps), pt1[1]*(1.0-tmps)+pt2[1]*(tmps)};

          {
            cylinder->addInnerPoint(pt[0]+tmp * AssyCosSinAngles[sp1][0],
                                    pt[1]+tmp * AssyCosSinAngles[sp1][1]);
          }
          if(j != core->getLattice().getSize()-1 )
          {
            cylinder->addInnerPoint(pt[0]+tmp * AssyCosSinAngles[sp2][0],
                                    pt[1]+tmp * AssyCosSinAngles[sp2][1]);
          }
        }

        pt1[0] = pt2[0];
        pt1[1] = pt2[1];
      }
    }
    else
    {
      double h = core->getLattice().getSize();
      double w = core->getLattice().getSize(0);
      double width = outerDuctWidth*(w)*0.5;
      double height = outerDuctHeight*(h)*0.5;

      cylinder->addInnerPoint( width,-height);
      cylinder->addInnerPoint( width, height);
      cylinder->addInnerPoint(-width, height);
      cylinder->addInnerPoint(-width,-height);
      
    }
    double bc[] = {extraXaTrans, extraYaTrans, 0};
    cylinder->SetBaseCenter(bc);

    point loc;
    cylinder->GetBaseCenter(loc.xyz);
    double z0 = core->GetDefaults()->getZ0();
    loc.xyz[2] += z0;

    key k;
    GeoToPoints & geo = geometry[k];
    geo.geo = cylinder->CreateUnitLayer(0);
    double h = core->GetDefaults()->getHeight();
    cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
    geo.points.push_back(GeoToPoints::data( loc, point(),matColorMap->getUnknownMaterial(),
                                            cmbNucRender::scale(1,1,h)));

  }
};

cmbNucRender::cmbNucRender()
 :GlyphMapper(vtkSmartPointer<vtkGlyph3DMapper>::New()),
  GlyphActor(vtkSmartPointer<vtkActor>::New()),
  TransparentMapper(vtkSmartPointer<vtkGlyph3DMapper>::New()),
  TransparentActor(vtkSmartPointer<vtkActor>::New())
{

  this->GlyphActor->SetMapper(this->GlyphMapper.GetPointer());
  this->GlyphActor->GetProperty()->SetShading(1);
  this->GlyphActor->GetProperty()->SetInterpolationToPhong();

  this->TransparentActor->SetMapper(this->TransparentMapper.GetPointer());
  this->TransparentActor->GetProperty()->SetShading(1);
  this->TransparentActor->GetProperty()->SetInterpolationToPhong();
  this->TransparentActor->GetProperty()->SetOpacity(0.99);
}

cmbNucRender::~cmbNucRender()
{
}

void cmbNucRender::addToRender(vtkSmartPointer<vtkRenderer> renderer)
{
  renderer->AddActor(this->GlyphActor);
  renderer->AddActor(this->TransparentActor);
}

void cmbNucRender::clearMappers()
{
  GlyphMapper->RemoveAllInputConnections(1);
  GlyphMapper->SetInputData(NULL);
  TransparentMapper->RemoveAllInputConnections(1);
  TransparentMapper->SetInputData(NULL);
}

void cmbNucRender::render(cmbNucCore * core, bool renderBoundaryLayer)
{
  std::map<key, GeoToPoints> geometry;
  cmbNucRenderHelper::createGeo(core, renderBoundaryLayer, geometry);
  cmbNucRenderHelper::addOuterJacket(core, geometry);
  BoundingBox = core->computeBounds();
  sendToGlyphMappers(geometry);
}

void cmbNucRender::render(cmbNucAssembly * assy,
                          std::vector<cmbNucCore::boundaryLayer*> const& bl)
{
  std::map<key, GeoToPoints> geometry;
  cmbNucRenderHelper::createGeo(assy,
                                assy->getLattice().getFullCellMode(),
                                bl, geometry);
  BoundingBox = assy->computeBounds();
  sendToGlyphMappers(geometry);
}

void cmbNucRender::render(DuctCell* ductCell, bool isHex, bool cutaway)
{
  std::map<key, GeoToPoints> geometry;
  cmbNucRenderHelper::createGeo(ductCell, isHex, geometry);
  if(cutaway)
  {
    double normal[] = {0, 1, 0};
    for(std::map<key, GeoToPoints>::iterator iter = geometry.begin(); iter != geometry.end(); ++iter)
    {
      cmbNucRenderHelper::clip(iter->second.geo,iter->second.geo,normal);
    }
  }
  BoundingBox = ductCell->computeBounds(isHex);
  sendToGlyphMappers(geometry);
}

void cmbNucRender::render(PinCell* pinCell, bool isHex, bool cutaway)
{
  std::map<key, GeoToPoints> geometry;
  cmbNucRenderHelper::createGeo(pinCell, isHex, geometry);
  if(cutaway)
  {
    for(std::map<key, GeoToPoints>::iterator iter = geometry.begin(); iter != geometry.end(); ++iter)
    {
      double normal[] = {0, 1, 0};
      double xform[] = { -iter->second.points[0].rotation.xyz[0],
                         -iter->second.points[0].rotation.xyz[1],
                         -iter->second.points[0].rotation.xyz[2] };

      cmbNucRenderHelper::transformNormal(normal, xform);
      cmbNucRenderHelper::clip(iter->second.geo, iter->second.geo, normal);
    }
  }
  BoundingBox = pinCell->computeBounds(isHex);
  sendToGlyphMappers(geometry);
}

void cmbNucRender::computeBounds(vtkBoundingBox & in)
{
  in = BoundingBox;
}

void cmbNucRender::setZScale(double v)
{
  this->GlyphActor->SetScale(1, 1, v);
  this->TransparentActor->SetScale(1, 1, v);
}

void cmbNucRender::sendToGlyphMappers(std::map<key, GeoToPoints> & geometry)
{
  clearMappers();
  vtkSmartPointer<vtkPoints> polyPoints = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPoints> polyPointsTrans = vtkSmartPointer<vtkPoints>::New();

  vtkSmartPointer<vtkIntArray> polyScalers = vtkSmartPointer<vtkIntArray>::New();
  vtkSmartPointer<vtkIntArray> polyScalersTrans = vtkSmartPointer<vtkIntArray>::New();

  vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
  vtkSmartPointer<vtkUnsignedCharArray> colorsTrans = vtkSmartPointer<vtkUnsignedCharArray>::New();

  vtkSmartPointer<vtkDoubleArray> rotationAngles = vtkSmartPointer<vtkDoubleArray>::New();
  vtkSmartPointer<vtkDoubleArray> rotationAnglesTrans = vtkSmartPointer<vtkDoubleArray>::New();

  vtkSmartPointer<vtkDoubleArray> scaleGlyph = vtkSmartPointer<vtkDoubleArray>::New();
  vtkSmartPointer<vtkDoubleArray> scaleTrans = vtkSmartPointer<vtkDoubleArray>::New();

  colors->SetName("colors");
  colors->SetNumberOfComponents(3);
  colorsTrans->SetName("colors");
  colorsTrans->SetNumberOfComponents(4);


  polyScalers->SetNumberOfComponents(1);
  polyScalers->SetName("Ids");
  polyScalersTrans->SetNumberOfComponents(1);
  polyScalersTrans->SetName("Ids");

  rotationAngles->SetNumberOfComponents(3);
  rotationAngles->SetName("Rotation");
  rotationAnglesTrans->SetNumberOfComponents(3);
  rotationAnglesTrans->SetName("Rotation");

  scaleGlyph->SetNumberOfComponents(3);
  scaleGlyph->SetName("Scale");
  scaleTrans->SetNumberOfComponents(3);
  scaleTrans->SetName("Scale");

  int index = 0;
  int indexTrans = 0;
  for(std::map<key, GeoToPoints>::const_iterator iter = geometry.begin(); iter != geometry.end(); ++iter)
  {
    bool added_to_trans = false;
    bool added_to_solid = false;

    GeoToPoints const& geo = iter->second;
    for(unsigned int j = 0; j < geo.points.size(); ++j)
    {
      geo.points[j].material->setDisplayed();
      if(!geo.points[j].material->isVisible()) continue;
      QColor bColor =
          (geo.points[j].boundaryLayer)?
            (geo.points[j].material->getColor().lightnessF() < 0.5) ?
                Qt::white : Qt::black
            : geo.points[j].material->getColor();
      unsigned char color[] = { static_cast<unsigned char>(bColor.redF()*255),
                                static_cast<unsigned char>(bColor.greenF()*255),
                                static_cast<unsigned char>(bColor.blueF()*255),
                                static_cast<unsigned char>(bColor.alphaF()*255) };
      vtkSmartPointer<vtkPoints> toutpts = polyPoints;
      vtkSmartPointer<vtkIntArray> toutsc = polyScalers;
      vtkSmartPointer<vtkUnsignedCharArray> toutc = colors;
      vtkSmartPointer<vtkDoubleArray> toutr = rotationAngles;
      vtkSmartPointer<vtkDoubleArray> tscale = scaleGlyph;
      int at = index-1;

      if(bColor.alphaF() == 0) continue;


      if(bColor.alphaF() != 1)
      {
        if(!added_to_trans)
        {
          added_to_trans = true;
          TransparentMapper->SetSourceData(indexTrans++, geo.geo);
        }
        at = indexTrans - 1;
        toutpts = polyPointsTrans;
        toutsc = polyScalersTrans;
        toutc = colorsTrans;
        toutr = rotationAnglesTrans;
        tscale = scaleTrans;
      }
      else if(!added_to_solid)
      {
        added_to_solid = true;
        at = index;
        GlyphMapper->SetSourceData(index++, geo.geo);
      }
      toutpts->InsertNextPoint(geo.points[j].pt.xyz);
      toutr->InsertNextTuple(geo.points[j].rotation.xyz);
      toutsc->InsertNextValue(at);
      toutc->InsertNextTupleValue(color);
      tscale->InsertNextTuple(geo.points[j].ptScale.xyz);
    }
  }

  vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
  polydata->SetPoints(polyPoints);
  polydata->GetPointData()->AddArray(polyScalers);
  polydata->GetPointData()->AddArray(rotationAngles);
  polydata->GetPointData()->AddArray(scaleGlyph);
  polydata->GetPointData()->SetScalars(colors);
  GlyphMapper->ScalingOn();
  GlyphMapper->ClampingOff();
  GlyphMapper->SetScaleArray("Scale");
  GlyphMapper->SetScaleModeToScaleByVectorComponents();
  GlyphMapper->SetInputData(polydata);
  GlyphMapper->SourceIndexingOn();
  GlyphMapper->SetSourceIndexArray("Ids");
  GlyphMapper->SetRange(0,index);
  GlyphMapper->OrientOn();
  GlyphMapper->SetOrientationModeToRotation();
  GlyphMapper->SetOrientationArray("Rotation");

  vtkSmartPointer<vtkPolyData> polydataTrans = vtkSmartPointer<vtkPolyData>::New();
  polydataTrans->SetPoints(polyPointsTrans);
  polydataTrans->GetPointData()->AddArray(polyScalersTrans);
  polydataTrans->GetPointData()->AddArray(rotationAnglesTrans);
  polydataTrans->GetPointData()->SetScalars(colorsTrans);
  polydataTrans->GetPointData()->AddArray(scaleTrans);
  TransparentMapper->ScalingOn();
  TransparentMapper->ClampingOff();
  TransparentMapper->SetScaleArray("Scale");
  TransparentMapper->SetScaleModeToScaleByVectorComponents();
  TransparentMapper->SetInputData(polydataTrans);
  TransparentMapper->SourceIndexingOn();
  TransparentMapper->SetSourceIndexArray("Ids");
  TransparentMapper->SetRange(0,indexTrans);
  TransparentMapper->OrientOn();
  TransparentMapper->SetOrientationModeToRotation();
  TransparentMapper->SetOrientationArray("Rotation");

  BoundingBox.AddBounds(polydataTrans->GetBounds());
  BoundingBox.AddBounds(polydata->GetBounds());
}

vtkSmartPointer<vtkCmbLayeredConeSource>
cmbNucRender::CreateLayerManager(PinCell* pincell, bool isHex, size_t j,
                                 double pitchX, double pitchY)
{
  PinSubPart* part = pincell->GetPart(static_cast<int>(j));
  
  vtkSmartPointer<vtkCmbLayeredConeSource> coneSource =
      vtkSmartPointer<vtkCmbLayeredConeSource>::New();
  coneSource->SetNumberOfLayers(pincell->GetNumberOfLayers() +
                                (pincell->cellMaterialSet()?1:0));
  coneSource->SetBaseCenter(0, 0, part->getZ1());
  coneSource->SetHeight(part->getZ2() - part->getZ1());
  double largestRadius = 0;

  for(int k = 0; k < pincell->GetNumberOfLayers(); k++)
  {
    coneSource->SetBaseRadius(k, part->getRadius(k,Frustum::BOTTOM));
    coneSource->SetTopRadius(k, part->getRadius(k,Frustum::TOP));
    coneSource->SetResolution(k, PinCellResolution);
    if(largestRadius < part->getRadius(k,Frustum::BOTTOM))
    {
      largestRadius = part->getRadius(k,Frustum::BOTTOM);
    }
    if(largestRadius < part->getRadius(k,Frustum::TOP))
    {
      largestRadius = part->getRadius(k,Frustum::TOP);
    }
  }
  if(pincell->cellMaterialSet())
  {
    largestRadius *= 2.50;
    if(pitchX == -1 || pitchY == -1)
    {
      pitchX = pitchY = largestRadius;
    }
    double r[] = {pitchX*0.5, pitchY*0.5};
    int res = 4;
    if(isHex)
    {
      res = 6;
      r[0] = r[1] = r[0]/cmbNucMathConst::cos30;
    }
    coneSource->SetBaseRadius(pincell->GetNumberOfLayers(), r[0], r[1]);
    coneSource->SetTopRadius(pincell->GetNumberOfLayers(), r[0], r[1]);
    coneSource->SetResolution(pincell->GetNumberOfLayers(), res);
  }
  double direction[] = { 0, 0, 1 };
  coneSource->SetDirection(direction);
  return coneSource;
}

vtkSmartPointer<vtkCmbLayeredConeSource>
cmbNucRender::CreateLayerManager(DuctCell* ductCell, bool isHex, size_t i)
{
  Duct *duct = ductCell->getDuct(static_cast<int>(i));
  double z = duct->getZ1();
  double height = duct->getZ2() - duct->getZ1();
  double deltaZ = height * 0.0005;
  if(i == 0) // first duct
  {
    z = duct->getZ1() + deltaZ;
    // if more than one duct, first duct height need to be reduced by deltaZ
    height = ductCell->numberOfDucts() > 1 ? height - deltaZ : height - 2*deltaZ;
  }
  else if (i == ductCell->numberOfDucts() - 1) // last duct
  {
    height -= 2*deltaZ;
  }
  else
  {
    z = duct->getZ1() + deltaZ;
  }

  int numLayers = static_cast<int>(duct->NumberOfLayers());
  vtkSmartPointer<vtkCmbLayeredConeSource> coneSource =
     vtkSmartPointer<vtkCmbLayeredConeSource>::New();
  coneSource->SetNumberOfLayers(numLayers);
  coneSource->SetBaseCenter(duct->getX(), duct->getY(), z);
  double direction[] = { 0, 0, 1 };
  coneSource->SetDirection(direction);
  coneSource->SetHeight(height);

  int res = 4;
  double mult = 0.5;

  if(isHex)
  {
    res = 6;
    mult = 0.5/cmbNucMathConst::cos30;
  }

  for(int k = 0; k < numLayers; k++)
  {
    double tx = duct->GetLayerThick(k,0) - duct->GetLayerThick(k,0)*0.0005;
    double ty = duct->GetLayerThick(k,1) - duct->GetLayerThick(k,0)*0.0005;
    coneSource->SetBaseRadius(k, tx * mult, ty*mult);
    coneSource->SetTopRadius(k, tx * mult, ty*mult);
    coneSource->SetResolution(k, res);
  }
  return coneSource;
}
