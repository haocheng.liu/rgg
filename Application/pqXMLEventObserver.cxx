/*=========================================================================

   Program: ParaView
   Module:    pqXMLEventObserver.cxx

   Copyright (c) 2005-2008 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2. 

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

#include "pqXMLEventObserver.h"

#include <QTextStream>
#include <QDebug>
#include <QMouseEvent>

/// Escapes strings so they can be embedded in an XML document
static const QString textToXML(const QString& string)
{
  QString result = string;
  result.replace("&", "&amp;");  // keep first
  result.replace("<", "&lt;");
  result.replace(">", "&gt;");
  result.replace("'", "&apos;");
  result.replace("\"", "&quot;");
  result.replace("\n", "&#xA;");
  
  return result;
}

////////////////////////////////////////////////////////////////////////////////////
// pqXMLEventObserver

pqXMLEventObserver::pqXMLEventObserver(QObject* p, QPointer<cmbNucDraw2DLattice> ld)
  : pqEventObserver(p), LatticeDraw(ld)
{
}

pqXMLEventObserver::~pqXMLEventObserver()
{
  this->setStream(NULL);
}

void pqXMLEventObserver::setStream(QTextStream* stream)
{
  if(this->Stream)
  {
    *this->Stream << "</events>\n";
  }
  pqEventObserver::setStream(stream);
  if(this->Stream)
  {
    *this->Stream << "<?xml version=\"1.0\" ?>\n";
    *this->Stream << "<events>\n";
  }
}


void pqXMLEventObserver::onRecordEvent(const QString& Widget, const QString& Command,
                                       const QString& Arguments)
{
  QString w = Widget, c = Command, a = Arguments;
  if(this->Stream)
  {
    if(Widget.contains("qNucMainWindow/Dock2D/LatticeDrawWidget/1QWidget0") &&
       Command == "mousePress")
    {
      QStringList	s = Arguments.split(',');
      int x = s[3].toInt();
      int y = s[4].toInt();
      DrawLatticeItem* dli = LatticeDraw->getItemAt(QPoint(x,y));
      //QPoint qat = LatticeDraw->mapFromScene(LatticeDraw->getLatticeLocation(dli->layer(), dli->cellIndex()).toPoint());
      //QPoint qat = LatticeDraw->mapFromScene(dli->getCentroid().toPoint());
      //DrawLatticeItem * di = LatticeDraw->getItemAt(qat);
      //qDebug() << dli->layer() << dli->cellIndex() << '\t' << x << y
      //         << LatticeDraw->getLatticeLocation(dli->layer(), dli->cellIndex())
      //         << qat << (dli == di);
      if(dli == NULL)
      {
        return;
      }
      Qt::MouseButtons buttons = static_cast<Qt::MouseButton>(s[1].toInt());
      //Qt::KeyboardModifiers keym = static_cast<Qt::KeyboardModifier>(s[2].toInt());
      if(Qt::RightButton == buttons)
      {
        *this->Stream
          << "  <event " << "object=\"qNucMainWindow/Dock2D/LatticeDrawWidget/PopupMenu\" "
          << "command=\"show_menu\" "
          << "arguments=\"" << QString::number(dli->layer()).toLatin1().data() << ","
          << QString::number(dli->cellIndex()).toLatin1().data() << "\" " << "/>\n";
      }
      else
      {
        //not a useful mouse press
        return;
      }
    }
    else
    {
      if(w.contains("qNucMainWindow/SimpleFill"))
      {
        w.replace(QString("qNucMainWindow/"), QString(""));
      }
      *this->Stream
        << "  <event " << "object=\"" << textToXML(w).toLatin1().data() << "\" "
        << "command=\"" << textToXML(c).toLatin1().data() << "\" "
        << "arguments=\"" << textToXML(a).toLatin1().data() << "\" " << "/>\n";
    }
  }
}
