#ifndef __cmbNucInputListWidget_h
#define __cmbNucInputListWidget_h

#include <QWidget>
#include <QTreeWidgetItem>
#include "cmbNucPartDefinition.h"

class cmbNucAssembly;
class cmbNucAssemblyLink;
class cmbNucCore;
class cmbNucPartLibrary;
class cmbNucPart;
class cmbNucInputListWidgetInternal;
class QTreeWidget;
class QTreeWidgetItem;
class cmbNucPartsTreeItem;
class PinCell;
class DuctCell;
class QMenu;

class cmbNucInputListWidget : public QWidget
{
  Q_OBJECT

public:
  cmbNucInputListWidget(QWidget* parent=0);
  virtual ~cmbNucInputListWidget();

  /// set core this widget with be interact with
  void setCore(cmbNucCore*);
  /// get current assembly that this widget is interacting with
  cmbNucAssembly* getCurrentAssembly();
  /// update UI and if selCore is not set,
  /// the last assembly lattice will be selected by default,
  void updateUI(bool selCore);
  /// get selected part object
  cmbNucPart* getSelectedPart();
  cmbNucPartsTreeItem* getSelectedPartNode();

  bool onlyMeshLoaded();

  void clear();

  void clearTable();

  void setPartOptions(QMenu * qm) const;

  void initMaterialsTree();

  void emitCheckSavedAndGenerage()
  {
    emit(checkSavedAndGenerate());
  }

signals:
  // Description:
  // Fired when a part/material is selected in the tree
  void objectSelected(cmbNucPart*, const char* name);
  void objectRemoved();

  void assembliesModified(cmbNucCore*);

  void deleteCore();
  void deleteObj(cmbNucPart*);

  void checkSavedAndGenerate();

  void subMeshSelected(QTreeWidgetItem*);
  void meshValueChanged(QTreeWidgetItem*);
  void majorMeshSelection(int);

  void sendColorControl(int);
  void sendEdgeControl(bool);
  void resetMeshCamera();

  void drawBoundaryControl(bool);

  void raiseMeshDock();
  void raiseModelDock();

public slots:
  void onNewAssembly();
  void onNewAssemblyLink();
  void onNewBlank();
  void valueChanged();
  void onRemoveSelectedPart();
  void meshIsLoaded(bool);
  void modelIsLoaded(bool);
  void selectMeshTab(bool);
  void selectModelTab(bool);
  void updateMainMeshComponents(QStringList parts, int select);
  void updateMeshTable(QList<QTreeWidgetItem*> MeshParts);
  void onNewDuct();
  void repaintList();
  void updateWithPinLibrary();
  void updateWithDuctLibrary();
  void updateWithCoreParts();
  void setBoundaryLayerControlMode(bool);

protected:
  cmbNucPartsTreeItem* getSelectedItem(QTreeWidget* treeWidget);
  void fireObjectSelectedSignal(cmbNucPartsTreeItem* selItem);
  void updateContextMenu(cmbNucPart* selObj,
                         const cmbNucPartsTreeItem* selItem);
  void setActionsEnabled(bool val);
  void updateWithPartLibrary(cmbNucPartLibrary * pl, cmbNucPartsTreeItem* parent);
  void updateWithPart(cmbNucPart* part, bool select=false);
  cmbNucPartsTreeItem* getCurrentAssemblyNode();
  cmbNucPartsTreeItem* getCurrentAssemblyLinkNode();
  void initCoreRootNode();
  void assemblyModified(cmbNucPartsTreeItem* assyNode);
  void coreModified();

private slots:
  // Description:
  // Tree widget interactions related slots
  virtual void onPartsSelectionChanged();
  virtual void onTabChanged(int);
  void onMaterialClicked(QTreeWidgetItem*, int col);

  // Description:
  // Tree widget context menu related slots
  void onNewPin();
  void onImportMaterial();
  void onSaveMaterial();
  void onClone();

  void hideLabels(bool);

signals:
  void deleteAssembly(cmbNucAssembly *, QTreeWidgetItem*);
  void deleteAssemblyLink(QTreeWidgetItem*);

private:
  cmbNucInputListWidgetInternal* Internal;

  /// clear UI
  void initUI();
  void initPartsTree();

  cmbNucCore *NuclearCore;
};
#endif
