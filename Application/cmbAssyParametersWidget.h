#ifndef __cmbAssyParametersWidget_h
#define __cmbAssyParametersWidget_h

#include <QWidget>
#include "cmbNucPartDefinition.h"
#include "cmbNucWidgetChangeChecker.h"
#include <QStringList>
#include <cmbNucCore.h>

class cmbNucAssembly;

class cmbAssyParametersWidget : public QWidget
{
  Q_OBJECT

public:
  cmbAssyParametersWidget(QWidget* p);
  virtual ~cmbAssyParametersWidget();

  // Description:
  // set/get the assembly that this widget with be interact with
  void setAssembly(cmbNucAssembly*, cmbNucCore* core, cmbNucWidgetChangeChecker & checker);
  cmbNucAssembly* getAssembly(){return this->Assembly;}

signals:
  void valuesChanged();

private:

  class cmbAssyParametersWidgetInternal;
  cmbAssyParametersWidgetInternal* Internal;

  void initUI();
  cmbNucAssembly *Assembly;
};
#endif
