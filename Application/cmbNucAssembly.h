#ifndef cmbNucAssembly_H
#define cmbNucAssembly_H

#include <string>
#include <vector>
#include <QObject>
#include <QDebug>
#include <QPointer>

#include "vtkMultiBlockDataSet.h"
#include "cmbNucPart.h"
#include "cmbNucLattice.h"
#include "cmbNucPinCell.h"
#include "cmbNucDuctCell.h"
#include "cmbNucPart.h"

#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkBoundingBox.h"

class vtkCompositeDataDisplayAttributes;
class inpFileReader;
class inpFileHelper;
class inpFileWriter;
class cmbNucDefaults;
class cmbNucPartLibrary;
class cmbNucPartLibrary;


#define ASSYGEN_EXTRA_VARABLE_MACRO() \
FUN_SIMPLE(std::string, QString, StartPinId,               startpinid               ) \
FUN_SIMPLE(std::string, QString, MeshType,                 meshtype                 ) \
FUN_SIMPLE(std::string, QString, CellMaterial,             cellmaterial             ) \
FUN_SIMPLE(std::string, QString, CreateMatFiles,           creatematfiles           ) \
FUN_SIMPLE(std::string, QString, List_NeumannSet_StartId,  list_neumannset_startid  ) \
FUN_SIMPLE(std::string, QString, List_MaterialSet_StartId, list_materialset_startid ) \
FUN_SIMPLE(std::string, QString, NumSuperBlocks,           numsuperblocks           ) \
FUN_SIMPLE(std::string, QString, SuperBlocks,              superblocks              ) \
FUN_SIMPLE(std::string, QString, CreateSideset,            createsideset            ) \
FUN_SIMPLE(int,         QString, EdgeInterval,             edgeinterval             ) \
FUN_SIMPLE(double,      QString, MergeTolerance,           mergetolerance           ) \
FUN_SIMPLE(std::string, QString, MeshScheme,               meshscheme               ) \
FUN_SIMPLE(std::string, QString, Geometry,                 Geometry                 ) \
FUN_SIMPLE(double,      QString, TetMeshSize,              TetMeshSize              ) \
FUN_SIMPLE(double,      QString, RadialMeshSize,           RadialMeshSize           ) \
FUN_SIMPLE(int,         QString, CreateFiles,              CreateFiles              ) \
FUN_SIMPLE(std::string, QString, Info,                     Info                     ) \
FUN_SIMPLE(std::string, QString, HBlock,                   HBlock                   )

class cmbAssyParameters
{
private:
  template<typename T>
  class singleton
  {
  public:
    T value;
    bool valid;
    singleton():value(T()),valid(false)
    { }
    void clear()
    {
      valid = false;
      value = T();
    }
  };
public:
  cmbAssyParameters()
  {
    MoveXYZ[0] = MoveXYZ[1] = MoveXYZ[2] = 0.0;
    Save_Exodus = false;
  }

  void fill(cmbAssyParameters * other);

private:
  double MoveXYZ[3];

#define FUN_SIMPLE(TYPE,X,Var,Key)\
private: \
  singleton<TYPE> Var; \
public: \
  TYPE const& get##Var(){ return Var.value; } \
  void set##Var(TYPE const& in){ Var.valid = true; Var.value = in; } \
  void clear##Var(){ Var.valid = false; } \
  bool isSet##Var(){ return Var.valid; }

  ASSYGEN_EXTRA_VARABLE_MACRO()
  FUN_SIMPLE(std::string, QString, Center,                   Center       )
  FUN_SIMPLE(double,      QString, AxialMeshSize,            AxialMeshSize)

#undef FUN_SIMPLE

  bool isSetMoveXYZ() const { return MoveXYZ[0] != 0 || MoveXYZ[1] || MoveXYZ[2] != 0; }
  double * getMoveXYZ() {return MoveXYZ;}

#define MOVE_XYZ_ACCESS(NAME, INDEX) \
  bool isSetMove##NAME() const { return true; } \
  double getMove##NAME() const { return MoveXYZ[INDEX]; }\
  void setMove##NAME(double in) { MoveXYZ[INDEX] = in; } \
  void clearMove##NAME() { MoveXYZ[INDEX] = 0; }

  MOVE_XYZ_ACCESS(X, 0)
  MOVE_XYZ_ACCESS(Y, 1)
  MOVE_XYZ_ACCESS(Z, 2)

private:
  bool Save_Exodus;
public:
  bool const& getSave_Exodus(){ return Save_Exodus; }
  void setSave_Exodus(bool const& in){ Save_Exodus = in; }
  void clearSave_Exodus(){ Save_Exodus = false; }
  bool isSetSave_Exodus(){ return true; }
#undef FUN_SIMPLE

private:
  QString UnknownParams;
public:
  QString const& getUnknownParams(){ return UnknownParams; }
  void setUnknownParams(QString const& in){ UnknownParams = in; }
  void clearUnknownParams(){ UnknownParams = QString(); }
  bool isSetUnknownParams(){ return true; }

private:
  int neumannSetStartId, materialSetStartId;
public:
  bool isSetNeumannSet_StartId() const { return true; }
  int getNeumannSet_StartId() const { return neumannSetStartId; }
  void setNeumannSet_StartId(int in) { neumannSetStartId = in; }
  void clearNeumannSet_StartId() { }

  bool isSetMaterialSet_StartId() const { return true; }
  int getMaterialSet_StartId() const { return materialSetStartId; }
  void setMaterialSet_StartId(int in) { materialSetStartId = in; }
  void clearMaterialSet_StartId() { }

};

class cmbNucAssembly;
class PinCell;

// Represents an assembly. Assemblies are composed of pin cells (cmbNucPinCell)
// and the surrounding ducting. Assemblies can be loaded and stored to files
// with the ReadFile() and Write() file methods. Assemblies are grouped together
// into cores (cmbNucCore).
class cmbNucAssembly : public LatticeContainer
{
public:
  class Transform
  {
  public:
    enum CONTROLS{HAS_REVERSE = 0x00001, HAS_VALUE = 0x00002, HAS_AXIS = 0x00004};
    enum AXIS{ X = 0, Y = 1, Z = 2 };
    Transform():Valid(false), axis(Z){}
    virtual ~Transform(){}
    bool isValid() const { return Valid; }
    //returns true if changed
    bool setValid( bool valid)
    {
      bool r = Valid != valid;
      Valid = valid;
      return r;
    }
    virtual double getValue() const = 0;
    virtual AXIS getAxis() const {return axis;}
    virtual std::string getLabel() const = 0;
    virtual bool reverse() const = 0;
    virtual std::ostream& write(std::ostream& os) const = 0;
    virtual int getControls() const = 0;
    void setAxis(std::string a);
  protected:
    bool Valid;
    AXIS axis;
  };
  class Rotate: public Transform
  {
  public:
    Rotate(): angle(0) {}
    Rotate(std::string a, double delta);
    Rotate(AXIS a, double delta);
    std::ostream& write(std::ostream& os) const;
    virtual double getValue() const {return angle;}
    virtual bool reverse() const {return false;}
    virtual std::string getLabel() const {return "Rotate";}
    virtual int getControls() const {return 6;}
  private:
    double angle;
  };
  class Section: public Transform
  {
  public:
    Section(): dir(1), value(0){}
    Section(std::string a, double v, std::string dir);
    Section(AXIS a, double v, int dir);
    std::ostream& write(std::ostream& os) const;
    virtual double getValue() const { return value; }
    virtual bool reverse() const {return dir == -1;}
    virtual std::string getLabel() const {return "Section";}
    virtual int getControls() const {return 7;}
  private:
    int dir;
    double value;
  };

  class PinCellUsed
  {
  public:
    PinCellUsed() : count(0){}
    int count;
    PinCell* pincell;
  };
public:

  // Creates an empty assembly.
  cmbNucAssembly(QString label, QString name, QColor color, bool centerPins = true,
                 double ppitchX = 0, double ppitchY = 0);

  std::string getOutputExtension();

  // Destroys the assembly.
  virtual ~cmbNucAssembly();

  void setPinLibrary(cmbNucPartLibrary * p)
  {
    this->Pins = p;
  }

  void setDuctLibrary(cmbNucPartLibrary * d);

  virtual QString extractLabel(QString const& s);
  virtual void setUpdateUsed();

  virtual enumNucPartsType GetType() const {return CMBNUC_ASSEMBLY;}

  vtkBoundingBox computeBounds();

  // Adds a new pincell to the assebly. After adding the pincell it
  // can be placed in the assembly with the SetCell() method.
  void AddPinCell(PinCell *pincell);
  void SetPinCell(int i, PinCell *pc);

  // Remove the pincell with label from the assembly.
  void RemovePinCell(PinCell *);

  void geometryChanged();

  // Returns the pincell with label. Returns 0 if no pincell with
  // label exists.
  PinCell* GetPinCell(const QString &label);
  PinCell* GetPinCell(int pc) const;
  std::size_t GetNumberOfPinCells() const;

  cmbAssyParameters* GetParameters() {return this->Parameters;}

  //Set the different from file and tests the cub file;
  void setAndTestDiffFromFiles(bool diffFromFile);
  bool changeSinceLastGenerate() const;

  void GetDuctWidthHeight(double r[2]);

  void getZRange(double & z1, double & z2);

  void clear();

  QSet< cmbNucMaterial* > getMaterials();
  bool has_boundary_layer_interface(QPointer<cmbNucMaterial> in_material) const;

  void setFromDefaults(QPointer<cmbNucDefaults> d);
  void computeDefaults();

  void calculatePitch(double & x, double & y);
  void calculatePitch(int width, int height, double & x, double & y);
  void calculateRadius(double & r);
  virtual void setPitch(double x, double y)
  {
    this->setPitch(x,y,true);
  }
  virtual bool setPitch(double x, double y, bool testDiff);

  void centerPins();

  void setCenterPins(bool t);

  bool isPinsAutoCentered()
  {
    return KeepPinsCentered;
  }

  void setPath(std::string const& path);
  void setFileName(std::string const& fname);
  std::string getFileName();
  std::string getFileName(Lattice::CellDrawMode mode, size_t nom = 1);
  virtual QString getTitle(){ return Name + " (" + Label + ")"; }
  bool needToRunMode(Lattice::CellDrawMode mode, std::string & fname, size_t nom = 1);

  std::string getGeometryLabel() const;
  void setGeometryLabel(std::string geomType);

  QPointer<cmbNucDefaults> getDefaults()
  { return this->Defaults; }

  QPointer<cmbNucDefaults> Defaults;

  // Check if GeometryType is Hexagonal
  bool IsHexType();

  //Transforms exist for inp file import and export
  bool addTransform(Transform * in); //Will not add invalid xfroms, takes ownership

  bool removeOldTransforms(int newSize);
  Transform* getTransform(int i) const; //NULL if not found
  size_t getNumberOfTransforms() const;

  virtual void fillList(std::vector< std::pair<QString, cmbNucPart *> > & l);

  virtual cmbNucPart * getFromLabel(const QString & s)
  {
    return this->GetPinCell(s);
  }

  virtual void calculateExtraTranslation(double & transX, double & transY);

  virtual void calculateTranslation(double & transX, double & transY);

  cmbNucPartLibrary * getPinLibrary()
  { return Pins; }

  cmbNucPartLibrary * getDuctLibrary()
  { return Ducts; }

  DuctCell & getAssyDuct()
  {
    return *AssyDuct;
  }

  virtual void getRadius(double & ri, double & rj) const;

  //return true when changed
  void setDuctCell(DuctCell * AssyDuct);

  void adjustRotation();

  double getZAxisRotation() const;
  void setZAxisRotation(double d);

  cmbNucAssembly * clone(cmbNucPartLibrary * pl, cmbNucPartLibrary * dl);

  cmbNucPart * clone() { return NULL; }

protected:
  std::vector<PinCell*> PinCells;
  cmbNucPartLibrary * Pins;
  cmbNucPartLibrary * Ducts;
  DuctCell * AssyDuct;

  //Transforms exist for inp file import and export
  std::vector<Transform*> Transforms;

  Rotate zAxisRotation;

  bool KeepPinsCentered;

  cmbAssyParameters *Parameters;

  std::string GeometryType;

  virtual void componentPartDeleted(cmbNucPart * obj);
  virtual void componentPartChanged(cmbNucPart * obj);

private:
  bool DifferentFromCub;
  bool DifferentFromJournel;

  std::string ExportFilename;
  std::string Path;

  bool checkJournel(std::string const& fname );
  bool checkCubitOutputFile(std::string const& fname );

};

#endif // cmbNucAssembly_H
