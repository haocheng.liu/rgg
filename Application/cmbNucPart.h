#ifndef __cmbNucPart_h
#define __cmbNucPart_h

#include <boost/function.hpp>
#include <QColor>
#include <vector>

#include "cmbNucPartDefinition.h"

class cmbNucPart;

class cmbNucPartConnection: public QObject
{
  Q_OBJECT
public:
  cmbNucPartConnection( cmbNucPart* apo);
  ~cmbNucPartConnection();
  void emitCheckChangeState()
  { emit checkChangeState(); }
public slots:
  void componentIsDeleted(cmbNucPart* component);
  void componentChanged(cmbNucPart* component);
  void changed();
signals:
  void partDeleted(cmbNucPart*);
  void partChanged(cmbNucPart*);
  void sendPitch(double, double);
  void checkChangeState();
private:
  cmbNucPart * part;
};

class cmbNucPart
{
public:
  friend class cmbNucPartConnection;
  cmbNucPart(QString l="", QString n = "", QColor lc = Qt::white);
  virtual ~cmbNucPart();
  virtual enumNucPartsType GetType() const;
  template<class T> static bool removeObj(T* obj, std::vector<T*>& objs, bool del = true)
  {
    for(typename std::vector<T*>::iterator fit=objs.begin(); fit!=objs.end(); ++fit)
    {
      if(*fit == obj)
      {
        if(del) delete obj;
        objs.erase(fit);
        return true;
      }
    }
    return false;
  }
  template<class T> static void deleteObjs(std::vector<T*>& objs)
  {
    for(typename std::vector<T*>::iterator fit=objs.begin(); fit!=objs.end(); ++fit)
    {
      if(*fit)
      {
        delete *fit;
      }
    }
    objs.clear();
  }
  virtual QString getListTitle() const;
  virtual QString const& getLabel() const;
  virtual QString getTitle();
  virtual QString const& getName() const;
  virtual void setLabel(QString const& l);
  virtual void setName(QString const& n);
  virtual std::string getFileName();
  virtual QColor GetLegendColor() const;
  virtual void SetLegendColor(QColor color);

  virtual bool sameShape(cmbNucPart const* /*other*/) const { return false; }
  virtual bool equal(cmbNucPart const* other) const;

  virtual cmbNucPart * clone(/*boost::function<QString (QString)> label_fun = 0,
                             boost::function<QString (QString)> name_fun = 0,
                             bool new_color = false*/);

  static QString getUnique(boost::function<bool (QString)> testFunction, QString prefix,
                           bool conflict_value, bool allHaveNumber);

  virtual cmbNucPartConnection * getConnection()
  {
    return this->connection;
  }

  virtual void sendChanged();
  virtual void checkChangeState();

  virtual double getRadius() const
  { return -1; }
protected:
  QString Label;
  QString Name;
  QColor LedgendColor;

  cmbNucPartConnection * connection;

  virtual void componentPartDeleted(cmbNucPart * /*obj*/){/*do nothing here*/}
  virtual void componentPartChanged(cmbNucPart * /*obj*/)
  { this->sendChanged(); }

  void deleteConnection();

  void connectObj(cmbNucPart * other);
  void disconnectObj(cmbNucPart * other);
};

#endif
