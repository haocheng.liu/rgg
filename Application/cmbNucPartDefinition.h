#ifndef __cmbNucPartDefinition_h
#define __cmbNucPartDefinition_h

#include <vector>
#include <sstream>
#include <assert.h>


enum enumNucPartsType
{
  CMBNUC_ASSY_BASEOBJ = 0,
  CMBNUC_ASSEMBLY = 1,
  CMBNUC_ASSEMBLY_LINK = 2,
  CMBNUC_CORE,
  CMBNUC_ASSY_DUCTCELL,
  CMBNUC_ASSY_PINCELL
};

enum enumGeometryType {
  RECTILINEAR = 0x0140,
  HEXAGONAL = 0x0241,
};

enum enumGeometryControls
{
  FLAT = 0x0001,
  VERTEX = 0x0002,
  JUST_HEX_SUBTYPE = 0x00FF,
  ANGLE_60 = 0x00100,
  ANGLE_30 = 0x00200,
  ANGLE_360 = 0x00400,
  JUST_ANGLE = 0x0FF00,
};

#endif
