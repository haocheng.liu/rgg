#include "cmbNucFillLattice.h"
#include "cmbNucLattice.h"
#include "DrawLatticeItem.h"
#include "cmbNucCordinateConverter.h"

#include "ui_qSimpleFill.h"

#include <cmath>
#include <QApplication>
#include <QDebug>

//===========================Fill Functions=====================================
class cmbLatticeFillInc : public cmbLaticeFillFunction
{
public:
  cmbLatticeFillInc(int const s[2], int const e[2], int const i[2])
  {
    this->start[0] = s[0]; this->start[1] = s[1];
    this->end[0]   = e[0]; this->end[1]   = e[1];
    this->inc[0]   = i[0]; this->inc[1]   = i[1];
    this->begin();
    this->isNeg[0] = i[0] < 0;   this->isNeg[1] = i[1] < 0;
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    if(this->stop()) return false;
    i = static_cast<size_t>(this->at[0]);
    j = static_cast<size_t>(this->at[1]);
    this->at[0] = static_cast<size_t>(static_cast<int>(this->at[0]) + this->inc[0]);
    this->at[1] = static_cast<size_t>(static_cast<int>(this->at[1]) + this->inc[1]);
    return true;
  }

  virtual void begin()
  {
    at[0] = this->start[0]; at[1] = this->start[1];
  }
protected:
  int start[2], end[2];
  int inc[2];
  int at[2];
  bool isNeg[2];

  inline bool stop()
  {
    return ((isNeg[0] && this->at[0] < this->end[0])||(!isNeg[0] && this->at[0] > this->end[0]))||
           ((isNeg[1] && this->at[1] < this->end[1])||(!isNeg[1] && this->at[1] > this->end[1]));
  }
};

class cmbLatticeFillHexCordInc : public cmbLatticeFillInc
{
public:
  cmbLatticeFillHexCordInc(int const s[2], int const e[2], int const i[2])
  :cmbLatticeFillInc(s,e,i)
  {
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    if(this->stop()) return false;
    //convert to polar
    cmbNucCordinateConverter::convertFromHexCordinates(this->at[0], this->at[1], i, j);
    this->at[0] += this->inc[0];
    this->at[1] += this->inc[1];
    return true;
  }
protected:
};

class cmbHexFillLineBetweenLevels : public cmbLaticeFillFunction
{
public:
  cmbHexFillLineBetweenLevels(int const s[2], int minL, int maxL, int const i[2])
  {
    this->start[0] = s[0]; this->start[1] = s[1];
    this->end[0] = minL;   this->end[1] = maxL;
    this->inc[0] = i[0];   this->inc[1] = i[1];
    this->begin();
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    //convert to polar
    size_t ti,tj;
    cmbNucCordinateConverter::convertFromHexCordinates(this->at[0], this->at[1], ti, tj);
    if(static_cast<int>(ti) < this->end[0] || static_cast<int>(ti) > this->end[1]) return false;
    i = ti; j = tj;
    this->at[0] += this->inc[0];
    this->at[1] += this->inc[1];
    return true;
  }

  virtual void begin()
  {
    at[0] = this->start[0]; at[1] = this->start[1];
  }
protected:
  int start[2], end[2], inc[2], at[2];
};


class cmbHexFillCircle : public cmbLaticeFillFunction
{
public:
  cmbHexFillCircle(int const s[2], int const c[2], double rin, int inc)
  {
    this->start[0] = s[0]; this->start[1] = s[1];
    cmbNucCordinateConverter::convertFromHexCordToEuclid(c[0], c[1], center[0], center[1]);
    r = rin;
    increment = inc;
    this->begin();
    double x, y;
    cmbNucCordinateConverter::convertFromHexCordToEuclid(at[0], at[1], x, y);
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    if(returned) return false;
    cmbNucCordinateConverter::convertFromHexCordinates(this->at[0], this->at[1], i, j);
    static int dir[6][2] = {{1,-1},{1,0},{0,1},{-1,1},{-1,0},{0,-1}};
    double x, y, prevR = 0;
    for(int inc = 0; inc < increment && !returned; ++inc)
    {
      int idx = (previous_index + 1)%6;
      bool changed = false;
      int original_at[] = {this->at[0], this->at[1]};
      for(int tmp = 0; tmp < 5; ++tmp)
      {
        int tmpIdx = (idx + tmp)%6;
        int tat[] = {original_at[0]+ dir[tmpIdx][0], original_at[1] + dir[tmpIdx][1]};
        cmbNucCordinateConverter::convertFromHexCordToEuclid(tat[0], tat[1], x, y);
        double distx = center[0] - x;
        double disty = center[1] - y;
        double tmpr = std::sqrt( distx*distx + disty*disty );
        if( tmpr <= r && (!changed || prevR < tmpr) )
        {
          prevR = tmpr;
          at[0] = tat[0];
          at[1] = tat[1];
          previous_index = (tmpIdx+3)%6;
          changed = true;
        }
      }
      returned = !changed || (at[0] == start[0] && at[1] == start[1]);
    }
    return true;
  }

  virtual void begin()
  {
    at[0] = this->start[0]; at[1] = this->start[1];
    previous_index = 0;
    returned = false;
  }

protected:
  int start[2], at[2];
  double center[2];
  int previous_index;
  int increment;
  bool returned;
  double r;
};

int pass_start;

class cmbRectFillCircle : public cmbLaticeFillFunction
{
public:
  cmbRectFillCircle(int const s[2], double const c[2], double rin, int inc)
  {
    this->start[0] = s[0]; this->start[1] = s[1];
    center[0] = c[0]; center[1] = c[1];
    r = rin;
    r2 = r*r;
    increment = inc;
    this->begin();

  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    if(returned) return false;
    i = this->at[0];
    j = this->at[1];

    for (int inc = 0; inc < increment && !returned; ++inc)
    {
      int idx = (previous_index + 1)%8;
      previous_index = find_index(idx, 1, 6, this->at);
      returned = previous_index == -1 || (at[0] == start[0] && at[1] == start[1]);
    }
    return true;
  }

  virtual void begin()
  {
    at[0] = this->start[0]; at[1] = this->start[1];
    int tmp[2];
    previous_index = find_index(0, 0, 8, tmp);
    pass_start = 0;

    returned = false;
  }

  int find_index(int idx, int s, int e, int l[2])
  {
    static int dir[8][2] = {{0,-1},{1,-1},{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1}};
    double x, y, prevRDiffSquare = r2;
    double diffThresh = 2.0*prevRDiffSquare;
    int original_at[] = {this->at[0], this->at[1]};
    int result_ind = -1;

    for(int tmp = s; tmp < e; ++tmp)
    {
      int tmpIdx = (idx + tmp)%8;
      int tat[] = {original_at[0]+ dir[tmpIdx][0], original_at[1] + dir[tmpIdx][1]};
      x = tat[0];
      y = tat[1];
      double dist[] = {this->center[0] - x, this->center[1] - y};
      double tmpr = dist[0]*dist[0] + dist[1]*dist[1];
      double rdiff = std::abs(tmpr - r2);

      if( rdiff < prevRDiffSquare ||
         (std::abs(rdiff - prevRDiffSquare) <= 10*std::numeric_limits<double>::epsilon() && tmpr < diffThresh))
      {
        prevRDiffSquare = rdiff;
        diffThresh = tmpr;
        l[0] = tat[0];
        l[1] = tat[1];
        result_ind = (tmpIdx+4)%8;
      }
    }
    return result_ind;
  }
protected:
  int start[2], at[2];
  int increment;
  double center[2];
  int previous_index;
  bool returned;
  double r;
  double r2;
};

class cmbNucSkipFunction
{
public:
  virtual ~cmbNucSkipFunction()
  {}
  virtual bool skip(int /*i*/, int /*j*/) const
  {
    return false;
  }
};

class skipOutSideCircle: public cmbNucSkipFunction
{
public:
  double center[2];
  double radius2;
  skipOutSideCircle(double c[2], double r)
  {
    radius2 = r*r;
    center[0] = c[0];
    center[1] = c[1];
  }

  virtual bool skip(int i, int j) const
  {
    double x, y;
    this->convert(i,j, x, y);
    double diff[] = {center[0]-x, center[1] - y};
    return (diff[0]*diff[0] + diff[1]*diff[1])>radius2;
  }
protected:
  virtual void convert(double i, double j, double &x, double &y) const
  {
    x = i; y = j;
  }
};

class skipOutSideCircleHex: public skipOutSideCircle
{
public:
  skipOutSideCircleHex(double c[2], double r):skipOutSideCircle(c,r)
  {}
protected:
  virtual void convert(double i, double j, double &x, double &y) const
  {
    int ti, tj;
    cmbNucCordinateConverter::convertToHexCordinates(static_cast<int>(i), static_cast<int>(j),
                                                     ti, tj);
    cmbNucCordinateConverter::convertFromHexCordToEuclid(ti, tj, x, y);
  }
};

class cmbLatticeFillFunSet : public cmbLaticeFillFunction
{
public:
  cmbLatticeFillFunSet(std::vector<cmbLaticeFillFunction*> & funs, cmbNucSkipFunction * fun = NULL )
  : functions(funs), at(0)
  {
    if(fun == NULL)
    {
      fun = new cmbNucSkipFunction();
    }
    skip_fun = fun;
  }
  ~cmbLatticeFillFunSet()
  {
    for(size_t i = 0; i < functions.size(); ++i)
    {
      delete functions[i];
    }
    delete skip_fun;
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    for(;at<functions.size(); ++at)
    {
      while(functions[at]->getNext(i,j))
      {
        if(!this->skip_fun->skip(i,j)) return true;
      }
    }
    return false;
  }

  virtual void begin()
  {
    at = 0;
    for(size_t i = 0; i < functions.size(); ++i)
    {
      functions[i]->begin();
    }
  }
protected:
  std::vector<cmbLaticeFillFunction*> functions;
  cmbNucSkipFunction const* skip_fun;
  size_t at;
};



namespace
{
  cmbLaticeFillFunction * ring_hex_function(int pt[2], int inc, bool fill,
                                            cmbNucSkipFunction * skipFun =  NULL)
  {
    std::vector<cmbLaticeFillFunction*> funs;
    static const int pts[][2] = {{-1, 0}, {0,-1}, {1,-1}, { 1,0}, { 0,1}, {-1,1}};
    static const int dir[][2] = {{ 1,-1}, {1, 0}, {0, 1}, {-1,1}, {-1,0}, {0,-1}};
    do
    {
      for(unsigned int i = 0; i < 6; ++i)
      {
        int n = (i+1)%6;
        int start[] = {pt[0]+pts[i][0]*inc, pt[1]+pts[i][1]*inc};
        int end[]   = {pt[0]+pts[n][0]*inc, pt[1]+pts[n][1]*inc};
        funs.push_back(new cmbLatticeFillHexCordInc(start, end, dir[i]));
      }
      --inc;
    } while (fill && inc >= 1);
    if(fill)
    {
      int start[] = {pt[0], pt[1]};
      int end[]   = {pt[0], pt[1]};
      funs.push_back(new cmbLatticeFillHexCordInc(start, end, dir[0]));
    }
    return new cmbLatticeFillFunSet(funs, skipFun);
  }

  cmbLaticeFillFunction * ring_rect_function(int const pt[2], int inc, bool fill,
                                             cmbNucSkipFunction * skipFun =  NULL)
  {
    std::vector<cmbLaticeFillFunction*> functions;
    static const int point[][2] = {{-1,-1}, {1,-1}, {1,1}, {-1,1}};
    static const int dir[][2] = {{1,0}, {0,1}, {-1,0}, {0,-1}};
    do
    {
      for (int i = 0; i < 4; ++i)
      {
        int n = (i+1)%4;
        int start[] = {point[i][0] * inc+pt[0], point[i][1] * inc+pt[1]};
        int end[]   = {point[n][0] * inc+pt[0], point[n][1] * inc+pt[1]};
        functions.push_back(new cmbLatticeFillInc(start, end, dir[i]));
      }
      --inc;
    } while (fill && inc >= 1);
    if(fill)
    {
      int start[] = {pt[0], pt[1]};
      int end[]   = {pt[0], pt[1]};
      functions.push_back(new cmbLatticeFillInc(start, end, dir[0]));
    }
    return new cmbLatticeFillFunSet(functions, skipFun);
  }
}

cmbLaticeFillFunction *
cmbLaticeFillFunction::generateFunctionHex( size_t r, size_t c, double inc, bool fill,
                                           FunctionType fun, Lattice * lat )
{
  assert(lat->GetGeometryType() == HEXAGONAL);
  if(fun == RING || (fun == CIRCLE && (r < 2 || (r == 2 && c%2 == 0))))
  {
    size_t start, end;
    bool tr = lat->getValidRange(r, start, end);
    if(!tr)
    {
      start = 0;
      end = 0;
    }
    size_t dis = c - start;
    size_t tmp = dis / inc;
    start = c - tmp*inc;
    int s[] = {static_cast<int>(r), static_cast<int>(start)},
        e[] = {static_cast<int>(r), static_cast<int>(end)};
    int i[] = {0, static_cast<int>(inc)};
    return new cmbLatticeFillInc(s, e, i);
  }
  int total_level = static_cast<int>(lat->GetDimensions().first) - 1;
  int pt[2];
  cmbNucCordinateConverter::convertToHexCordinates(r,c,pt[0],pt[1]);

  enum hexFillFuns {HF0 = 0, HF1, HF2, HF3, HF4, HF5, HF6, HR0, HC0, HC1};

  int funAt = fun - ROW;
  assert(funAt >= 0 && funAt<7);
  static const hexFillFuns operatorToFucntion[][7] ={ {HF0, HF1, HF2, HF3, HR0, HC0, HC1},
                                                      {HF4, HF3, HF2, HF0, HR0, HC0, HC1},
                                                      {HF6, HF2, HF0, HF3, HR0, HC0, HC1} };
  int index = lat->getFullCellMode() + ((lat->GetGeometrySubType() & VERTEX)>>1);
  hexFillFuns hff_at = operatorToFucntion[index][funAt];
  if( hff_at < HR0)
  {
    static const int funcIncs[][2] = { {1,-1}, {1,1}, {1,0}, {0,1}, {2, -1}, {2, -2}, {-1, 2}};
    int i[] = {static_cast<int>(funcIncs[hff_at][0]*inc),
               static_cast<int>(funcIncs[hff_at][1]*inc)};
    int invI[] = {-i[0], -i[1]};
    std::vector<cmbLaticeFillFunction*> funs;
    funs.push_back(new cmbHexFillLineBetweenLevels(pt, 0, total_level, i));
    funs.push_back(new cmbHexFillLineBetweenLevels(pt, 0, total_level, invI));
    return new cmbLatticeFillFunSet(funs);
  }
  else if(hff_at == HR0)
  {
    return ring_hex_function(pt, inc, fill);
  }
  else if(hff_at == HC0)
  {
    if(fill)
    {
      double center[2];
      cmbNucCordinateConverter::convertFromHexCordToEuclid(pt[0], pt[1], center[0], center[1]);
      r = inc + 0.5;
      int l_layer = r + std::ceil((1 - cmbNucMathConst::cos30)*r);
      skipOutSideCircleHex * skipper = new skipOutSideCircleHex(center, r + 0.5);
      return ring_hex_function(pt, l_layer, fill, skipper);
    }
    else
    {
      int start[] = { pt[0], static_cast<int>(pt[1] + inc) };
      return new cmbHexFillCircle(start, pt, inc + 0.5, 1.0);
    }
  }
  else if(hff_at == HC1)
  {
    static const int center[] = {0, 0};
    double x,y;
    cmbNucCordinateConverter::convertFromHexCordToEuclid(pt[0], pt[1], x, y);
    double rad = std::sqrt(x*x+y*y);
    return new cmbHexFillCircle(pt, center, rad, inc);
  }
  return NULL;
}

class cmbRectFillRing : public cmbLaticeFillFunction
{
public:
  cmbRectFillRing(std::pair<size_t, size_t> dem, size_t r, size_t c, int inc)
  : increment(inc)
  {
    int farPt[] = {static_cast<int>(dem.first-1), static_cast<int>( dem.second - 1)};
    int ci = static_cast<int>(c);
    int ri = static_cast<int>(r);
    this->start[0] = ri;      this->start[1] = ci;
    int ring =  std::min(ri, std::min(ci, std::min(farPt[0]-ri, farPt[1]-ci)));

    corners[0] = ring; corners[1] = farPt[0]-ring;
    corners[3] = ring; corners[2] = farPt[1]-ring;

    this->begin();
  }
  virtual bool getNext(size_t& i, size_t& j)
  {
    if(returned) return false;
    i = static_cast<size_t>(this->at[0]);
    j = static_cast<size_t>(this->at[1]);

    static const int directions[] =    { 1,      1,  -1,      -1};
    static const int side_index[] = {0,1,0,1};

    for ( int inc = 0; inc < increment && !returned; ++inc)
    {
      bool changed = false;
      int si = side_index[side];
      int next = (side + 1)%4;
      //at a corner, time to switch sides
      int count = 0;
      while(this->at[si] == corners[next] && count < 4)
      {
        side = next;
        si = side_index[side];
        next = (side + 1)%4;
        count++;
      }
      if(this->at[si] != corners[next])
      {
        this->at[si] += directions[side];
        changed = true;
      }
      returned = (!changed || (this->start[0] == this->at[0] && this->start[1] == this->at[1]));
    }

    return true;
  }

  virtual void begin()
  {
    at[0] = this->start[0]; at[1] = this->start[1];
    side = -1;
    if     (at[1] == corners[0]) side = 0;
    else if(at[0] == corners[1]) side = 1;
    else if(at[1] == corners[2]) side = 2;
    else if(at[0] == corners[3]) side = 3;
    assert(side >= 0);
    returned = false;
  }
protected:
  int corners[4], side, increment, at[2], start[2];
  bool returned;
};


cmbLaticeFillFunction *
cmbLaticeFillFunction::generateFunction( size_t r, size_t c, double inc, bool fill,
                                        FunctionType fun, Lattice * lat )
{
  //RING,ROW,COL,DIANGLE1,DIANGLE2
  if(lat->GetGeometryType() == HEXAGONAL)
  {
    return generateFunctionHex(r,c,inc, fill, fun,lat);
  }
  switch (fun)
  {
    case RING:
      return new cmbRectFillRing(lat->GetDimensions(), r, c, inc);
    case COL:
    {
      std::pair<size_t, size_t> dem = lat->GetDimensions();
      size_t s =r - (r / inc) * inc;
      int start[] = {static_cast<int>(s), static_cast<int>(c)},
          end[] = {static_cast<int>(dem.first-1), static_cast<int>(c)};
      int i[] = {static_cast<int>(inc), 0};
      return new cmbLatticeFillInc(start, end, i);
    }
    case ROW:
    {
      std::pair<size_t, size_t> dem = lat->GetDimensions();
      size_t s =c - (c / inc) * inc;
      int start[] = {static_cast<int>(r), static_cast<int>(s)},
          end[] = {static_cast<int>(r), static_cast<int>(dem.second-1)};
      int i[] = {0, static_cast<int>(inc)};
      return new cmbLatticeFillInc(start, end, i);
    }
    case DIANGLE2:
    {
      std::pair<size_t, size_t> dem = lat->GetDimensions();
      size_t pt[] = {r,c};
      static size_t at[][2] = { {0,1}, {1,0} };
      size_t mi = (r < c)?0:1;
      int start[2];
      start[at[mi][0]] = static_cast<int>(pt[at[mi][0]] - ((pt[at[mi][0]] / inc) * inc));
      start[at[mi][1]] = static_cast<int>(pt[at[mi][1]] - (pt[at[mi][0]] - start[at[mi][0]]));
      int end[] = {static_cast<int>(dem.first-1), static_cast<int>(dem.second-1)};
      int i[] = {static_cast<int>(inc), static_cast<int>(inc)};
      return new cmbLatticeFillInc(start, end, i);
    }
    case DIANGLE1:
    {
      std::pair<size_t, size_t> dem = lat->GetDimensions();
      size_t sum = r + c;
      size_t x = (sum >= dem.first)?dem.first -1:sum;
      int start[] = { static_cast<int>(x), static_cast<int>(sum - x) };
      int end[] = {0, static_cast<int>(dem.second-1)};
      int i[] = {static_cast<int>(-inc), static_cast<int>(inc)};
      return new cmbLatticeFillInc(start, end, i);
    }
    case RELATIVE_RING:
    {
      const int pt[] = {static_cast<int>(r), static_cast<int>(c)};
      return ring_rect_function(pt, inc, fill, NULL);
    }
    case RELATIVE_CIRCLE:
    {
      const double pt[] = {static_cast<double>(r), static_cast<double>(c)};
      double radius;
      double r2 = inc*inc;
      double rf = std::floor(inc);
      double rc = std::ceil(inc);
      double floorDiff = std::abs(rf*rf - r2);
      double ceilDiff = std::abs(rc*rc - r2);
      if ( ceilDiff < floorDiff &&
           std::abs(ceilDiff-floorDiff) > 10*std::numeric_limits<double>::epsilon() )
      {
        radius = rc;
      }
      else
      {
        radius = rf;
      }
      int start[] = { static_cast<int>(pt[0]), static_cast<int>(pt[1] + radius) };
      cmbRectFillCircle * circle = new cmbRectFillCircle(start, pt, inc, 1);

      if(fill)
      {
        double center[2] = {static_cast<double>(r), static_cast<double>(c)};
        const int pti[] = {static_cast<int>(r), static_cast<int>(c)};
        skipOutSideCircle * skipper = new skipOutSideCircle(center, inc);
        std::vector<cmbLaticeFillFunction*> funs;
        funs.push_back(circle);
        funs.push_back(ring_rect_function(pti, inc, fill, skipper));
        return new cmbLatticeFillFunSet(funs, NULL);;
      }
      return circle;
    }
    case CIRCLE:
    {
      std::pair<size_t, size_t> dem = lat->GetDimensions();
      const double pt[] = { (dem.first-1) * 0.5, (dem.second-1) * 0.5 };
      int start[] = { static_cast<int>(r), static_cast<int>(c) };
      double tmp[] = {r - pt[0], c - pt[1]};
      double rad = std::sqrt(tmp[0]*tmp[0] + tmp[1]*tmp[1]);
      return new cmbRectFillCircle(start, pt, rad, inc);
    }
  }
  return NULL;
}
//==============================================================================

//=============================simple dialog====================================

class cmbNucFillSimpleDialogInternal: public Ui_SimpleFill
{
public:
  Lattice * grid;
  size_t i, j;
  DrawLatticeItem* item;
};

cmbNucFillSimpleDialog
::cmbNucFillSimpleDialog(QWidget * p, DrawLatticeItem* item, Lattice * c,
                         std::vector< std::pair<QString, cmbNucPart *> > & al)
: QDialog(p, Qt::Tool), ui(new cmbNucFillSimpleDialogInternal)
{
  this->ui->grid = c;
  this->ui->i = item->layer(); this->ui->j = item->cellIndex();
  this->ui->item = item;
  this->ui->setupUi(this);

  this->ui->Function->clear();
  this->ui->Function->addItem("Ring", QVariant(cmbLaticeFillFunction::RING));
  this->ui->Function->addItem("Relative Ring", QVariant(cmbLaticeFillFunction::RELATIVE_RING));
  this->ui->Function->addItem("Row", QVariant(cmbLaticeFillFunction::ROW));
  this->ui->Function->addItem("Column", QVariant(cmbLaticeFillFunction::COL));
  this->ui->Function->addItem("Major Diangle", QVariant(cmbLaticeFillFunction::DIANGLE1));
  this->ui->Function->addItem("Minor Diangle", QVariant(cmbLaticeFillFunction::DIANGLE2));
  this->ui->Function->addItem("Circle", QVariant(cmbLaticeFillFunction::CIRCLE));
  this->ui->Function->addItem("Relative Circle", QVariant(cmbLaticeFillFunction::RELATIVE_CIRCLE));

  for(size_t i = 0; i < al.size(); ++i)
  {
    std::pair<QString, cmbNucPart *> const& at = al[i];
    this->ui->FillWith->addItem(at.first, qVariantFromValue(static_cast<void*>(at.second)));
  }
  connect( this->ui->Function, SIGNAL(currentIndexChanged(int)),
           this,               SLOT(previewFunction()));
  connect( this->ui->FillWith, SIGNAL(currentIndexChanged(int)),
           this,               SLOT(previewFunction()));
  connect( this->ui->Inc,      SIGNAL(valueChanged(int)),
           this,               SLOT(previewFunction()));

  connect( this->ui->fill,      SIGNAL(clicked()),
           this,               SLOT(previewFunction()) );

  this->previewFunction();
}

cmbNucFillSimpleDialog::~cmbNucFillSimpleDialog()
{
  delete ui;
}

cmbLaticeFillFunction * cmbNucFillSimpleDialog::generateFunction()
{
  int inc = this->ui->Inc->value();
  int at =this->ui->Function->currentIndex();
  bool fill = this->ui->fill->isChecked();
  cmbLaticeFillFunction::FunctionType funct =
        static_cast<cmbLaticeFillFunction::FunctionType>(this->ui->Function->itemData(at).toInt());

  return cmbLaticeFillFunction::generateFunction( this->ui->i, this->ui->j,
                                                  inc, fill, funct, this->ui->grid );
}

void cmbNucFillSimpleDialog::previewFunction()
{
  int at =this->ui->Function->currentIndex();
  cmbLaticeFillFunction::FunctionType funct =
       static_cast<cmbLaticeFillFunction::FunctionType>(this->ui->Function->itemData(at).toInt());
  if(funct == cmbLaticeFillFunction::RELATIVE_RING ||
     funct == cmbLaticeFillFunction::RELATIVE_CIRCLE)
  {
    this->ui->IncLabel->setText("Radius");
    this->ui->fill->setVisible(true);
  }
  else
  {
    this->ui->IncLabel->setText("Increment");
    this->ui->fill->setVisible(false);
  }
  cmbLaticeFillFunction * fun = this->generateFunction();
  QVariant var = this->ui->FillWith->QComboBox::itemData(this->ui->FillWith->currentIndex());
  cmbNucPart * part = static_cast<cmbNucPart *>(var.value<void *>());
  this->ui->grid->unselect();
  this->ui->grid->functionPreview(fun, part);
  this->ui->item->select();
  delete fun;
  emit refresh(this->ui->item);
}

QString cmbNucFillSimpleDialog::getFillWith()
{
  return this->ui->FillWith->currentText();
}

//==============================================================================

class cmbNucFillLatticeInternal
{
};

cmbNucFillLattice::cmbNucFillLattice()
{
}

cmbNucFillLattice::~cmbNucFillLattice()
{
}

bool cmbNucFillLattice
::fillSimple(DrawLatticeItem* item, Lattice * lat, LatticeContainer* container)
{
  std::vector< std::pair<QString, cmbNucPart *> > al;
  container->fillList(al);
  cmbNucFillSimpleDialog simpleDialog(QApplication::activeWindow(), item, lat, al);
  connect(&simpleDialog, SIGNAL(refresh(DrawLatticeItem*)),
          this,          SIGNAL(refresh(DrawLatticeItem*)));
  int r = simpleDialog.exec();
  if(r)
  {
    QString text = container->extractLabel(simpleDialog.getFillWith());
    cmbNucPart * newPart = container->getFromLabel(text);
    cmbLaticeFillFunction * fillFun = simpleDialog.generateFunction();
    lat->fill(fillFun, newPart);
    delete fillFun;
  }
  return true;
}
