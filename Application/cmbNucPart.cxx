#include "cmbNucPart.h"
#include <QColor>

#include <boost/function.hpp>

cmbNucPartConnection::cmbNucPartConnection( cmbNucPart* apo)
: part(apo)
{ }

cmbNucPartConnection::~cmbNucPartConnection()
{
  emit partDeleted(part);
  part = NULL;
}

void cmbNucPartConnection::changed()
{
  emit partChanged(this->part);
}

void cmbNucPartConnection::componentIsDeleted(cmbNucPart* component)
{
  this->part->componentPartDeleted(component);
}

void cmbNucPartConnection::componentChanged(cmbNucPart* component)
{
  this->part->componentPartChanged(component);
}

//=============

cmbNucPart::cmbNucPart(QString l, QString n, QColor lc)
: Label(l), Name(n), LedgendColor(lc), connection( new cmbNucPartConnection(this) )
{}

cmbNucPart::~cmbNucPart()
{
  deleteConnection();
}

void cmbNucPart::deleteConnection()
{
  delete connection;
  connection = NULL;
}

enumNucPartsType cmbNucPart::GetType() const
{return CMBNUC_ASSY_BASEOBJ;}

bool cmbNucPart::equal(cmbNucPart const* other) const
{
  if(other == NULL) return false;
  return this->Name == other->Name && this->Label == other->Label;
}

QString const& cmbNucPart::getLabel() const
{ return this->Label; }

QString cmbNucPart::getTitle()
{
  return this->Name + " (" + this->Label + ")";
}

QString cmbNucPart::getListTitle() const
{
  return this->Name + " (" + this->Label + ")";
}

QString const& cmbNucPart::getName() const
{
  return this->Name;
}

void cmbNucPart::setLabel(QString const& l)
{
  this->Label = l;
}

void cmbNucPart::setName(QString const& n)
{
  this->Name = n;
}

std::string cmbNucPart::getFileName()
{
  return "";
}

QColor cmbNucPart::GetLegendColor() const
{
  return this->LedgendColor;
}

void cmbNucPart::SetLegendColor(QColor color)
{
  this->LedgendColor = color;
}

QString cmbNucPart::getUnique(boost::function<bool (QString)> testFunction, QString prefix,
                              bool conflict_value, bool allHaveNumber)
{
  int count = 0;
  QString name = prefix;
  if(allHaveNumber)
    name = (prefix + QString::number(count++));
  while(testFunction(name) == conflict_value)
  {
    name = (prefix + QString::number(count++));
  }
  return name;
}

void cmbNucPart::sendChanged()
{
  this->connection->changed();
}

void cmbNucPart::checkChangeState()
{
  this->connection->emitCheckChangeState();
}

void cmbNucPart::connectObj(cmbNucPart * other)
{
  QObject::connect(other->getConnection(), SIGNAL(partDeleted(cmbNucPart*)),
                   this->connection,       SLOT(componentIsDeleted(cmbNucPart*)));
  QObject::connect(other->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                   this->connection,       SLOT(componentChanged(cmbNucPart*)));
}

void cmbNucPart::disconnectObj(cmbNucPart * other)
{
  QObject::disconnect(other->getConnection(), SIGNAL(partDeleted(cmbNucPart*)),
                      this->connection,       SLOT(componentIsDeleted(cmbNucPart*)));
  QObject::disconnect(other->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                      this->connection,       SLOT(componentChanged(cmbNucPart*)));
}

cmbNucPart * cmbNucPart::clone(/*boost::function<QString (QString)> label_fun,
                               boost::function<QString (QString)> name_fun,
                               bool new_color*/)
{
  QString l = this->getLabel();
  QString n = this->getName();
  QColor c = this->GetLegendColor();
  /*if(label_fun)
  {
    l = label_fun(l);
  }
  if(name_fun)
  {
    n = name_fun(n);
  }
  if(new_color)
  {
    double rgb[3];
    cmbNucMaterialColors::instance()->CalcRGB(rgb[0],rgb[1],rgb[2]);
    c = QColor::fromRgbF(rgb[0],rgb[1],rgb[2]);
  }*/
  return new cmbNucPart(l, n, c);
}
