#ifndef cmbNucCordinateConverter_h_
#define cmbNucCordinateConverter_h_

#include <cstddef>

class Lattice;
class cmbNucCordinateConverterInternal;

class cmbNucMathConst
{
public:
  static const double cos30;
  static const double cos30Squared;
  static const double cosSinAngles[6][2];
  static const double radians60;
};

class cmbNucCordinateConverter
{
public:
  // for hex: control = true will disable rotation based on pointyness
  // for rec: control = true will disable fliping i axis
  cmbNucCordinateConverter(Lattice & lat, bool control = false);
  ~cmbNucCordinateConverter();
  void convertToPixelXY(int i, int j, double & x, double & y, double radius = 0.5);
  void convertToPixelXY(int i, int j, double & x, double & y, double radius1, double radius2);
  void computeRadius(int w, int h, double r[2]);

  static void convertToHexCordinates(size_t level, size_t ringI, int & x, int & y );
  static void convertFromHexCordinates(int x, int y, size_t & level, size_t & ringI);
  static void convertFromHexCordToEuclid(int i, int j, double & x, double & y);
private:
  cmbNucCordinateConverterInternal * internal;
};

#endif
