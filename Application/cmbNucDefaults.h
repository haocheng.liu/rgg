#ifndef __cmbNucDefaults_h
#define __cmbNucDefaults_h

#include <QObject>
#include <QPointer>
#include <QString>

#include <vector>

#define EASY_DEFAULT_PARAMS_MACRO()\
  FUN1(double, AxialMeshSize) \
  FUN1(int, EdgeInterval)\
  FUN1(QString, MeshType) \
  FUN1(double, Z0) \
  FUN1(double, Height) \
  FUN1(QString, UserDefined) \
  FUN1(double, DuctThickX) \
  FUN1(double, DuctThickY) \

class cmbNucDefaults : public QObject
{
  Q_OBJECT

public:
  cmbNucDefaults();
  ~cmbNucDefaults();
public:
#define FUN1(T,X)      \
  void set##X(T vin);  \
  bool has##X() const; \
  void clear##X();     \
  T get##X();
  EASY_DEFAULT_PARAMS_MACRO()
#undef FUN1
  bool hasDuctThickness() const { return true; }
  void setDuctThickness(double x, double y)
  {
    this->setDuctThickX(x);
    this->setDuctThickY(y);
  }
  void clearDuctThickness()
  {
    //do nothing
  }
  bool getDuctThickness(double& v1, double& v2)
  {
    v1 = getDuctThickX();
    v2 = getDuctThickY();
    return true;
  }
signals:
  void recieveCalculatedPitch(double x, double y);
protected:
#define FUN1(T, X) \
  struct X##Paired\
  {\
    T X; \
    bool valid; \
    X##Paired():valid(false){} \
    void set(T v)\
    { valid = true; X = v; } \
    void set(X##Paired v) \
    { valid = v.valid; X = v.X; }\
  } X;
  EASY_DEFAULT_PARAMS_MACRO()
#undef FUN1
};
#endif
