#include "cmbNucDefaultWidget.h"
#include "cmbNucDefaults.h"
#include "cmbNucCore.h"
#include "ui_qDefaults.h"
#include "cmbNucWidgetChangeCheckingData.h"
#include <QDebug>
#include <boost/preprocessor/punctuation/comma.hpp>

class cmbNucDefaultWidgetInternal
{
public:
  cmbNucDefaultWidgetInternal()
  {
    ui = new Ui::qDefaults;
  }
  ~cmbNucDefaultWidgetInternal()
  {
    delete ui;
  }
  void setCameraNeedsResest(bool changed)
  {
    if(changed)
    {
      needCameraReset = true;
    }
  }
  Ui_qDefaults * ui;
  bool isHex;
  bool needCameraReset;
};

cmbNucDefaultWidget::cmbNucDefaultWidget(QWidget *p)
:QWidget(p)
{
  this->Internal = new cmbNucDefaultWidgetInternal();
  this->Internal->ui->setupUi(this);
  QObject::connect(this->Internal->ui->DuctThickX, SIGNAL(valueChanged(double)),
                   this,                           SIGNAL(widthChanged(double)));
  QObject::connect(this->Internal->ui->DuctThickY, SIGNAL(valueChanged(double)),
                   this,                           SIGNAL(heightChanged(double)));
}

cmbNucDefaultWidget::~cmbNucDefaultWidget()
{
  delete Internal;
}

typedef cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper WRAPPER;

#define COMMONMACRO() \
COMMON(QString, MeshType) \
COMMON(QString, UserDefined)

#define CHECKER_MACRO()\
CHECKERFUN(AxialMeshSize, double, QLineEdit, QString, WRAPPER::isNumber) \
CHECKERFUN(EdgeInterval, int, QLineEdit, QString, WRAPPER::isNumber) \
CHECKERFUN(MeshType, QString, QComboBox, QString, WRAPPER::isNotEmtpyString) \
CHECKERFUN(Z0, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>) \
CHECKERFUN(Height, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double> ) \
CHECKERFUN(UserDefined, QString, QPlainTextEdit, QString, WRAPPER::isNotEmtpyString) \
CHECKERFUN(DuctThickX, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>) \
CHECKERFUN(DuctThickY, double, QDoubleSpinBox, double, WRAPPER::alwaysValid<double>) \

namespace
{
  void postApplyFun(cmbNucDefaultWidgetInternal * i, cmbNucCore * core, bool changed)
  {
    i->setCameraNeedsResest(changed);
    core->sendDefaults();
  }
}

void cmbNucDefaultWidget::set(cmbNucCore * core, cmbNucWidgetChangeChecker & checker)
{
  QPointer<cmbNucDefaults> c = this->Current = core->GetDefaults();
  bool isHex = core->IsHexType();

#define CHECKERFUN(NAME, DATA_TYPE, WIDGET, W_TYPE, IS_VALID_DATA)                                 \
{                                                                                                  \
  boost::function<DATA_TYPE (void)> getData = boost::bind(&cmbNucDefaults::get##NAME, c.data());   \
  boost::function<void (DATA_TYPE)> setData = boost::bind(&cmbNucDefaults::set##NAME,              \
                                                          c.data(), _1);                           \
  boost::function<void (void)> setDataInvalid = boost::bind(&cmbNucDefaults::clear##NAME,          \
                                                            c.data());                             \
  boost::function<bool (void)> dataIsSet = boost::bind(&cmbNucDefaults::has##NAME, c.data());      \
                                                                                                   \
  boost::function<bool (W_TYPE)> isValidData = IS_VALID_DATA;                                      \
                                                                                                   \
  WIDGET * widget = this->Internal->ui->NAME;                                                      \
  boost::function<void (bool)> postApply = boost::bind(&postApplyFun, this->Internal, core, _1);   \
                                                                                                   \
  cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE> * tmpD =                                             \
        new cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE>(widget, getData, setData, setDataInvalid,  \
                                                        dataIsSet, isValidData,  postApply);       \
  checker.registerToTrack(tmpD);                                                                   \
}
  CHECKER_MACRO()
#undef CHECKERFUN
  QObject::disconnect(this->Internal->ui->DuctThickX, SIGNAL(valueChanged(double)),
                      this->Internal->ui->DuctThickY, SLOT(setValue(double)));
  if(isHex)
  {
    QObject::connect(this->Internal->ui->DuctThickX, SIGNAL(valueChanged(double)),
                     this->Internal->ui->DuctThickY, SLOT(setValue(double)));
  }

  this->Internal->ui->ductX->setVisible(!isHex);
  this->Internal->ui->ductY->setVisible(!isHex);
  this->Internal->ui->DuctThickY->setVisible(!isHex);
  this->Internal->isHex = isHex;
  this->reset();
}

bool cmbNucDefaultWidget::assyPitchChanged()
{
  double tmpDuctThickX = this->Internal->ui->DuctThickX->value();
  double tmpDuctThickY = this->Internal->ui->DuctThickY->value();
  double cDuctThickX; double cDuctThickY;
  if(Current->getDuctThickness(cDuctThickX, cDuctThickY))
  {
    return cDuctThickX != tmpDuctThickX || cDuctThickY != tmpDuctThickY;
  }
  return false;
}

void cmbNucDefaultWidget::reset()
{
  this->Internal->needCameraReset = false;
}

bool cmbNucDefaultWidget::needCameraReset()
{
  return this->Internal->needCameraReset;
}
