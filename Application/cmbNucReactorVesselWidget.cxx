#include "cmbNucReactorVesselWidget.h"
#include "ui_qCoreBackground.h"

#include <QString>
#include <QDir>
#include <QSettings>
#include <QFileInfo>
#include <QFileDialog>
#include <QDebug>

#include <cmath>

#include "cmbNucDefaults.h"
#include "cmbNucMaterialColors.h"

class cmbNucReactorVesselWidgetInternal: public Ui::CoreBackground
{
public:
  cmbNucReactorVesselWidgetInternal() : Vessel(NULL), background_full_path("")
  {
  }
  cmbNucCoreParams::ReactorVessel * Vessel;
  QSpinBox * latticeX;
  QSpinBox * latticeY;
  double ductsize[2];
  std::string background_full_path;
};

cmbNucReactorVesselWidget
::cmbNucReactorVesselWidget(QWidget * p, QSpinBox * spx, QSpinBox * spy)
: cmbNucCheckableWidget(p), Internal(new cmbNucReactorVesselWidgetInternal)
{
  this->Core = NULL;
  this->Internal->setupUi(this);
  this->Internal->FileName->setVisible(false);
  this->Internal->GenerateControls->setVisible(false);
  this->Internal->latticeX = spx;
  this->Internal->latticeY = spy;
  QObject::connect(this->Internal->JacketMode, SIGNAL(currentIndexChanged(int)),
                   this, SLOT(displayBackgroundControls(int)));
  QObject::connect( this->Internal->JacketMode, SIGNAL(currentIndexChanged(const QString &)),
                    this, SLOT(checkChange()) );
  QObject::connect(this->Internal->FindMinCylinder, SIGNAL(clicked()),
                   this, SLOT(onCalculateCylinderDefaults()));
  QObject::connect(this->Internal->OuterEdgeInterval, SIGNAL(valueChanged(int)),
                   this, SLOT(onIntervalChanged(int)));
  QObject::connect(this->Internal->RadiusBox, 	SIGNAL(valueChanged(double)),
                   this,                        SLOT(onRadiusChanged(double)));
  QObject::connect(this->Internal->latticeX, SIGNAL(valueChanged(int)),
                   this,                     SLOT(coreSizeChanged(int)));
  QObject::connect(this->Internal->latticeY, SIGNAL(valueChanged(int)),
                   this,                     SLOT(coreSizeChanged(int)));

  QObject::connect( this->Internal->BackgroundSetting, SIGNAL(clicked()),
                   this, SLOT(onSetBackgroundMesh()) );
  QObject::connect( this->Internal->BackgroundClear, SIGNAL(clicked()),
                   this, SLOT(onClearBackgroundMesh()));
}

cmbNucReactorVesselWidget::~cmbNucReactorVesselWidget()
{
  delete Internal;
}

void cmbNucReactorVesselWidget::set(cmbNucCore * core)
{
  this->Core = core;
  if(core == NULL)
  {
    this->Internal->Vessel = NULL;
    return;
  }
  this->Internal->Vessel = &(core->getVessel());
  Core->GetDefaults()->getDuctThickness(this->Internal->ductsize[0], this->Internal->ductsize[1]);
  this->onReset();
}

void cmbNucReactorVesselWidget::onApply()
{
  if(this->Internal->Vessel == NULL)
  {
    return;
  }
  int index = this->Internal->JacketMode->currentIndex();
  this->Internal->Vessel->setMode(static_cast<cmbNucCoreParams::ReactorVessel::Mode>(index));
  this->Internal->Vessel->setCylinderOuterSpacing(this->Internal->OuterEdgeInterval->value());
  this->Internal->Vessel->setCylinderRadius(this->Internal->RadiusBox->value());
  this->Internal->Vessel->Background = this->Internal->Background->text().toStdString();
  this->Internal->Vessel->BackgroundFullPath = this->Internal->background_full_path;
}

void cmbNucReactorVesselWidget::onReset()
{
  if(this->Internal->Vessel == NULL)
  {
    return;
  }
  onClearBackgroundMesh();

  QString bg = QString::fromStdString(this->Internal->Vessel->Background);

  this->Internal->Background->setText(bg);
  this->Internal->background_full_path = this->Internal->Vessel->BackgroundFullPath;

  if(this->Internal->Vessel->generate())
  {
    onCalculateCylinderDefaults(true);
  }

  double r = this->Internal->Vessel->getCylinderRadius();
  int i = this->Internal->Vessel->getCylinderOuterSpacing();

  this->Internal->OuterEdgeInterval->setValue(i);


  this->Internal->RadiusBox->setValue(r);

  this->Internal->JacketMode->setCurrentIndex(this->Internal->Vessel->getMode());
}

void cmbNucReactorVesselWidget::onClear()
{
  if(this->Internal->Vessel == NULL)
  {
    return;
  }
  this->Core = NULL;
  this->Internal->Vessel = NULL;
}

void cmbNucReactorVesselWidget::checkChange()
{
  if(this->Internal->Vessel == NULL)
  {
    return;
  }
  int index = this->Internal->JacketMode->currentIndex();
  bool diff = false;
  if(index != this->Internal->Vessel->getMode())
  {
    diff = true;
  }
  if(index != cmbNucCoreParams::ReactorVessel::None)
  {
    if(index == cmbNucCoreParams::ReactorVessel::Generate)
    {
      if(this->Internal->RadiusBox->value() != this->Internal->Vessel->getCylinderRadius())
      {
        diff = true;
      }
      if(this->Internal->OuterEdgeInterval->value() !=
         this->Internal->Vessel->getCylinderOuterSpacing())
      {
        diff = true;
      }
    }
    if(this->Internal->Vessel->Background != this->Internal->Background->text().toStdString())
    {
      diff = true;
    }
    this->Internal->Vessel->BackgroundFullPath = this->Internal->background_full_path;
  }
  setValueChanged(diff);
}

void cmbNucReactorVesselWidget::displayBackgroundControls(int index)
{
  this->Internal->FileName->setVisible(index != cmbNucCoreParams::ReactorVessel::None);
  this->Internal->GenerateControls->setVisible(index == cmbNucCoreParams::ReactorVessel::Generate);
  if(index == cmbNucCoreParams::ReactorVessel::Generate)
  {
    onCalculateCylinderDefaults();
  }
  checkChange();
}

void cmbNucReactorVesselWidget::onCalculateCylinderDefaults(bool checkOld)
{
  if(this->Internal->Vessel == NULL)
  {
    return;
  }
  double initRadius;
  int ei = Core->GetDefaults()->getEdgeInterval();
  if(!Core->GetDefaults()->hasEdgeInterval()) ei = 10;
  double * ductsize = this->Internal->ductsize;
  double lx = this->Internal->latticeX->value();
  double ly = this->Internal->latticeY->value();
  if(Core->IsHexType())
  {
    double a = ductsize[0] * 0.2;
    double tmp = (lx-0.5)*ductsize[0]+a;
    double tmp2 = ((ductsize[0]+a)/std::sqrt(3.0))*0.5;
    double h = std::sqrt(tmp*tmp + tmp2*tmp2);
    initRadius = h;
    this->Internal->OuterEdgeInterval->setValue(lx*ei*12);
  }
  else
  {
    double ti = this->Internal->latticeY->value() * ductsize[0];
    double tj = lx * ductsize[1];
    double tmpr = std::sqrt(ti*ti+tj*tj);
    initRadius = tmpr*0.5 + std::sqrt( ductsize[0]*ductsize[0]+ ductsize[1]*ductsize[1])*0.5*0.2;
    this->Internal->OuterEdgeInterval->setValue(std::max(lx,ly)*ei*4);
  }

  double r = this->Internal->RadiusBox->value();
  this->Internal->RadiusBox->setMinimum(initRadius);
  this->Internal->RadiusBox->setValue((checkOld) ? (r>initRadius) ? r : initRadius :initRadius);
}

void cmbNucReactorVesselWidget::onRadiusChanged(double /*v*/)
{
  checkChange();
}

void cmbNucReactorVesselWidget::onIntervalChanged(int /*v*/)
{
  checkChange();
}

void cmbNucReactorVesselWidget::coreSizeChanged(int /*i*/)
{
  if(this->Internal->JacketMode->currentIndex() == cmbNucCoreParams::ReactorVessel::Generate)
  {
    this->onCalculateCylinderDefaults(true);
  }
  checkChange();
}

void cmbNucReactorVesselWidget::onSetBackgroundMesh()
{
  if(this->Core == NULL)
  {
    return;
  }
  QString fileName;
  if(this->Internal->JacketMode->currentIndex() == cmbNucCoreParams::ReactorVessel::Generate)
  {
    QString defaultLoc;
    QString name(Core->getExportFileName().c_str());
    if(name.isEmpty()) name = QString(Core->getFileName().c_str());
    if(!name.isEmpty())
    {
      QFileInfo fi(name);
      QDir dir = fi.dir();
      if(dir.path() == ".")
      {
        QDir tdir = QSettings("CMBNuclear",
                              "CMBNuclear").value("cache/lastDir",
                                                  QDir::homePath()).toString();
        defaultLoc = tdir.path();
      }
      else
      {
        defaultLoc = dir.path();
      }
    }
    else
    {
      QDir tdir = QSettings("CMBNuclear",
                            "CMBNuclear").value("cache/lastDir",
                                                QDir::homePath()).toString();
      defaultLoc = tdir.path();
    }

    fileName = QFileDialog::getSaveFileName( this, "Save Outer Cylinder File...",
                                             defaultLoc, "cub Files (*.cub)" );
  }
  else
  {
    // Use cached value for last used directory if there is one,
    // or default to the user's home dir if not.
    QSettings settings("CMBNuclear", "CMBNuclear");
    QDir dir = settings.value("cache/lastDir", QDir::homePath()).toString();

    QStringList fileNames =
      QFileDialog::getOpenFileNames(this, "Open File...", dir.path(), "cub Files (*.cub)");
    if(fileNames.count()==0)
    {
      return;
    }
    fileName =fileNames[0];
  }
  // Cache the directory for the next time the dialog is opened
  QFileInfo info(fileName);
  this->Internal->background_full_path = info.absoluteFilePath().toStdString();
  this->Internal->Background->setText(info.fileName());
}

void cmbNucReactorVesselWidget::onClearBackgroundMesh()
{
  this->Internal->background_full_path = "";
  Internal->Background->setText("");
}

void cmbNucReactorVesselWidget::setWidth(double v)
{
  this->Internal->ductsize[0] = v;
  if(this->Internal->JacketMode->currentIndex() == cmbNucCoreParams::ReactorVessel::Generate)
  {
    this->onCalculateCylinderDefaults(true);
  }
}

void cmbNucReactorVesselWidget::setHeight(double v)
{
  this->Internal->ductsize[1] = v;
  if(this->Internal->JacketMode->currentIndex() == cmbNucCoreParams::ReactorVessel::Generate)
  {
    this->onCalculateCylinderDefaults(true);
  }
}
