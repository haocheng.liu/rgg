#ifndef _cmbNucWidgetChangeChecker_h_
#define _cmbNucWidgetChangeChecker_h_

#include <QObject>
#include <QLineEdit>
#include <QDoubleSpinBox>
#include <QWidget>
#include <QPointer>

#include <map>
#include <string>

#include <boost/bind.hpp>
#include <boost/function.hpp>

//template <class TYPE, class FUNCTION>
//class cmbNucWidgetChangeCheckerFunctionCaller

class cmbNucWidgetChangeChecker: public QObject
{
  Q_OBJECT
public:

  enum mode{do_nothing, reverted_to_orginal, newly_changed};
  class cmbNucWidgetChangeCheckerWrapper
  {
  public:
    virtual ~cmbNucWidgetChangeCheckerWrapper(){}
    virtual mode check() = 0;
    virtual void apply() = 0;
    virtual void reset() = 0;
    virtual void disconnect(cmbNucWidgetChangeChecker *) = 0;
    virtual void connect(cmbNucWidgetChangeChecker *) = 0;
    virtual size_t getNumberOfObjects() = 0;
    virtual QObject * getObject(size_t i) = 0;

    template<typename DTYPE> static DTYPE justReturn(DTYPE t) {return t;}
    static bool isNotEmtpyString(QString s){return !s.isEmpty();}
    static void nothingToSet() {return;}
    template<typename DTYPE> static void doNothing(DTYPE){}
    template<typename DTYPE> static bool alwaysValid(DTYPE){ return true; }
    static bool isNumber(QString in)
    {
      bool ok;
      in.toDouble(&ok);
      return ok;
    }
    static int toInt(QString in) { return in.toInt(); }
    static double toDouble(QString in) { return in.toDouble(); }
    static std::string toStdString(QString in) { return in.toStdString(); }

    template<typename DTYPE>
    static QString toNumber(DTYPE in)
    {
      return QString::number(in);
    }

    static QString replace_space_converter(QString in);

  protected:
    template<typename QType> void connect( QType * qt, cmbNucWidgetChangeChecker * t );
    template<typename QType> void disconnect( QType * qt, cmbNucWidgetChangeChecker * t );
  };

  cmbNucWidgetChangeChecker();
  ~cmbNucWidgetChangeChecker();

  void reset();
  void clear();
  void apply();

  bool needToApply();

  //Takes ownership
  void registerToTrack( cmbNucWidgetChangeCheckerWrapper * val );

signals:
  void sendApply();
  void sendClear();
  void sendReset();

public slots:
  void changeDetected();
  void updateMode(cmbNucWidgetChangeChecker::mode);

protected:
  std::map<QObject*, cmbNucWidgetChangeCheckerWrapper* > objectToChecker;
  typedef std::map<QObject*, cmbNucWidgetChangeCheckerWrapper* > checker_map_type;
private:
  int numberOfChanges;

};

class cmbNucFunctionWrapperHelper
{
public:
  template<typename QT_WIDGET, typename QT_WIDGET_RETURN_TYPE>
  static boost::function<QT_WIDGET_RETURN_TYPE (void)> wrapQtGetFunction(QT_WIDGET * widget);

  template<typename QT_WIDGET, typename QT_WIDGET_RETURN_TYPE>
  static boost::function<void (QT_WIDGET_RETURN_TYPE)> wrapQtSetFunction(QT_WIDGET * widget);

  template<typename QT_WIDGET>
  static boost::function<void (void)> wrapQtClearFunction(QT_WIDGET * widget);

  template<typename TO_DATA_TYPE, typename FROM_DATA_TYPE>
  static boost::function<TO_DATA_TYPE (FROM_DATA_TYPE)> wrapDataType();

};

template <typename DATATYPE>
class cmbNucGetSetDataUnqueChecker
{
public:
  virtual ~cmbNucGetSetDataUnqueChecker(){}
  virtual bool check(DATATYPE const&) { return true; }
  virtual void displayMessage(DATATYPE const&) {}
};

class cmbNucCheckableWidget: public QWidget
{
  Q_OBJECT
public:
  cmbNucCheckableWidget(QWidget* p): QWidget(p), change(false){}
  virtual ~cmbNucCheckableWidget(){}

public slots:
  void apply() { this->onApply(); change = false; }
  void reset() { this->onReset(); change = false; }
  void clear() { this->onClear(); }
  bool isDifferent() { return change; }
  virtual void setValueChanged(bool changeIn)
  {
    if(changeIn)
    {
      if(change) emit sendMode(cmbNucWidgetChangeChecker::do_nothing);
      else emit sendMode(cmbNucWidgetChangeChecker::newly_changed);
      change = true;
    }
    else
    {
      if(change) emit sendMode(cmbNucWidgetChangeChecker::reverted_to_orginal);
      else emit sendMode(cmbNucWidgetChangeChecker::do_nothing);
      change = false;
    }
  }
signals:
  void sendMode(cmbNucWidgetChangeChecker::mode);
protected:
  virtual void onApply() = 0;
  virtual void onReset() = 0;
  virtual void onClear() = 0;
private:
  bool change;
};

template <typename QT_WIDGET, typename QT_DATA_TYPE, typename DATATYPE>
class cmbNucGetSetData: public cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
{
public:
  typedef cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper parent;
  cmbNucGetSetData(QT_WIDGET * qw,
                   boost::function<DATATYPE (void)> gd, boost::function<void (DATATYPE)> sd,
                   boost::function<void (void)> sdi, boost::function<bool (void)> dis,

                   //Data conversion functions
                   boost::function<bool (QT_DATA_TYPE)> ivd,

                   //Post apply function
                   boost::function<void (bool)> postApplyIn)
  : widget(qw), getData(gd), setData(sd), setDataInvalid(sdi), dataIsSet(dis),isValidData(ivd),
    data_to_widget(cmbNucFunctionWrapperHelper::wrapDataType<QT_DATA_TYPE, DATATYPE>()),
    widget_to_data(cmbNucFunctionWrapperHelper::wrapDataType<DATATYPE, QT_DATA_TYPE>()),
    getWidgetData(cmbNucFunctionWrapperHelper::wrapQtGetFunction<QT_WIDGET,QT_DATA_TYPE>(qw)),
    setWidgetData(cmbNucFunctionWrapperHelper::wrapQtSetFunction<QT_WIDGET,QT_DATA_TYPE>(qw)),
    setWidgetDataNone(cmbNucFunctionWrapperHelper::wrapQtClearFunction(qw)),
    postApply(postApplyIn)
  {
    change = false;
    this->reset();
    uniqueChecker = new cmbNucGetSetDataUnqueChecker<DATATYPE>();
  }

  virtual ~cmbNucGetSetData()
  {
    delete uniqueChecker;
  }

  virtual void setWidgetToData(boost::function<DATATYPE (QT_DATA_TYPE)> fun)
  {
    widget_to_data = fun;
  }

  virtual cmbNucWidgetChangeChecker::mode check()
  {
    bool dset = dataIsSet();
    QT_DATA_TYPE qtData = getWidgetData();
    bool dwSet = isValidData(qtData);
    DATATYPE old = getData();
    bool currentChangeState = (dset != dwSet || (dset && dwSet && old !=  widget_to_data(qtData)));
    if( change == currentChangeState ) return cmbNucWidgetChangeChecker::do_nothing;
    change = currentChangeState;
    return (change) ? cmbNucWidgetChangeChecker::newly_changed :
                      cmbNucWidgetChangeChecker::reverted_to_orginal;
  }

  virtual void apply()
  {
    QT_DATA_TYPE wrt = getWidgetData();
    if(isValidData(wrt))
    {
      DATATYPE tmp = widget_to_data(wrt);
      if(uniqueChecker->check(tmp))
      {
        setData(tmp);
      }
      else
      {
        uniqueChecker->displayMessage(tmp);
        change = false;
      }
    }
    else
    {
      setDataInvalid();
    }
    postApply(change);
    change = false;
    this->reset();
  }

  void addUniqueChecker(cmbNucGetSetDataUnqueChecker<DATATYPE> * in) //Takes ownership
  {
    if(in != NULL)
    {
      delete uniqueChecker;
      uniqueChecker = in;
    }
  }

  virtual void reset()
  {
    if(dataIsSet())
    {
      DATATYPE tmp = getData();
      setWidgetData(data_to_widget(tmp));
    }
    else
    {
      setWidgetDataNone();
    }

    change = false;
  }

  virtual void disconnect(cmbNucWidgetChangeChecker * cmwcc)
  {
    QT_WIDGET * w = widget.data();
    if(w != NULL) parent::disconnect(w, cmwcc);
  }

  virtual void connect(cmbNucWidgetChangeChecker * cmwcc)
  {
    QT_WIDGET * w = widget.data();
    if(w != NULL) parent::connect(w, cmwcc);
  }

  virtual size_t getNumberOfObjects()
  {
    return 1;
  }

  virtual QObject * getObject(size_t /*i*/)
  {
    return widget;
  }
protected:
  QPointer<QT_WIDGET> widget;
  bool change;
  //Data accessor functions
  boost::function<DATATYPE (void)> getData;
  boost::function<void (DATATYPE)> setData;
  boost::function<void (void)> setDataInvalid;
  boost::function<bool (void)> dataIsSet;

  //Data conversion functions
  boost::function<bool (QT_DATA_TYPE)> isValidData;
  boost::function<QT_DATA_TYPE (DATATYPE)> data_to_widget;
  boost::function<DATATYPE (QT_DATA_TYPE)> widget_to_data;

  //WIDGET Access Functions
  boost::function<QT_DATA_TYPE (void)> getWidgetData;
  boost::function<void (QT_DATA_TYPE)> setWidgetData;
  boost::function<void (void)> setWidgetDataNone;

  boost::function<void (bool)> postApply;

  cmbNucGetSetDataUnqueChecker<DATATYPE> * uniqueChecker;

};

#endif