
#include "cmbNucInputListWidget.h"
#include "ui_qInputListWidget.h"

#include "cmbNucAssembly.h"
#include "cmbNucAssemblyLink.h"
#include "cmbNucCore.h"
#include "cmbNucPartsTreeItem.h"
#include "cmbNucMaterialColors.h"
#include "cmbNucDefaults.h"
#include "cmbNucPartLibrary.h"

#include <boost/bind.hpp>

#include <QLabel>
#include <QPointer>
#include <QTreeWidget>
#include <QAction>
#include <QBrush>
#include <QFileDialog>
#include <QColorDialog>
#include <QHeaderView>
#include <QItemDelegate>
#include <QMenu>
#include <QHeaderView>
#include <QPainter>
#include <QTreeWidgetItem>

extern bool IS_IN_TESTING_MODE;

class PartsItemDelegate: public QItemDelegate
{
public:
  PartsItemDelegate(QObject* pParent = 0) : QItemDelegate(pParent)
  {
  }

  void paint(QPainter* pPainter, const QStyleOptionViewItem& rOption, const QModelIndex& rIndex) const
  {
    QStyleOptionViewItemV4 ViewOption(rOption);

    QString oldText;

    QColor ItemForegroundColor = rIndex.data(Qt::ForegroundRole).value<QColor>();
    if (ItemForegroundColor.isValid())
    {
      if (ItemForegroundColor != rOption.palette.color(QPalette::WindowText))
      {
        ViewOption.palette.setColor(QPalette::Highlight, ItemForegroundColor);
      }
    }
    QItemDelegate::paint(pPainter, ViewOption, rIndex);
  }

  QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
  {
    QSize tmp = QItemDelegate::sizeHint(option, index);
    tmp.setHeight(15);
    return tmp;
  }
};

class MaterialItemDelegate: public QItemDelegate
{
public:
  MaterialItemDelegate(QObject* pParent = 0) : QItemDelegate(pParent)
  {
  }

  void paint(QPainter* pPainter, const QStyleOptionViewItem& rOption, const QModelIndex& rIndex) const
  {
    if(rOption.state & QStyle::State_Selected)
    {

      if(rIndex.column() == 3)
      {
        QStyleOptionViewItem unSelect = rOption;
        unSelect.state &= (~QStyle::State_Selected);
        QItemDelegate::paint(pPainter,unSelect,rIndex);
        return;
      }
    }
    QItemDelegate::paint(pPainter,rOption,rIndex);
  }
  QTreeWidget* treeWidget;
};


class cmbNucInputListWidgetInternal :
  public Ui::InputListWidget
{
public:
  cmbNucInputListWidgetInternal()
  {
    previousMeshOrModelTab = -1;
    this->RootCoreNode = NULL;
    TreeItemDelegate = new PartsItemDelegate();
    MaterialDelegate = new MaterialItemDelegate();
  }
  virtual ~cmbNucInputListWidgetInternal()
  {
    delete this->Action_NewAssembly;
    delete this->Action_NewAssemblyLink;
    delete this->Action_NewPin;
    delete this->Action_NewDuct;
    delete this->Action_DeletePart;
    delete this->TreeItemDelegate;
    delete this->MaterialDelegate;
    delete this->Action_NewBlank;
  }
  void initActions()
  {
    this->Action_NewAssembly = new QAction("Create Assembly", this->PartsList);
    this->Action_NewAssemblyLink = new QAction("Create Same As Assembly", this->PartsList);
    this->Action_NewBlank = new QAction("Create External", this->PartsList);
    this->Action_NewPin = new QAction("Create Pin", this->PartsList);
    this->Action_NewDuct = new QAction("Create Duct", this->PartsList);
    this->Action_DeletePart = new QAction("Delete Selected", this->PartsList);
    this->Action_Clone = new QAction("Clone Selected", this->PartsList);
    this->setPartOptions(this->PartsList);
  }

  void setPartOptions(QWidget * qw)
  {
    qw->addAction(this->Action_NewAssembly);
    qw->addAction(this->Action_NewAssemblyLink);
    qw->addAction(this->Action_NewBlank);
    qw->addAction(this->Action_NewPin);
    qw->addAction(this->Action_NewDuct);
    qw->addAction(this->Action_Clone);
    qw->addAction(this->Action_DeletePart);
  }

  QPointer<QAction> Action_NewAssembly;
  QPointer<QAction> Action_NewAssemblyLink;
  QPointer<QAction> Action_NewPin;
  QPointer<QAction> Action_NewDuct;
  QPointer<QAction> Action_DeletePart;
  QPointer<QAction> Action_Clone;
  QPointer<QAction> Action_NewBlank;

  cmbNucPartsTreeItem* RootCoreNode;
  cmbNucPartsTreeItem* AssemblyNode;
  cmbNucPartsTreeItem* PinsNode;
  cmbNucPartsTreeItem* DuctsNode;
  cmbNucPartsTreeItem* BlankNode;

  PartsItemDelegate * TreeItemDelegate;
  MaterialItemDelegate * MaterialDelegate;
  int previousMeshOrModelTab; //contains the previous tab except for material
  std::map<cmbNucPart*, cmbNucPartsTreeItem*> partToTreeItem;
};

//-----------------------------------------------------------------------------
cmbNucInputListWidget::cmbNucInputListWidget(QWidget* _p)
  : QWidget(_p)
{
  this->NuclearCore = NULL;
  this->Internal = new cmbNucInputListWidgetInternal;
  this->Internal->setupUi(this);
  this->Internal->initActions();

  this->modelIsLoaded(false);
  this->Internal->tabInputs->setTabEnabled(2, false);

  this->Internal->PartsList->setAlternatingRowColors(true);
  this->Internal->PartsList->header()->setResizeMode(QHeaderView::ResizeToContents);

  this->setBoundaryLayerControlMode(false);

  // set up the UI trees
  QTreeWidget* treeWidget = this->Internal->PartsList;
  treeWidget->setItemDelegate(this->Internal->TreeItemDelegate);

  this->Internal->MaterialTree->setItemDelegate(this->Internal->MaterialDelegate);
  this->Internal->MaterialDelegate->treeWidget = this->Internal->MaterialTree;

  // context menu for parts tree
  QObject::connect(this->Internal->Action_NewAssembly, SIGNAL(triggered()),
                   this, SLOT(onNewAssembly()));
  QObject::connect(this->Internal->Action_NewAssemblyLink, SIGNAL(triggered()),
                   this, SLOT(onNewAssemblyLink()));
  QObject::connect(this->Internal->Action_NewBlank, SIGNAL(triggered()),
                   this, SLOT(onNewBlank()));
  QObject::connect(this->Internal->Action_NewPin, SIGNAL(triggered()),
                   this, SLOT(onNewPin()));
  QObject::connect(this->Internal->Action_NewDuct, SIGNAL(triggered()),
                   this, SLOT(onNewDuct()));
  QObject::connect(this->Internal->Action_Clone, SIGNAL(triggered()),
                   this, SLOT(onClone()));
  QObject::connect(this->Internal->Action_DeletePart, SIGNAL(triggered()),
                   this, SLOT(onRemoveSelectedPart()));

  QObject::connect(this->Internal->newMaterial, SIGNAL(clicked()),
                   cmbNucMaterialColors::instance(), SLOT(CreateNewMaterial()));
  QObject::connect(this->Internal->delMaterial, SIGNAL(clicked()),
                   cmbNucMaterialColors::instance(), SLOT(deleteSelected()));
  QObject::connect(this->Internal->hideLabels, SIGNAL(clicked(bool)),
                   this,                       SLOT(hideLabels(bool)));
  QObject::connect(this->Internal->importMaterial, SIGNAL(clicked()),
                   this, SLOT(onImportMaterial()));
  QObject::connect(this->Internal->saveMaterial, SIGNAL(clicked()),
                   this, SLOT(onSaveMaterial()));
  QObject::connect(this->Internal->materialDisplayed, SIGNAL(currentIndexChanged(int)),
                   cmbNucMaterialColors::instance(), SLOT(controlShow(int)));

  QObject::connect(this->Internal->PartsList, SIGNAL(itemSelectionChanged()),
                   this, SLOT(onPartsSelectionChanged()), Qt::QueuedConnection);

  QObject::connect(this->Internal->MaterialTree, SIGNAL(itemClicked (QTreeWidgetItem*, int)),
                   this, SLOT(onMaterialClicked(QTreeWidgetItem*, int)), Qt::QueuedConnection);

  QObject::connect(this->Internal->MeshComponents, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem * )),
                   this, SIGNAL(subMeshSelected(QTreeWidgetItem *)), Qt::QueuedConnection);

  QObject::connect(this->Internal->MeshComponents, SIGNAL(itemChanged(QTreeWidgetItem*, int)),
                   this, SIGNAL(meshValueChanged(QTreeWidgetItem*)));

  QObject::connect(this->Internal->tabInputs, SIGNAL(currentChanged(int)),
                   this, SLOT(onTabChanged(int)));

  QObject::connect(this->Internal->color_control, SIGNAL(currentIndexChanged(int)),
                   this, SIGNAL(sendColorControl(int)));
  QObject::connect(this->Internal->edge_control, SIGNAL(clicked(bool)),
                   this, SIGNAL(sendEdgeControl(bool)));

  QObject::connect(this->Internal->drawBoundaryLayer,  SIGNAL(clicked(bool)),
                   this, SIGNAL(drawBoundaryControl(bool)));

  QObject::connect(this->Internal->meshMajorSet, SIGNAL(currentIndexChanged(int)),
                   this, SIGNAL(majorMeshSelection(int)));

  QObject::connect(this->Internal->resetCamera, SIGNAL(clicked()), this, SIGNAL(resetMeshCamera()));

  this->initUI();
}

//-----------------------------------------------------------------------------
cmbNucInputListWidget::~cmbNucInputListWidget()
{
  delete this->Internal;
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget::setPartOptions(QMenu * qm) const
{
  qm->clear();
  this->Internal->setPartOptions(qm);
}

void cmbNucInputListWidget::clear()
{
  this->clearTable();
  this->setCore(NULL);
  this->setEnabled(false);
  this->initPartsTree();
  if(this->Internal->RootCoreNode)
  {
    this->Internal->RootCoreNode = NULL;
    this->Internal->AssemblyNode = NULL;
    this->Internal->BlankNode = NULL;
    this->Internal->PinsNode = NULL;
    this->Internal->DuctsNode = NULL;
  }
  this->modelIsLoaded(false);
  this->Internal->tabInputs->setTabEnabled(2, false);
}

bool cmbNucInputListWidget::onlyMeshLoaded()
{
  return this->isEnabled() && !this->Internal->tabInputs->isTabEnabled(0);
}

void cmbNucInputListWidget::meshIsLoaded(bool v)
{
  if(this->onlyMeshLoaded() || v) this->setEnabled(v);
  if(this->onlyMeshLoaded() || v) this->Internal->tabInputs->setTabEnabled(1, v);
  this->Internal->tabInputs->setTabEnabled(2, v);
  if(v) this->initMaterialsTree();
  if(v && onlyMeshLoaded()) this->Internal->tabInputs->setCurrentIndex(2);
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget::setCore(cmbNucCore* core)
{
  if(this->NuclearCore == core)
    {
    return;
    }
  if(this->NuclearCore!=NULL)
  {
    disconnect(this->NuclearCore->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
               this,                               SLOT(repaintList()));
  }
  this->NuclearCore = core;
  if(this->NuclearCore!=NULL)
  {
    connect( this->NuclearCore->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
             this,                               SLOT(repaintList()) );
  }
}

//-----------------------------------------------------------------------------
cmbNucPart* cmbNucInputListWidget::getSelectedPart()
{
  cmbNucPartsTreeItem* selItem = this->getSelectedItem(this->Internal->PartsList);
  if(!selItem || !selItem->getPartObject()) return NULL;
  return selItem->getPartObject();
}
//-----------------------------------------------------------------------------
cmbNucPartsTreeItem* cmbNucInputListWidget::getSelectedPartNode()
{
  return this->getSelectedItem(this->Internal->PartsList);
}

//-----------------------------------------------------------------------------
cmbNucPartsTreeItem* cmbNucInputListWidget::getCurrentAssemblyNode()
{
  cmbNucPartsTreeItem* selItem = this->getSelectedItem(this->Internal->PartsList);
  if(!selItem || !selItem->getPartObject()) return NULL;

  cmbNucPart* selObj = selItem->getPartObject();
  QTreeWidgetItem* pItem=NULL;
  enumNucPartsType selType = selObj->GetType();
  switch(selType)
  {
    case CMBNUC_ASSY_DUCTCELL:
    case CMBNUC_ASSY_PINCELL:
    case CMBNUC_ASSY_BASEOBJ:
      return( NULL );
      break;
    case CMBNUC_ASSEMBLY_LINK:
    case CMBNUC_ASSEMBLY:
      pItem = selItem;
      break;
    case CMBNUC_CORE:
      if(selItem->childCount()==1)
      {
        pItem = selItem->child(0);
      }
      break;
    default:
      break;
  }
  return pItem ? dynamic_cast<cmbNucPartsTreeItem*>(pItem) : NULL;
}

//-----------------------------------------------------------------------------
cmbNucAssembly* cmbNucInputListWidget::getCurrentAssembly()
{
  cmbNucPartsTreeItem* assyItem = this->getCurrentAssemblyNode();
  cmbNucAssembly * assy = NULL;
  if(assyItem && assyItem->getPartObject())
  {
    cmbNucPart* selObj = assyItem->getPartObject();
    if(selObj->GetType() == CMBNUC_ASSEMBLY)
    {
      assy = dynamic_cast<cmbNucAssembly*>(selObj);
    }
    else if(selObj->GetType() == CMBNUC_ASSEMBLY_LINK)
    {
      assy = dynamic_cast<cmbNucAssemblyLink*>(selObj)->getLink();
    }
  }
  return assy;
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::initUI()
{
  this->setActionsEnabled(false);
  if(this->Internal->RootCoreNode)
  {
    delete this->Internal->RootCoreNode; // Other nodes are children of this one
                                         // so they will be deleted
    this->Internal->RootCoreNode = NULL;
    this->Internal->AssemblyNode = NULL;
    this->Internal->BlankNode = NULL;
    this->Internal->DuctsNode = NULL;
    this->Internal->PinsNode = NULL;
  }
  this->initPartsTree();
  this->initMaterialsTree();
}
//----------------------------------------------------------------------------
void cmbNucInputListWidget::onTabChanged(int currentTab)
{
  if(currentTab == 0)
  {
    if(this->Internal->previousMeshOrModelTab != 0) emit raiseModelDock();
    this->Internal->previousMeshOrModelTab = 0;
  }
  if(currentTab == 1) // materials
  {
    int i = this->Internal->materialDisplayed->currentIndex();
    cmbNucMaterialColors::instance()->controlShow(i);
  }
  if(currentTab == 2)
  {
    if(this->Internal->previousMeshOrModelTab != 2) emit raiseMeshDock();
    this->Internal->previousMeshOrModelTab = 2;
  }
 }
//----------------------------------------------------------------------------
void cmbNucInputListWidget::setActionsEnabled(bool val)
{
  this->Internal->Action_NewAssembly->setEnabled(val);
  this->Internal->Action_NewAssemblyLink->setEnabled(val);
  this->Internal->Action_NewPin->setEnabled(val);
  this->Internal->Action_NewDuct->setEnabled(val);
  this->Internal->Action_Clone->setEnabled(val);
  this->Internal->Action_DeletePart->setEnabled(val);
}
//----------------------------------------------------------------------------
void cmbNucInputListWidget::onNewAssembly()
{
  cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
  double r,g,b;
  matColorMap->CalcRGB(r,g,b);

  this->setEnabled(1);
  boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
  QString label = cmbNucPart::getUnique(fun, "A", false, true);
  fun = boost::bind(&cmbNucCore::name_unique, this->NuclearCore, _1);
  QString name = cmbNucPart::getUnique(fun, "Assembly", false, true);

  cmbNucAssembly* assembly = new cmbNucAssembly(label, name, QColor::fromRgbF(r,g,b));
  if(this->NuclearCore->IsHexType())
  {
    assembly->setGeometryLabel("Hexagonal");
    assembly->getLattice().SetDimensions(1, 0, true);
  }
  else
  {
    assembly->setGeometryLabel("Rectangular");
  }

  this->NuclearCore->addPart(assembly);
  assembly->computeDefaults();
  assembly->setFromDefaults(this->NuclearCore->GetDefaults());

  // set neumann/material IDs
  int id = this->NuclearCore->getFreeId();
  assembly->GetParameters()->setNeumannSet_StartId(id);
  assembly->GetParameters()->setMaterialSet_StartId(id);

  this->initCoreRootNode();
  this->updateWithPart(assembly);
  emit assembliesModified(this->NuclearCore);
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::onNewAssemblyLink()
{
  cmbNucAssembly * assy = this->getCurrentAssembly();
  if(assy == NULL)
  {
    return;
  }

  int nid = this->NuclearCore->getFreeLinkId(assy->GetParameters()->getNeumannSet_StartId());
  int mid = this->NuclearCore->getFreeLinkId(assy->GetParameters()->getMaterialSet_StartId());
  cmbNucAssemblyLink * link = new cmbNucAssemblyLink(assy, nid, mid);

  boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
  link->setLabel(cmbNucPart::getUnique(fun, "A", false, true));
  fun = boost::bind(&cmbNucCore::name_unique, this->NuclearCore, _1);
  link->setName(cmbNucPart::getUnique(fun, "Assembly", false, true));

  double r,g,b;
  cmbNucMaterialColors::instance()->CalcRGB(r,g,b);
  link->SetLegendColor(QColor::fromRgbF(r,g,b));

  if(!this->NuclearCore->addPart(link))
  {
    delete link;
    return;
  }
  this->initCoreRootNode();
  this->updateWithPart(link);
  this->NuclearCore->getConnection()->componentChanged(NULL);
  emit assembliesModified(this->NuclearCore);
}

void cmbNucInputListWidget::onNewBlank()
{
  boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
  double r,g,b;
  cmbNucMaterialColors::instance()->CalcRGB(r,g,b);
  cmbNucPart * blank = new cmbNucPart(cmbNucPart::getUnique(fun, "B", false, true), "",
                                      QColor::fromRgbF(r,g,b));
  this->NuclearCore->addPart(blank);
  this->initCoreRootNode();
  this->updateWithPart(blank);
}

void cmbNucInputListWidget::onNewDuct()
{
  DuctCell * cd = new DuctCell();
  QPointer<cmbNucDefaults> defaults = this->NuclearCore->GetDefaults();
  double d1,d2, h = defaults->getHeight();
  defaults->getDuctThickness(d1,d2);
  cd->AddDuct(new Duct(h, d1, d2));
  boost::function<bool (QString)> fun =
            boost::bind(&cmbNucPartLibrary::nameConflicts, this->NuclearCore->getDuctLibrary(), _1);
  cd->setName(cmbNucPart::getUnique(fun, "Duct", true, true));
  cd->setLabel(cd->getName());
  this->NuclearCore->getDuctLibrary()->addPart(cd);
  this->initCoreRootNode();
  this->updateWithPart(cd, true);
  emit assembliesModified(this->NuclearCore);
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::onNewPin()
{
  cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
  double rgb[3];
  matColorMap->CalcRGB(rgb[0],rgb[1],rgb[2]);

  cmbNucAssembly * assy = this->getCurrentAssembly();
  double h = this->NuclearCore->GetDefaults()->getHeight(), r = 0.5;
  if(assy != NULL) assy->calculateRadius(r);
  PinCell* newpin = new PinCell();
  newpin->SetLegendColor(QColor::fromRgbF(rgb[0],rgb[1],rgb[2]));
  newpin->AddPart(new Cylinder(r, 0, h));

  boost::function<bool (QString)> fun =
            boost::bind(&cmbNucPartLibrary::labelConflicts, this->NuclearCore->getPinLibrary(), _1);
  newpin->setLabel(cmbNucPart::getUnique(fun, "PC", true, true));
  fun = boost::bind(&cmbNucPartLibrary::nameConflicts, this->NuclearCore->getPinLibrary(), _1);
  newpin->setName(cmbNucPart::getUnique(fun, "PinCell", true, true));

  this->NuclearCore->getPinLibrary()->addPart(newpin);
  this->initCoreRootNode();
  this->updateWithPart(newpin, true);
  emit assembliesModified(this->NuclearCore);
  //emit checkSavedAndGenerate();
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::onRemoveSelectedPart()
{
  cmbNucPartsTreeItem* selItem = this->getSelectedItem(this->Internal->PartsList);
  if(!selItem || !selItem->getPartObject())
  {
    return;
  }
  cmbNucPart* selObj = selItem->getPartObject();

  enumNucPartsType selType = selObj->GetType();
  switch(selType)
  {
    case CMBNUC_CORE:
      emit deleteCore();
      break;
    case CMBNUC_ASSY_BASEOBJ:
    case CMBNUC_ASSEMBLY:
    case CMBNUC_ASSY_PINCELL:
    case CMBNUC_ASSY_DUCTCELL:
    case CMBNUC_ASSEMBLY_LINK:
    {
      this->setCursor(Qt::BusyCursor);
      this->Internal->PartsList->blockSignals(true);
      QTreeWidgetItem * p = selItem->parent();
      emit deleteObj(selObj);
      delete selItem;
      this->Internal->PartsList->setCurrentItem(p);
      this->Internal->PartsList->blockSignals(false);
      this->onPartsSelectionChanged();
      this->unsetCursor();
      break;
    }
    default:
      break;
  }
  //emit checkSavedAndGenerate();
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::onImportMaterial()
{
  QStringList fileNames =
    QFileDialog::getOpenFileNames(this,
                                 "Open Material File...",
                                 QDir::homePath(),
                                 "INI Files (*.ini);;All Files (*.*)");
  if(fileNames.count() == 0)
    {
    return;
    }
  this->setCursor(Qt::BusyCursor);
  cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
  foreach(QString fileName, fileNames)
  {
    if(!fileName.isEmpty())
    {
      if(matColorMap->OpenFile(fileName))
      {
        this->initMaterialsTree();// reload the material tree.
      }
    }
  }
  this->unsetCursor();
}
//----------------------------------------------------------------------------
void cmbNucInputListWidget::onSaveMaterial()
{
  QString fileName = QFileDialog::getSaveFileName(this, "Save Material File...",
                                                  QDir::homePath(), "INI Files (*.ini)");
  if(!fileName.isEmpty())
  {
    this->setCursor(Qt::BusyCursor);
    cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
    matColorMap->SaveToFile(fileName);
    this->unsetCursor();
  }
}
//----------------------------------------------------------------------------
void cmbNucInputListWidget::onMaterialClicked(QTreeWidgetItem* item, int col)
{
  if(col != 3)
  {
    return;
  }

  QBrush bgBrush = item->background(col);
  QColorDialog::ColorDialogOptions options = QColorDialog::ShowAlphaChannel;
  if(IS_IN_TESTING_MODE)
  {
    options |= QColorDialog::DontUseNativeDialog;
  }
  QColor color = QColorDialog::getColor(bgBrush.color(), this,
                                        "Select Color for Material", options);
  if(color.isValid() && color != bgBrush.color())
  {
    bgBrush.setColor(color);
    item->setBackground(col, bgBrush);
  }
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget::updateUI(bool selCore)
{
  this->initUI();
  if(!this->NuclearCore)
  {
    this->setEnabled(0);
    return;
  }
  this->setEnabled(1);
  this->initCoreRootNode();
  this->updateWithPartLibrary(this->NuclearCore->getPinLibrary(), this->Internal->PinsNode);
  this->updateWithPartLibrary(this->NuclearCore->getDuctLibrary(), this->Internal->DuctsNode);
  this->updateWithCoreParts();

  if(selCore)
  {
    this->Internal->RootCoreNode->setSelected(true);
    this->onPartsSelectionChanged();
  }
}
//-----------------------------------------------------------------------------
void cmbNucInputListWidget::initCoreRootNode()
{
  if(this->Internal->RootCoreNode == NULL)
  {
    cmbNucPartsTreeItem* root = this->Internal->RootCoreNode =
                            new cmbNucPartsTreeItem(this->Internal->PartsList->invisibleRootItem(),
                                                    this->NuclearCore, "core");
    this->Internal->partToTreeItem[this->NuclearCore] = root;
    connect(this,             SIGNAL(checkSavedAndGenerate()),
            root->connection, SLOT(checkSaveAndGenerate()));
    root->setExpanded(true);

    this->Internal->PinsNode = new cmbNucPartsTreeItem(root, NULL, "Pins");
    this->Internal->DuctsNode = new cmbNucPartsTreeItem(root, NULL, "Ducts");
    this->Internal->AssemblyNode = new cmbNucPartsTreeItem(root,  NULL, "Assemblies");
    this->Internal->BlankNode = new cmbNucPartsTreeItem(root, NULL, "External");
  }
}

void cmbNucInputListWidget::setBoundaryLayerControlMode(bool enabled)
{
  this->Internal->drawBoundaryLayer->setEnabled(enabled);
}

void cmbNucInputListWidget::updateWithCoreParts()
{
  QList<QTreeWidgetItem *> tmpl = this->Internal->AssemblyNode->takeChildren();
  for(QList<QTreeWidgetItem *>::iterator i = tmpl.begin(); i != tmpl.end(); ++i)
  {
    delete *i;
  }
  tmpl = this->Internal->BlankNode->takeChildren();
  for(QList<QTreeWidgetItem *>::iterator i = tmpl.begin(); i != tmpl.end(); ++i)
  {
    delete *i;
  }
  for(size_t i=0; i<this->NuclearCore->getNumberOfParts(); i++)
  {
    this->updateWithPart(this->NuclearCore->getPart(i), false);
  }
}

void cmbNucInputListWidget::updateWithPart(cmbNucPart* part, bool select)
{
  cmbNucPartsTreeItem* parentItem = NULL;
  switch( part->GetType() )
  {
    case CMBNUC_ASSEMBLY:
      parentItem = this->Internal->AssemblyNode;
      connect(part->getConnection(),     SIGNAL(partChanged(cmbNucPart*)),
              this->Internal->PartsList, SLOT(update()));
      break;
    case CMBNUC_ASSEMBLY_LINK:
    {
      std::map<cmbNucPart*, cmbNucPartsTreeItem*>::const_iterator node =
            this->Internal->partToTreeItem.find(dynamic_cast<cmbNucAssemblyLink*>(part)->getLink());
      if( node != this->Internal->partToTreeItem.end())
      {
        parentItem =  node->second;
      }
      break;
    }
    case CMBNUC_ASSY_BASEOBJ:
      parentItem = this->Internal->BlankNode;
      break;
    case CMBNUC_ASSY_DUCTCELL:
      parentItem = this->Internal->DuctsNode;
      break;
    case CMBNUC_ASSY_PINCELL:
      parentItem = this->Internal->PinsNode;
      break;
    default:
      break;
  }
  if(parentItem == NULL) return;
  this->Internal->PartsList->blockSignals(true);
  cmbNucPartsTreeItem* partNode = new cmbNucPartsTreeItem(parentItem, part, "");
  this->Internal->partToTreeItem[part] = partNode;
  //connect(this,                      SIGNAL(checkSavedAndGenerate()),
  //        partNode->connection,      SLOT(checkSaveAndGenerate()));
  this->Internal->PartsList->blockSignals(false);

  if(select)
  {
    this->Internal->PartsList->setCurrentItem(partNode);
    this->onPartsSelectionChanged();
  }
}

void cmbNucInputListWidget::updateWithPinLibrary()
{
  this->updateWithPartLibrary(this->NuclearCore->getPinLibrary(), this->Internal->PinsNode);
}

void cmbNucInputListWidget
::updateWithPartLibrary(cmbNucPartLibrary * pl, cmbNucPartsTreeItem* parentItem)
{
  QList<QTreeWidgetItem *> tmpl = parentItem->takeChildren();
  for(QList<QTreeWidgetItem *>::iterator i = tmpl.begin(); i != tmpl.end(); ++i)
  {
    delete *i;
  }
  for(size_t i = 0; i < pl->GetNumberOfParts(); i++)
  {
    this->updateWithPart(pl->GetPart(static_cast<int>(i)));
  }
}

void cmbNucInputListWidget::updateWithDuctLibrary()
{
  this->updateWithPartLibrary(this->NuclearCore->getDuctLibrary(), this->Internal->DuctsNode);
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget::onPartsSelectionChanged()
{
  cmbNucPartsTreeItem* selItem = this->getSelectedItem(this->Internal->PartsList);
  this->updateContextMenu(selItem ? selItem->getPartObject() : NULL,selItem);
  this->fireObjectSelectedSignal(selItem);
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget
::updateContextMenu(cmbNucPart* selObj, const cmbNucPartsTreeItem* selItem)
{
  if(!selObj)
  {
    this->setActionsEnabled(false);

    if(selItem == NULL)
    {
      //DO nothing
    }
    else if(selItem == this->Internal->PinsNode)
    {
      this->Internal->Action_NewPin->setEnabled(true);
    }
    else if(selItem == this->Internal->DuctsNode)
    {
      this->Internal->Action_NewDuct->setEnabled(true);
    }
    else if(selItem == this->Internal->AssemblyNode)
    {
      this->Internal->Action_NewAssembly->setEnabled(true);
    }

    return;
  }
  this->setActionsEnabled(false);
  this->Internal->Action_NewAssembly->setEnabled(true);
  this->Internal->Action_NewPin->setEnabled(true);
  this->Internal->Action_NewDuct->setEnabled(true);
  this->setBoundaryLayerControlMode(false);

  switch(selObj->GetType())
  {
    case CMBNUC_ASSEMBLY:
      this->setBoundaryLayerControlMode(true);
      this->Internal->Action_DeletePart->setEnabled(true);
      this->Internal->Action_Clone->setEnabled(true);
      this->Internal->Action_NewAssemblyLink->setEnabled(true);
      break;
    case CMBNUC_ASSEMBLY_LINK:
      this->setBoundaryLayerControlMode(true);
      this->Internal->Action_DeletePart->setEnabled(true);
      this->Internal->Action_Clone->setEnabled(true);
      break;
    case CMBNUC_CORE:
      this->setBoundaryLayerControlMode(true);
      this->Internal->Action_NewPin->setEnabled(false);
      this->Internal->Action_NewDuct->setEnabled(false);
      this->Internal->Action_DeletePart->setEnabled(true);
      break;
    case CMBNUC_ASSY_PINCELL:
      this->Internal->Action_DeletePart->setEnabled(true);
      this->Internal->Action_Clone->setEnabled(true);
      break;
    case CMBNUC_ASSY_DUCTCELL:
    {
      DuctCell * dc = static_cast<DuctCell *>(selObj);
      this->Internal->Action_DeletePart->setEnabled(!dc->isUsed());
      this->Internal->Action_Clone->setEnabled(true);
      break;
    }
    case CMBNUC_ASSY_BASEOBJ:
    {
      this->Internal->Action_Clone->setEnabled(true);
      this->Internal->Action_DeletePart->setEnabled(true);
      break;
    }
    default:
      break;
  }
}

//-----------------------------------------------------------------------------
void cmbNucInputListWidget::fireObjectSelectedSignal(cmbNucPartsTreeItem* selItem)
{
  if(selItem)
  {
    emit this->objectSelected(selItem->getPartObject(), selItem->text(0).toStdString().c_str());
  }
  else
  {
    emit this->objectSelected(NULL, NULL);
  }
}

//-----------------------------------------------------------------------------
cmbNucPartsTreeItem* cmbNucInputListWidget::getSelectedItem(QTreeWidget* treeWidget)
{
  QList<QTreeWidgetItem*> selItems = treeWidget->selectedItems();
  return selItems.count()>0 ? dynamic_cast<cmbNucPartsTreeItem*>(selItems.value(0)) : NULL;
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::initPartsTree()
{
  QTreeWidget* treeWidget = this->Internal->PartsList;

  treeWidget->blockSignals(true);
  treeWidget->clear();
  this->Internal->partToTreeItem.clear();
  treeWidget->setHeaderLabels(QStringList() << tr("Name") << tr("") << tr("") << tr("FileName"));
  treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  treeWidget->setAcceptDrops(false);
  treeWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
  treeWidget->blockSignals(false);
}

//----------------------------------------------------------------------------
void cmbNucInputListWidget::initMaterialsTree()
{
  QTreeWidget* treeWidget = this->Internal->MaterialTree;

  treeWidget->blockSignals(true);
  treeWidget->clear();
  treeWidget->setHeaderLabels(
    QStringList() << tr("Show") << tr("Name") << tr("Label") << tr("Color") );
  treeWidget->setColumnCount(4);
  treeWidget->setAlternatingRowColors(true);
  treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
  treeWidget->header()->setResizeMode(1, QHeaderView::ResizeToContents);
  treeWidget->header()->setResizeMode(2, QHeaderView::ResizeToContents);
  treeWidget->setAcceptDrops(false);
  treeWidget->setContextMenuPolicy(Qt::NoContextMenu);

  cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
  matColorMap->buildTree(treeWidget);

  treeWidget->blockSignals(false);
}

void cmbNucInputListWidget::assemblyModified(cmbNucPartsTreeItem* /*assyNode*/)
{
  cmbNucAssembly* assem = this->getCurrentAssembly();
  if(assem)
  {
    assem->setAndTestDiffFromFiles(true);
    NuclearCore->setAndTestDiffFromFiles(true);
    this->Internal->PartsList->repaint();
  }
}

void cmbNucInputListWidget::repaintList()
{
  this->Internal->PartsList->repaint();
}

void cmbNucInputListWidget::coreModified()
{
  if( this->Internal->RootCoreNode && NuclearCore)
  {
    NuclearCore->setAndTestDiffFromFiles(true);
    this->Internal->PartsList->repaint();
  }
}

void cmbNucInputListWidget::valueChanged()
{
  cmbNucPart* part = this->getSelectedPart();
  cmbNucPartsTreeItem* selItem = this->getSelectedPartNode();
  if(part == NULL || selItem == NULL)
  {
    return;
  }
  switch(part->GetType())
  {
    case CMBNUC_ASSY_DUCTCELL:
    case CMBNUC_ASSY_PINCELL:
      //These are handled via connections
      break;
    case CMBNUC_ASSEMBLY:
      this->assemblyModified(selItem);
      break;
    case CMBNUC_CORE:
    case CMBNUC_ASSEMBLY_LINK:
    case CMBNUC_ASSY_BASEOBJ:
      this->coreModified();
      break;
  }
}

void cmbNucInputListWidget::clearTable()
{
  this->Internal->PartsList->clear();
  this->Internal->partToTreeItem.clear();
  this->Internal->MeshComponents->clear();
}

void cmbNucInputListWidget::hideLabels(bool v)
{
  this->Internal->MaterialTree->setColumnHidden(2,v);
}

void cmbNucInputListWidget::updateMeshTable(QList<QTreeWidgetItem*> meshParts)
{
  QTreeWidget* treeWidget = this->Internal->MeshComponents;

  treeWidget->blockSignals(true);
  treeWidget->clear();
  treeWidget->setHeaderLabels(QStringList() << tr("") << tr(""));
  treeWidget->setHeaderHidden(true);
  treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  treeWidget->setAcceptDrops(false);
  treeWidget->addTopLevelItems( meshParts );
  treeWidget->resizeColumnToContents(0);
  meshParts.at(0)->setSelected(true);
  treeWidget->blockSignals(false);
  emit(subMeshSelected(meshParts.at(0)));
}

void cmbNucInputListWidget::updateMainMeshComponents(QStringList parts, int sel)
{
  this->Internal->meshMajorSet->blockSignals(true);
  this->Internal->meshMajorSet->clear();
  this->Internal->meshMajorSet->addItems(parts);
  this->Internal->meshMajorSet->blockSignals(false);
  this->Internal->meshMajorSet->setCurrentIndex ( sel );
}

void cmbNucInputListWidget::modelIsLoaded(bool v)
{
  if(v) this->Internal->tabInputs->setCurrentIndex(0);
  this->Internal->tabInputs->setTabEnabled(0, v);
  this->Internal->tabInputs->setTabEnabled(1, v);
}

void cmbNucInputListWidget::selectMeshTab(bool v)
{
  if(v)
  {
    this->Internal->previousMeshOrModelTab = 2; //used to supress signal
    this->Internal->tabInputs->setCurrentIndex(2);
  }
}

void cmbNucInputListWidget::selectModelTab(bool v)
{
  if(v)
  {
    this->Internal->previousMeshOrModelTab = 0; //used to supress signal
    this->Internal->tabInputs->setCurrentIndex(0);
  }
}

void cmbNucInputListWidget::onClone()
{
  cmbNucPartsTreeItem* selItem = this->getSelectedItem(this->Internal->PartsList);
  if(!selItem || !selItem->getPartObject())
  {
    return;
  }
  cmbNucPart* selObj = selItem->getPartObject();

  enumNucPartsType selType = selObj->GetType();
  std::string selText = selItem->text(0).toStdString();
  switch(selType)
  {
    case CMBNUC_CORE:
      //DO Nothing
      break;
    case CMBNUC_ASSEMBLY:
    {
      //clone assembly
      cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly *>(selObj);
      if(assy)
      {
        cmbNucAssembly * clone_assy = assy->clone(this->NuclearCore->getPinLibrary(),
                                                  this->NuclearCore->getDuctLibrary());
        cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
        double r,g,b;
        matColorMap->CalcRGB(r,g,b);

        clone_assy->SetLegendColor(QColor::fromRgbF(r,g,b));

        boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
        clone_assy->setLabel(cmbNucPart::getUnique(fun, assy->getLabel(), false, false));
        fun = boost::bind(&cmbNucCore::name_unique, this->NuclearCore, _1);
        clone_assy->setName(cmbNucPart::getUnique(fun, assy->getName(), false, false));

        clone_assy->setFileName("");
        int id = this->NuclearCore->getFreeId();
        clone_assy->GetParameters()->setNeumannSet_StartId(id);
        clone_assy->GetParameters()->setMaterialSet_StartId(id);
        this->NuclearCore->addPart(clone_assy);
        this->initCoreRootNode();
        this->updateWithPart(clone_assy);
        this->NuclearCore->getConnection()->componentChanged(NULL);
        emit assembliesModified(this->NuclearCore);
      }
      break;
    }
    case CMBNUC_ASSEMBLY_LINK:
    {
      cmbNucAssemblyLink * original = dynamic_cast<cmbNucAssemblyLink *>(selObj);
      if(original != NULL)
      {
        cmbNucAssembly * assy = original->getLink();
        cmbAssyParameters * assyP = assy->GetParameters();
        cmbNucAssemblyLink * link =
           new cmbNucAssemblyLink(assy,
                                  this->NuclearCore->getFreeLinkId(assyP->getMaterialSet_StartId()),
                                  this->NuclearCore->getFreeLinkId(assyP->getNeumannSet_StartId()));

        cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
        double r,g,b;
        matColorMap->CalcRGB(r,g,b);

        link->SetLegendColor(QColor::fromRgbF(r,g,b));

        boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
        link->setLabel(cmbNucPart::getUnique(fun, original->getLabel(), false, false));
        fun = boost::bind(&cmbNucCore::name_unique, this->NuclearCore, _1);
        link->setName(cmbNucPart::getUnique(fun, original->getName(), false, false));

        this->NuclearCore->addPart(link);
        this->initCoreRootNode();
        this->updateWithPart(link);
        this->NuclearCore->getConnection()->componentChanged(NULL);
        emit assembliesModified(this->NuclearCore);
      }
      break;
    }
    case CMBNUC_ASSY_PINCELL:
    {
      PinCell* pincell = dynamic_cast<PinCell *>(selObj);
      if(pincell)
      {
        cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
        double rgb[3];
        matColorMap->CalcRGB(rgb[0],rgb[1],rgb[2]);
        PinCell* clonePin = new PinCell();
        clonePin->fill(pincell);
        clonePin->SetLegendColor(QColor::fromRgbF(rgb[0],rgb[1],rgb[2]));

        cmbNucPartLibrary * pl = this->NuclearCore->getPinLibrary();
        boost::function<bool (QString)> fun =
                                            boost::bind(&cmbNucPartLibrary::labelConflicts, pl, _1);
        clonePin->setLabel(cmbNucPart::getUnique(fun, clonePin->getLabel(), true, false));
        fun = boost::bind(&cmbNucPartLibrary::nameConflicts, pl, _1);
        clonePin->setName(cmbNucPart::getUnique(fun, clonePin->getName(), true, false));

        pl->addPart(clonePin);
        this->initCoreRootNode();
        this->updateWithPart(clonePin, true);
        emit assembliesModified(this->NuclearCore);
      }
      break;
    }
    case CMBNUC_ASSY_DUCTCELL:
    {
      DuctCell* ductcell = dynamic_cast<DuctCell *>(selObj);
      if(ductcell)
      {
        DuctCell* cloneDuct = new DuctCell();
        cloneDuct->fill(ductcell);
        cmbNucPartLibrary * dl = this->NuclearCore->getDuctLibrary();
        boost::function<bool (QString)> fun =
                                            boost::bind(&cmbNucPartLibrary::nameConflicts, dl, _1);
        cloneDuct->setName(cmbNucPart::getUnique(fun, cloneDuct->getName(), true, false));
        cloneDuct->setLabel(cloneDuct->getLabel());
        dl->addPart(cloneDuct);
        this->initCoreRootNode();
        this->updateWithPart(cloneDuct, true);
        emit assembliesModified(this->NuclearCore);
      }
      break;
    }
    case CMBNUC_ASSY_BASEOBJ:
    {
      if(selObj)
      {
        cmbNucPart * blank = selObj->clone();
        boost::function<bool (QString)> fun =
                                      boost::bind(&cmbNucCore::label_unique, this->NuclearCore, _1);
        blank->setLabel(cmbNucPart::getUnique(fun, selObj->getLabel(), false, false));
        double rgb[3];
        cmbNucMaterialColors::instance()->CalcRGB(rgb[0],rgb[1],rgb[2]);
        blank->SetLegendColor(QColor::fromRgbF(rgb[0],rgb[1],rgb[2]));
        this->NuclearCore->addPart(blank);
        this->initCoreRootNode();
        this->updateWithPart(blank);
        this->NuclearCore->getConnection()->componentChanged(NULL);
        emit assembliesModified(this->NuclearCore);
      }
      break;
    }
  }

  emit checkSavedAndGenerate();
}
