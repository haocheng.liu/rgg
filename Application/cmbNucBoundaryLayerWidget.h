#ifndef cmbNucBoundaryLayerWidget_h
#define cmbNucBoundaryLayerWidget_h

#include "cmbNucWidgetChangeChecker.h"
#include "cmbNucCore.h"

class cmbNucBoundaryLayerWidgetInternal;

class cmbNucBoundaryLayerWidget : public cmbNucCheckableWidget
{
  Q_OBJECT
public:
  cmbNucBoundaryLayerWidget(QWidget * parent);
  ~cmbNucBoundaryLayerWidget();
  void set(cmbNucCore * core);
protected:
  virtual void onApply();
  virtual void onReset();
  virtual void onClear();
protected slots:
  void checkChange();
  void setBoundaryEnabled(bool v);
protected:
  cmbNucBoundaryLayerWidgetInternal * Internal;
  cmbNucCore * Core;
};

#endif
