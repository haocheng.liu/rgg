#ifndef __cmbNucPinCellEditor_h
#define __cmbNucPinCellEditor_h

#include <QWidget>
#include <vtkSmartPointer.h>
#include "vtkMultiBlockDataSet.h"
#include "cmbNucWidgetChangeCheckingData.h"

#include "ui_cmbNucPinCellEditor.h"

class QComboBox;
class PinCell;
class PinSubPart;
class cmbNucPinLibrary;
class cmbNucPart;
class cmbNucCore;
class PinCellEditorInternal;

// the pin cell editor widget. this dialog allows the user to modify a
// single pin cell. the sections (cylinders/frustums) can
// be added/removed/modifed as well as the layers. materials can be assigned
// to each layer via the materials table.
class cmbNucPinCellEditor : public cmbNucCheckableWidget
{
  Q_OBJECT

public:
  cmbNucPinCellEditor(QWidget *parent = 0);
  ~cmbNucPinCellEditor();

  void SetPinCell(PinCell *pincell, cmbNucCore * core);

  bool isCrossSectioned();

signals:
  void pincellModified(cmbNucPart*);
  void nameChange(cmbNucPart* obj, const QString & name, const QString & oname);
  void valueChange();
  void resetView();

public slots:
  void UpdatePinCell();
  void UpdateData();
  void onUpdateLayerMaterial();
  void onUpdateCellMaterial( const QString & text );

private slots:
  void dataChanged();
  void addComponent();
  void deleteComponent();
  void addLayerBefore();
  void addLayerAfter();
  void deleteLayer();
  void sectionTypeComboBoxChanged(const QString &type);
  void setupMaterialComboBox(QComboBox *comboBox, bool isCell);
  void onPieceSelected();
  PinSubPart* createComponentObject(int i, PinSubPart * );
  void createComponentItem(int row, PinSubPart *);
  void nameChanged(QString);
  void labelChanged(QString);

protected:
  void onApply();
  void onClear();
  void onReset();

private:
  PinSubPart* getSelectedPiece();
  PinCellEditorInternal* Internal;

  void setButtons();

  void createMaterialRow(int row, PinSubPart *);

  Ui::cmbNucPinCellEditor *Ui;
  PinCell *InternalPinCell;
  PinCell *ExternalPinCell;
  cmbNucPinLibrary *PinLibrary;
  bool isHex;
};

#endif // __cmbNucPinCellEditor_h
