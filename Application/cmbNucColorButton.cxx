#include "cmbNucColorButton.h"
#include <QPushButton>
#include <QColorDialog>

extern bool IS_IN_TESTING_MODE;

cmbNucColorButton::cmbNucColorButton(QPushButton * pb)
 : button(pb)
{
  if(button)
  {
    QObject::connect(button, SIGNAL(clicked()), this, SLOT(chooseColor()));
  }
}

void cmbNucColorButton::setColor(QColor c)
{
  QPalette c_palette = button->palette();
  c_palette.setColor(button->backgroundRole(), c);
  button->setAutoFillBackground(true);
  button->setPalette(c_palette);
  emit changed();
}

QColor cmbNucColorButton::getColor() const
{
  QPalette c_palette = button->palette();
  return c_palette.color(button->backgroundRole());
}

void cmbNucColorButton::chooseColor()
{
  QColor oc = getColor();
  QColorDialog::ColorDialogOptions options;
  if(IS_IN_TESTING_MODE)
  {
    options = QColorDialog::DontUseNativeDialog;
  }
  QColor selected = QColorDialog::getColor(oc, NULL,
                                           "Select key color", options);
  if(selected.isValid())
  {
    this->setColor(selected);
  }
}
