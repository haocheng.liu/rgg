#ifndef cmbNucFillLattice_h_
#define cmbNucFillLattice_h_

#include <vector>

#include <QObject>
#include <QDialog>

class Lattice;
class cmbLaticeFillFunction;
class cmbNucFillLatticeInternal;
class cmbNucFillSimpleDialogInternal;
class DrawLatticeItem;
class LatticeContainer;
class cmbNucPart;

class cmbNucFillSimpleDialog : public QDialog
{
  Q_OBJECT
public:
  cmbNucFillSimpleDialog(QWidget * parent, DrawLatticeItem* item, Lattice * c,
                         std::vector< std::pair<QString, cmbNucPart *> > & al);
  ~cmbNucFillSimpleDialog();
  cmbLaticeFillFunction * generateFunction();
  QString getFillWith();
signals:
  void refresh(DrawLatticeItem*);
public slots:
  void previewFunction();
protected:
  cmbNucFillSimpleDialogInternal * ui;
};

class cmbLaticeFillFunction
{
public:
  enum FunctionType {RING=2, ROW=3, COL=4, DIANGLE1=5, DIANGLE2=6,
                     RELATIVE_RING = 7, RELATIVE_CIRCLE = 8, CIRCLE = 9};
  virtual ~cmbLaticeFillFunction(){}
  virtual bool getNext(size_t& i, size_t& j) = 0;
  virtual void begin() = 0;
  static cmbLaticeFillFunction * generateFunction(size_t r, size_t c, double inc, bool fill,
                                                  FunctionType fun, Lattice * lat);
  static cmbLaticeFillFunction * generateFunctionHex(size_t r, size_t c, double inc, bool fill,
                                                     FunctionType fun, Lattice * lat);
};

class cmbNucFillLattice: public QObject
{
  Q_OBJECT
public:
  cmbNucFillLattice();
  ~cmbNucFillLattice();
  bool fillSimple(DrawLatticeItem* item, Lattice * lat, LatticeContainer* container);
signals:
  void refresh(DrawLatticeItem*);
private:
  cmbNucFillLatticeInternal * internal;
};

#endif
